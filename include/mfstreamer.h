/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : mfstreamer.h
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#ifndef _MFSTREAMER_H_
#define _MFSTREAMER_H_
extern "C" {
#include <libswscale/swscale.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
}
#include "media.h"
#include "rtp.h"
#include "audio.h"
#include "video.h"
#include "codecs.h"
#include "avcdescriptor.h"
#include "ffstreamer.h"

#define MF_INVALID_TIMESTAMP	((QWORD)-1)


struct MFRtpTrack
{
	class Listener : public MediaFrame::Listener
	{
	public:
		//Virtual desctructor
		virtual ~Listener(){};
	public:
		//Interface
		virtual void onRTPPacket(RTPPacket &packet) = 0;
	};

	AVFormatContext *fmtCtx;
	unsigned int streamId;
	unsigned int timeScale;
	QWORD next_pts;

	MediaFrame::Type media;
	int codec;
	RTPPacket rtp;

	MFRtpTrack(MediaFrame::Type media,int codec) : rtp(media,codec)
	{
		//Store values
		this->media = media;
		this->codec = codec;
		//Empty the rest
		fmtCtx		= NULL;
		streamId	= -1;
		timeScale	= 0;
		next_pts	= 0;
		
		rtp.SetSeqCycles(0xFFFF);
		rtp.SetSeqNum(0xFFFF);
	}
	~MFRtpTrack()
	{
	}
	int Reset();
	QWORD Read(Listener* listener, AVPacket* avPkt);
	QWORD GetNextFrameTime();
};

class MFStreamer : public FFStreamer
{
public:
	class Listener : public MFRtpTrack::Listener
	{
	public:
		//Virtual desctructor
		virtual ~Listener(){};
	public:
		//Interface
		virtual void onEnd() = 0;
	};
public:
	MFStreamer(Listener* listener);
	~MFStreamer();

	int Open(const char* filename);
	bool HasAudioTrack()	{ return audio!=NULL;	}
	bool HasVideoTrack()	{ return video!=NULL;	}
	DWORD GetAudioCodec()	{ return audio->codec;	}
	DWORD GetVideoCodec()	{ return video->codec;	}
	double GetDuration();
	DWORD GetVideoWidth();
	DWORD GetVideoHeight();
	DWORD GetVideoBitrate();
	double GetVideoFramerate();
	AVCDescriptor* GetAVCDescriptor();
	int Play();
	QWORD Tell()			{ return t;	}
	int Stop();
	int Pause();
	int Resume();
	int Seek(QWORD time);
	int Close();

	int SerializeCodecCtx(MediaFrame::Type type, char* serial);

	virtual AVCodecContext* GetAudioCodecCtx();
	virtual AVCodecContext* GetVideoCodecCtx();
	
protected:
	int				PlayLoop();
	QWORD			ReadFrame(MediaFrame::Type media, AVPacket* avPkt);

private:
	static void*	play(void* par);
	void			seek(QWORD time);

private:
	Listener		*listener;
	bool			opened;
	bool			playing;
	pthread_t		thread;
	pthread_cond_t	cond;
	pthread_mutex_t	mutex;
	QWORD			t;

	AVFormatContext	*fmtCtx;
	MFRtpTrack		*audio;
	MFRtpTrack		*video;
	bool			useAudioCodec;
	bool			useVideoCodec;

	std::list<AVPacket>	audioPkt;
	std::list<AVPacket>	videoPkt;
};

#endif
