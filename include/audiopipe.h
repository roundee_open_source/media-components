/* 
 * File:   audiopipe.h
 * Author: endryu
 *
 * Created on 2016-06-29
 */

#ifndef AUDIOPIPE_H
#define	AUDIOPIPE_H

#include <pthread.h>
#include <fifo.h>
#include <audio.h>
#include "vad.h"
#include "audiotransrater.h"

class AudioPipe :
	public AudioOutput,
	public AudioInput
{
public:
	AudioPipe();
	~AudioPipe();
	int Init();
	/** AudioInput & AudioOutput **/
	virtual DWORD	GetNativeRate() { return 0; }
	/** AudioInput */
	virtual DWORD	GetRecordingRate() { return recordRate; }
	virtual int		RecBuffer(SWORD* buffer, DWORD size);
	virtual int		ClearBuffer();
	virtual void	CancelRecBuffer();
	virtual int		StartRecording(DWORD samplerate);
	virtual int		StopRecording();
	/** AudioOutput */
	virtual DWORD	GetPlayingRate() { return playRate; }
	virtual int		PlayBuffer(SWORD* buffer, DWORD size, DWORD frameTime, BYTE vadLevel=-1);
	virtual int		StartPlaying(DWORD samplerate);
	virtual int		StopPlaying();
	int End();
private:
	fifo<SWORD, 96000>	fifoBuffer;		//based on number of samples during 2000ms & 48000Hz rate(OPUS)
	int		inited;
	VAD		vad;
	DWORD	acu;
	bool	calcVAD;
	bool	recording;
	bool	canceled;

	AudioTransrater	transrater;

	DWORD	playRate;
	DWORD	recordRate;

	pthread_mutex_t mutex;
	pthread_cond_t  cond;
};

#endif	/* AUDIOPIPE_H */

