/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : ffstreamer.h
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#ifndef _FFSTREAMER_H_
#define _FFSTREAMER_H_
extern "C" {
#include <libavformat/avformat.h>
}

class FFStreamer
{
public:
	virtual AVCodecContext* GetAudioCodecCtx() = 0;
	virtual AVCodecContext* GetVideoCodecCtx() = 0;

};

#endif
