#ifndef REMOTEVAD_H
#define REMOTEVAD_H

#include "rtp.h"

#define MAX_VAD_IDX		150


class RemoteVAD
{
public:
	enum VoiceState {
		Silence,
		Speaking
	};

	class Listener
	{
	public:
		//Virtual destructor
		virtual ~Listener(){};
	public:
		//Interface
		virtual void onVoiceStateChanged(RemoteVAD::VoiceState state) = 0;
	};

public:
	RemoteVAD();
	~RemoteVAD();
	void SetListener(Listener* listener);
	void SetVADLevel(int decibel);
	void Update(RTPPacket* packet);
	void Reset();

private:
	Listener		*listener = NULL;
	int					cutDecibel;
	VoiceState	voiceState;

	bool				vadPacket[MAX_VAD_IDX];
	int					vadCnt;
	int					vadIdx;
};

#endif	/* REMOTEVAD_H */
