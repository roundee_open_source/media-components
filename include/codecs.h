#ifndef _CODECS_H_
#define _CODECS_H_

#include "config.h"
#include "media.h"
#include <map>
#include <string.h>

class AudioCodec
{
public:
	enum Type {PCMA=8,PCMU=0,GSM=3,G722=9,AMR=118,TELEPHONE_EVENT=100,OPUS=98,AAC=97,AMRWB=119,MP3=120,UNKNOWN=-1};

public:
	static Type GetCodecForName(const char* codec)
	{
		if	(strcasecmp(codec,"PCMA")==0) return PCMA;
		else if (strcasecmp(codec,"PCMU")==0) return PCMU;
		else if (strcasecmp(codec,"GSM")==0) return GSM;
		else if (strcasecmp(codec,"G722")==0) return G722;
		else if (strcasecmp(codec,"AMR")==0) return AMR;
		else if (strcasecmp(codec,"OPUS")==0) return OPUS;
		else if (strcasecmp(codec,"AAC")==0) return AAC;
		else if (strcasecmp(codec,"AMR-WB")==0) return AMRWB;
		else if (strcasecmp(codec,"MP3")==0) return MP3;
		return UNKNOWN;
	}

	static const char* GetNameFor(Type codec)
	{
		switch (codec)
		{
			case PCMA:	return "PCMA";
			case PCMU:	return "PCMU";
			case GSM:	return "GSM";
			case G722:	return "G722";
			case AMR:	return "AMR";
			case OPUS:	return "OPUS";
			case AAC:	return "AAC";
			case AMRWB:	return "AMR-WB";
			case MP3:	return "MP3";
			default:	return "unknown";
		}
	}

	static DWORD GetClockRate(Type codec)
	{
		switch (codec)
		{
			case PCMA:	return 8000;
			case PCMU:	return 8000;
			case GSM: return 8000;
			case G722:	return 16000;
			case AMR:	return 8000;
			case OPUS:	return 48000;
			case AAC: return 44100;
			case AMRWB:	return 16000;
			case MP3: return 44100;
			default:	return 8000;
		}
	}

	typedef std::map<int,Type> RTPMap;
};

class VideoCodec
{
public:
	enum Type {H264=99,H264Rec=102,VP8=107,ULPFEC=108,RED=109,RTX=110,UNKNOWN=-1};
	static const char* GetNameFor(Type type)
	{
		switch (type)
		{
			case H264:	return "H264";
			case H264Rec:	return "H264Rec";
			case VP8:	return "VP8";
			case ULPFEC:	return "FEC";
			case RED:	return "RED";
			case RTX:	return "RTX";
			default:	return "unknown";
		}
	}
	static Type GetCodecForName(const char* codec)
	{
		if (strcasecmp(codec,"H264")==0) return H264;
		else if (strcasecmp(codec,"H264Rec")==0) return H264Rec;
		else if (strcasecmp(codec,"VP8")==0) return VP8;
		return UNKNOWN;
	}
	typedef std::map<int,Type> RTPMap;
};


class TextCodec
{
public:
	enum Type {T140=106,T140RED=105};
	static const char* GetNameFor(Type type)
	{
		switch (type)
		{
			case T140:	return "T140";
			case T140RED:	return "T140RED";
			default:	return "unknown";
		}
	}
	typedef std::map<int,Type> RTPMap;
};

static MediaFrame::Type GetMediaForCodec(BYTE codec)
{
	switch (codec)
	{
		case AudioCodec::PCMA:
		case AudioCodec::PCMU:
		case AudioCodec::GSM:
		case AudioCodec::G722:
		case AudioCodec::AMR:
		case AudioCodec::OPUS:
		case AudioCodec::AAC:
		case AudioCodec::AMRWB:
		case AudioCodec::MP3:
			return MediaFrame::Audio;
		case VideoCodec::H264:
		case VideoCodec::H264Rec:
		case VideoCodec::VP8:
		case VideoCodec::ULPFEC:
		case VideoCodec::RED:
		case VideoCodec::RTX:
			return MediaFrame::Video;
		case TextCodec::T140:
		case TextCodec::T140RED:
			return MediaFrame::Text;
		default:
			return MediaFrame::Unknown;
	}
}

static const char* GetNameForCodec(MediaFrame::Type media,DWORD codec)
{
	switch (media)
	{
		case MediaFrame::Audio:
			return AudioCodec::GetNameFor((AudioCodec::Type)codec);
		case MediaFrame::Video:
			return VideoCodec::GetNameFor((VideoCodec::Type)codec);
		case MediaFrame::Text:
			return TextCodec::GetNameFor((TextCodec::Type)codec);
	}
	return "unknown media";
}
#endif
