/*============================== File Header =================================
  FILE NAME     : mfrecorder.h
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          :
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#ifndef _MFRECORDER_H_
#define _MFRECORDER_H_
extern "C" {
#include <libswscale/swscale.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <mp4v2/mp4v2.h>
}
#include "config.h"
#include "codecs.h"
#include "audio.h"
#include "video.h"
#include "text.h"
#include "media.h"
#include "rtp.h"
#include "recordercontrol.h"

class MFRecorder :
	public RecorderControl
{
public:
	MFRecorder();
	~MFRecorder();

	bool Create(const char* filename,int size,int fps,int bitrate,int intraPeriod);

	//RecorderControl interface
	virtual bool Create(const char* filename) { return false; }
	virtual bool Record();
	virtual bool Stop();
	virtual bool Close();
	virtual RecorderControl::Type GetType()	{ return RecorderControl::MP4; }

	bool IsRecording() { return recording; }
	void onAudioRTPPacket(RTPPacket& packet);
	void onVideoRTPPacket(RTPPacket& packet);

private:
	AVFormatContext	*oc;
	AVStream				*audioSt;

	MP4FileHandle		handle;
	MP4TrackId			audioTrackId;
	MP4TrackId			videoTrackId;

	bool						recording;

	pthread_mutex_t	mutex;

	int							fps;
	int							size;

};

#endif
