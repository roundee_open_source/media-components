#ifndef RTPBUFFER_H
#define	RTPBUFFER_H

#include <errno.h>
#include <pthread.h>
#include <map>

#include "config.h"
#include "acumulator.h"
#include "use.h"
#include "rtp/RTPPacket.h"

class RTPBuffer 
{
public:
	RTPBuffer() : waited(1000)
	{
		//NO wait time
		maxWaitTime = 0;
		//No hurring up
		hurryUp = false;
		//No canceled
		cancel = false;
		//No next
		next = (DWORD)-1;
		//Crete mutex
		pthread_mutex_init(&mutex,NULL);
		//Create condition
		pthread_cond_init(&cond,NULL);
	}

	virtual ~RTPBuffer()
	{
		//Free packets
		Clear();
		//Destroy mutex
		pthread_mutex_destroy(&mutex);
	}

	bool Add(RTPPacket *rtp)
	{
		//Get seq num
		DWORD seq = rtp->GetExtSeqNum();

		//Log
		//UltraDebug("-RTPBuffer::Add() | rtp packet [next:%u,seq:%u,maxWaitTime=%d,cycles:%d-%u,time:%lld,current:%lld,hurry:%d]",next,seq,maxWaitTime,rtp->GetSeqCycles(),rtp->GetSeqNum(),rtp->GetTime(),GetTime(),hurryUp);
		
		//Lock
		pthread_mutex_lock(&mutex);

		//If already past
		if (next!=(DWORD)-1 && seq<next)
		{
			//Error
			Debug("-RTPBuffer::Add() | Out of order non recoverable packet [next:%u,seq:%u,maxWaitTime=%d,cycles:%d-%u,time:%lld,current:%lld,hurry:%d]",next,seq,maxWaitTime,rtp->GetSeqCycles(),rtp->GetSeqNum(),rtp->GetTime(),GetTime(),hurryUp);
			//Delete pacekt
			delete(rtp);
			//Unlock
			pthread_mutex_unlock(&mutex);
			//Skip it and lost forever
			return 0;
		}
		
		//Check if we already have it
		if (packets.find(seq)!=packets.end())
		{
			//Error
			Debug("-RTPBuffer::Add() | Already have that packet [next:%u,seq:%u,maxWaitTime=%d,cycles:%d-%u]",next,seq,maxWaitTime,rtp->GetSeqCycles(),rtp->GetSeqNum());
			//Delete pacekt
			delete(rtp);
			//Unlock
			pthread_mutex_unlock(&mutex);
			//Skip it and lost forever
			return 0;
		}

		//Add packet
		packets[seq] = rtp;

		//Unlock
		pthread_mutex_unlock(&mutex);

		//Signal
		pthread_cond_signal(&cond);

		return true;
	}

	void Cancel()
	{
		//Lock
		pthread_mutex_lock(&mutex);

		//Canceled
		cancel = true;

		//Unlock
		pthread_mutex_unlock(&mutex);

		//Signal condition
		pthread_cond_signal(&cond);
	}
	
	RTPPacket* GetOrdered()
	{
		
		//Check if we have somethin in queue
		if (!packets.empty())
		{
			//Get first
			RTPOrderedPackets::iterator it = packets.begin();
			//Get first seq num
			DWORD seq = it->first;
			//Get packet
			RTPPacket* candidate = it->second;
			//Get time of the packet
			QWORD time = candidate->GetTime();

			//Check if first is the one expected or wait if not
			if (next==(DWORD)-1 || seq==next || time+maxWaitTime<GetTime() || hurryUp)
			{
				//We have it!
				RTPPacket* rtp = candidate;
				//Update next
				next = seq+1;
				//Remove it
				packets.erase(it);
				//Return it
				return rtp;
			}
		}
		//Rerturn 
		return NULL;
	}
	
	
	RTPPacket* Wait(int& lost)
	{
		//NO packet
		RTPPacket* rtp = NULL;
		lost = 0;

		//Lock
		pthread_mutex_lock(&mutex);

		//While we have to wait
		while (!cancel)
		{
			//Check if we have something in queue
			if (!packets.empty())
			{
				//Get first
				RTPOrderedPackets::iterator it = packets.begin();
				//Get first seq num
				DWORD seq = it->first;
				//Get packet
				RTPPacket* candidate = it->second;
				//Get time of the packet
				QWORD time = candidate->GetTime();
				//Get now
				QWORD now = GetTime();

				//Check if first is the one expected or wait if not
				if (next==(DWORD)-1 || seq==next || time+maxWaitTime<now || hurryUp)
				{
					//We have it!
					rtp = candidate;
					//Log
					//UltraDebug("-RTPBuffer::Wait() | rtp packet [next:%u,seq:%u,maxWaitTime=%d,cycles:%d-%u,time:%lld,current:%lld,hurry:%d]",next,seq,maxWaitTime,rtp->GetSeqCycles(),rtp->GetSeqNum(),rtp->GetTime(),GetTime(),hurryUp);
					if(next != (DWORD)-1 && seq != next) {
						Log("-RTPBuffer::Wait() | rtp packet loss [next=%u][seq=%u]", next, seq);
						lost = seq - next;
					}
					//Update next
					next = seq+1;
					//Remove it
					packets.erase(it);
					//Return it!
					break;
				}

				//We have to wait
				timespec ts;
				//Calculate until when we have to sleep
				ts.tv_sec  = (time+maxWaitTime) / 1000;
				ts.tv_nsec = (time+maxWaitTime - ts.tv_sec*1000)*1000;
				
				//Wait with time out
				int ret = pthread_cond_timedwait(&cond,&mutex,&ts);
				//Check if there is an errot different than timeout
				if (ret && ret!=ETIMEDOUT)
					//Print error
					Error("-WaitQueue cond timedwait error [%d,%d]",ret,errno);
			}
			else {
				timeval now;
				timespec ts;

				gettimeofday(&now, NULL);
				ts.tv_sec = now.tv_sec + 3;		// 3sec
				ts.tv_nsec = now.tv_usec * 1000;
				
				//Not hurryUp more
				hurryUp = false;
				//Wait until we have a new rtp pacekt
				int ret = pthread_cond_timedwait(&cond, &mutex, &ts);
				if(ret == ETIMEDOUT) {
					break;
				}
				//Check error
				if (ret) {
					//Print error
					Error("-WaitQueue cond timedwait error [%d,%d]",ret,errno);
				}
			}
		}
		
		//Unlock
		pthread_mutex_unlock(&mutex);

		//canceled
		return rtp;
	}

	void Clear()
	{
		//Lock
		pthread_mutex_lock(&mutex);

		//And remove all from queue
		ClearPackets();

		//UnLock
		pthread_mutex_unlock(&mutex);
	}

	void HurryUp()
	{
		//Set flag
		hurryUp = true;
		//Signal condition and proccess rtp now
		pthread_cond_signal(&cond);
	}

	void Reset()
	{
		//Lock
		pthread_mutex_lock(&mutex);

		//And remove all from queue
		ClearPackets();

		//And remove cancel
		cancel = false;

		//No next
		next = (DWORD)-1;

		//Signal condition
		pthread_cond_signal(&cond);
		
		//UnLock
		pthread_mutex_unlock(&mutex);
		
		//Clear stats
		waited.Reset(GetTime());
	}

	DWORD Length() const
	{
		//REturn objets in queu
		return packets.size();
	}

	void SetMaxWaitTime(DWORD maxWaitTime)
	{
		this->maxWaitTime = maxWaitTime;
	}

	// Set time in ms
	void SetTime(QWORD ms)
	{
		time = ms;
	}

	QWORD GetTime() const
	{
		if (!time)
			return getTime()/1000;
		return time;
	}

	DWORD GetMinWaitedime() const
	{
		return waited.GetMinValueInWindow();
	}

	DWORD GetMaxWaitedTime() const
	{
		return waited.GetMaxValueInWindow();
	}

	long double GetAvgWaitedTime() const
	{
		return waited.GetInstantMedia();
	}

private:
	void ClearPackets()
	{
		//For each item, list shall be locked before
		for (RTPOrderedPackets::iterator it=packets.begin(); it!=packets.end(); ++it)
			//Delete rtp
			delete(it->second);
		//Clear all list
		packets.clear();
	}

private:
	typedef std::map<DWORD,RTPPacket*> RTPOrderedPackets;

private:
	//The event list
	RTPOrderedPackets	packets;
	bool							cancel;
	bool							hurryUp;
	pthread_mutex_t		mutex;
	pthread_cond_t		cond;
	DWORD							next;
	DWORD							maxWaitTime;
	QWORD							time = 0;
	Acumulator				waited;
};

#endif	/* RTPBUFFER_H */

