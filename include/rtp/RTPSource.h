#ifndef RTPSOURCE_H
#define RTPSOURCE_H

#include "config.h"
#include "acumulator.h"
#include "rtp/RTCPSenderReport.h"
#include "rtp/RTCPCompoundPacket.h"


struct RTPSource
{
	DWORD	ssrc;
	DWORD	extSeq;
	DWORD	cycles;
	DWORD	jitter;
	DWORD	numPackets;
	DWORD	numRTCPPackets;
	DWORD	totalBytes;
	DWORD	totalRTCPBytes;
	Acumulator bitrate;

	RTPSource() : bitrate(1000)
	{
		ssrc		= 0;
		extSeq		= 0;
		cycles		= 0;
		numPackets	= 0;
		numRTCPPackets	= 0;
		totalBytes	= 0;
		totalRTCPBytes	= 0;
		jitter		= 0;
	}

	virtual ~RTPSource()
	{

	}

	RTCPCompoundPacket* CreateSenderReport();

	WORD SetSeqNum(WORD seqNum)
	{
		//Check if we have a sequence wrap
		if (seqNum<0x0FFF && (extSeq & 0xFFFF)>0xF000)
			//Increase cycles
			cycles++;

		//Get ext seq
		DWORD extSeq = ((DWORD)cycles)<<16 | seqNum;

		//If we have a not out of order pacekt
		if (extSeq > this->extSeq || !numPackets)
			//Update seq num
			this->extSeq = extSeq;

		//Return seq cycles count
		return cycles;
	}

	/*
	 * Get seq num cycles from a past sequence numer
	 */
	WORD RecoverSeqNum(WORD osn)
	{
		 //Check secuence wrap
		if (numPackets && (extSeq & 0xFFFF)<0x0FFF && (osn>0xF000))
			//It is from the past cycle
			return cycles - 1;
		//It is from current cyle
		return cycles;
	}

	virtual void Update(QWORD now, DWORD seqNum,DWORD size) 
	{
		//Increase stats
		numPackets++;
		totalBytes += size;

		//Update bitrate acumulator
		bitrate.Update(now,size);
	}

	virtual void Reset()
	{
		ssrc		= 0;
		extSeq		= 0;
		cycles		= 0;
		numPackets	= 0;
		numRTCPPackets	= 0;
		totalBytes	= 0;
		totalRTCPBytes	= 0;
		jitter		= 0;
	}
};


#endif /* RTPSOURCE_H */
