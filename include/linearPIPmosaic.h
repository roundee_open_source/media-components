#ifndef _LINEARPIPMOSAIC_H_
#define _LINEARPIPMOSAIC_H_

#include "mosaic.h"
#include "framescaler.h"

class LinearPIPMosaic : public Mosaic
{
public:
	LinearPIPMosaic(Mosaic::Type type, DWORD size);
	virtual ~LinearPIPMosaic();

	//virtual int GetSlots();

	virtual BYTE*	GetFrame();
	virtual int		GetWidth();
	virtual int		GetHeight();

	virtual int		Update(int index,BYTE *frame,int width,int heigth, bool keepAspectRatio);
	virtual int		Clean(int index);

	virtual int		GetWidth(int pos);
	virtual int		GetHeight(int pos);
	virtual int		GetTop(int pos);
	virtual int		GetLeft(int pos);

private:
	BYTE					*underBuffer;
	BYTE					*under;

};

#endif
