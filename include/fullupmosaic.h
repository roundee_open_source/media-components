#ifndef _FULLUPMOSAIC_H_
#define _FULLUPMOSAIC_H_

#include "mosaic.h"
#include "framescaler.h"

class FullupMosaic : public Mosaic
{
public:
	FullupMosaic(Mosaic::Type type, DWORD size);
	virtual ~FullupMosaic();

	virtual int Update(int index, BYTE *frame, int width, int heigth, bool keepAspectRatio);
	virtual int Clean(int index);
	virtual int GetWidth(int pos);
	virtual int GetHeight(int pos);
	virtual int GetTop(int pos);
	virtual int GetLeft(int pos);

	int		clipImgWidth;
	int		clipImgHeight;
	int		clipBufferSize;
	BYTE	*clipBuffer;
	BYTE	*clipY;
	BYTE	*clipU;
	BYTE	*clipV;
};

#endif
