#ifndef _LOG_H_
#define _LOG_H_

#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/time.h>
#include "config.h"
#include "tools.h"

typedef void (*p_onLogMessage)(int type, const char* message);

class Logger
{
public:
	enum Type { MCU_ERROR=0, MCU_LOG=1, MCU_DEBUG=2, MCU_ULTRADEBUG=3 };

	static Logger& getInstance()
	{
		static Logger   instance;
		return instance;
	}
	
	static void Print(int type, const char* message)
	{
		getInstance().logCb(type, message);
	}
	
	static bool IsUltraDebugEnabled()
	{
		return getInstance().ultradebug;
	}
	
	static bool IsDebugEnabled()
	{
		return getInstance().debug;
	}

	static bool EnableDebug(bool debug)
	{
		return getInstance().debug = debug;
	}
	
	static bool EnableUltraDebug(bool ultradebug)
	{
		if (ultradebug) EnableDebug(ultradebug);
		return getInstance().ultradebug = ultradebug;
	}

protected:
	bool debug;
	bool ultradebug;
private:
	Logger()
	{
		debug = true;
		ultradebug = false;
	}
	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singelton appearing.
	Logger(Logger const&);			// Don't Implement
	void operator=(Logger const&);		// Don't implement

public:
	static void SetLogCallback(p_onLogMessage cb)
	{
		getInstance().logCb = cb;
	}
private:
	p_onLogMessage logCb;

};

inline int UltraDebug(const char *msg, ...)
{
	static char logMessage[1024];
	va_list ap;

	if(Logger::IsUltraDebugEnabled()) {
		va_start(ap, msg);
		vsprintf(logMessage, msg, ap);
		va_end(ap);
		Logger::Print(Logger::MCU_ULTRADEBUG, logMessage);
	}

	return 1;
}

inline int Debug(const char *msg, ...)
{
	static char logMessage[1024];
	va_list ap;

	if(Logger::IsDebugEnabled()) {
		va_start(ap, msg);
		vsprintf(logMessage, msg, ap);
		va_end(ap);
		Logger::Print(Logger::MCU_DEBUG, logMessage);
	}

	return 1;
}

inline int Log(const char *msg, ...)
{
	static char logMessage[1024];
	va_list ap;

	va_start(ap, msg);
	vsprintf(logMessage, msg, ap);
	va_end(ap);
	Logger::Print(Logger::MCU_LOG, logMessage);

	return 1;
}

inline int Error(const char *msg, ...)
{
	static char logMessage[1024];
	va_list ap;

	va_start(ap, msg);
	vsprintf(logMessage, msg, ap);
	va_end(ap);
	Logger::Print(Logger::MCU_ERROR, logMessage);

	return 0;
}


inline void BitDump(DWORD val,BYTE n)
{
	char line1[136];
	char line2[136];
	DWORD i=0;
	if (n>24)
	{
		sprintf(line1,"0x%.2x     0x%.2x     0x%.2x     0x%.2x     ",(BYTE)(val>>24),(BYTE)(val>>16),(BYTE)(val>>4),(BYTE)(val));
		i+= BitPrint(line2,(BYTE)(val>>24),n-24);
		i+= BitPrint(line2+i,(BYTE)(val>>16),4);
		i+= BitPrint(line2+i,(BYTE)(val>>4),4);
		i+= BitPrint(line2+i,(BYTE)(val),4);
	} else if (n>16) {
		sprintf(line1,"0x%.2x     0x%.2x     0x%.2x     ",(BYTE)(val>>16),(BYTE)(val>>4),(BYTE)(val));
		i+= BitPrint(line2+i,(BYTE)(val>>16),n-16);
		i+= BitPrint(line2+i,(BYTE)(val>>4),4);
		i+= BitPrint(line2+i,(BYTE)(val),4);
	} else if (n>4) {
		sprintf(line1,"0x%.2x     0x%.2x     ",(BYTE)(val>>4),(BYTE)(val));
		i+= BitPrint(line2,(BYTE)(val>>4),n-4);
		i+= BitPrint(line2+i,(BYTE)(val),4);
	} else {
		sprintf(line1,"0x%.2x     ",(BYTE)(val));
		BitPrint(line2,(BYTE)(val),n);
	}
	Debug("Dumping 0x%.4x:%d\n\t%s\n\t%s",val,n,line1,line2);
}

inline void BitDump(WORD val)
{
	BitDump(val,16);
}

inline void BitDump(DWORD val)
{
	BitDump(val,32);
}

inline void BitDump(QWORD val)
{
	BitDump(val>>32,32);
	BitDump(val,32);
}

inline void Dump(const BYTE *data,DWORD size)
{
	for(DWORD i=0;i<(size/4);i++)
		Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   %c%c%c%c%c%c%c%c]",4*i,data[4*i],data[4*i+1],data[4*i+2],data[4*i+3],data[4*i+4],data[4*i+5],data[4*i+6],data[4*i+7],PC(data[4*i]),PC(data[4*i+1]),PC(data[4*i+2]),PC(data[4*i+3]),PC(data[4*i+4]),PC(data[4*i+5]),PC(data[4*i+6]),PC(data[4*i+7]));
	switch(size%4)
	{
		case 1:
			Debug("[%.4x] [0x%.2x                                                    %c       ]",size-1,data[size-1],PC(data[size-1]));
			break;
		case 2:
			Debug("[%.4x] [0x%.2x   0x%.2x                                             %c%c      ]",size-2,data[size-2],data[size-1],PC(data[size-2]),PC(data[size-1]));
			break;
		case 3:
			Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x                                      %c%c%c     ]",size-3,data[size-3],data[size-2],data[size-1],PC(data[size-3]),PC(data[size-2]),PC(data[size-1]));
			break;
		case 4:
			Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x   0x%.2x                               %c%c%c%c    ]",size-4,data[size-4],data[size-3],data[size-2],data[size-1],PC(data[size-4]),PC(data[size-3]),PC(data[size-2]),PC(data[size-1]));
			break;
		case 5:
			Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x                        %c%c%c%c%c   ]",size-5,data[size-5],data[size-4],data[size-3],data[size-2],data[size-1],PC(data[size-5]),PC(data[size-4]),PC(data[size-3]),PC(data[size-2]),PC(data[size-1]));
			break;
		case 6:
			Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x                 %c%c%c%c%c%c  ]",size-6,data[size-6],data[size-5],data[size-4],data[size-3],data[size-2],data[size-1],PC(data[size-6]),PC(data[size-5]),PC(data[size-4]),PC(data[size-3]),PC(data[size-2]),PC(data[size-1]));
			break;
		case 7:
			Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x   0x%.2x          %c%c%c%c%c%c%c ]",size-7,data[size-7],data[size-6],data[size-5],data[size-4],data[size-3],data[size-2],data[size-1],PC(data[size-7]),PC(data[size-6]),PC(data[size-5]),PC(data[size-4]),PC(data[size-3]),PC(data[size-2]),PC(data[size-1]));
			break;
	}
}


inline void Dump4(const BYTE *data,DWORD size)
{
	for(DWORD i=0;i<(size/4);i++)
		Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x   0x%.2x   %c%c%c%c]",4*i,data[4*i],data[4*i+1],data[4*i+2],data[4*i+3],PC(data[4*i]),PC(data[4*i+1]),PC(data[4*i+2]),PC(data[4*i+3]));
	switch(size%4)
	{
		case 1:
			Debug("[%.4x] [0x%.2x                      %c%c]",size-1,data[size-1],PC(data[size-1]));
			break;
		case 2:
			Debug("[%.4x] [0x%.2x   0x%.2x                %c%c]",size-2,data[size-2],data[size-1],PC(data[size-2]),PC(data[size-1]));
			break;
		case 3:
			Debug("[%.4x] [0x%.2x   0x%.2x   0x%.2x          %c%c%c%c]",size-3,data[size-3],data[size-2],data[size-1],PC(data[size-3]),PC(data[size-2]),PC(data[size-1]));
			break;
	}
}



#endif
