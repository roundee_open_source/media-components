#ifndef _OGGOPUS_CONTAINER_H_
#define _OGGOPUS_CONTAINER_H_

#include <ogg/ogg.h>
#include "config.h"


/* state struct for passing around our handles */
typedef struct {
  ogg_stream_state	stream;
  FILE							*out;
  int								seq;
  ogg_int64_t				granulepos;

} OGGOPUS_ctx;


OGGOPUS_ctx*	OGGOPUS_open(const char* ogg_file);
void					OGGOPUS_close(OGGOPUS_ctx* pCtx);
void					OGGOPUS_write(OGGOPUS_ctx* pCtx, const u_char* data, size_t size);


#endif
