/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : Player2.h
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#ifndef PLAYER_H
#define PLAYER_H

#include "RTPMultiplexer.h"
#include "mfstreamer.h"


class Player :
	public MFStreamer,
	public MFStreamer::Listener
{
public:
	class Listener
	{
	public:
		//Virtual destructor
		virtual ~Listener() {};
	public:
		//Interface
		virtual void onEndOfFile(Player* player, void* param) = 0;
	};

public:
	Player(std::wstring tag);
	void SetListener(Player::Listener* listener, void* param);
	Joinable* GetJoinable(MediaFrame::Type media);

	//MFStreamer listener
	virtual void onRTPPacket(RTPPacket& packet);
	virtual void onMediaFrame(MediaFrame& frame);
	virtual void onMediaFrame(DWORD ssrc, MediaFrame& frame);
	virtual void onEnd();

	std::wstring& GetTag() { return tag; }

private:
	RTPMultiplexer audio;
	RTPMultiplexer video;
	std::wstring tag;
	Player::Listener* listener;
	void *param;
};

#endif	/* PLAYER_H */
