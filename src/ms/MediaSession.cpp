#include "log.h"
#include "MediaSession.h"

MediaSession::MediaSession(std::wstring tag)
{
	//Init
	maxEndpointId = 1;
	maxPlayersId = 1;
	maxRecordersId = 1;
	maxAudioMixerId = 1;
	maxAudioTranscoderId = 1;
	maxVideoMixerId = 1;
	maxVideoTranscoderId = 1;
	//Store it
	this->tag = tag;
}

MediaSession::~MediaSession()
{
	End();
}

void MediaSession::SetListener(MediaSession::Listener *listener,void* param)
{
	//Store values
	this->listener = listener;
	this->param = param;
}

int MediaSession::Init()
{
	Log("-Init media session");

	//Inited
	return 1;
}

int MediaSession::End()
{
	Log(">End media session");

	//Delete all recorders
	for(Recorders::iterator it=recorders.begin(); it!=recorders.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	recorders.clear();

	//End all endpoints
	for(Endpoints::iterator it=endpoints.begin(); it!=endpoints.end(); ++it) {
		//End it
		it->second->End();
	}

	//Delete all players
	for(Players::iterator it=players.begin(); it!=players.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	players.clear();

	//Delete all audio transcoders
	for(AudioTranscoders::iterator it=audioTranscoders.begin(); it!=audioTranscoders.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	audioTranscoders.clear();

	//Delete all video transcoders
	for(VideoTranscoders::iterator it=videoTranscoders.begin(); it!=videoTranscoders.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	videoTranscoders.clear();

	//Delete all audio mixers
	for(AudioMixers::iterator it=audioMixers.begin(); it!=audioMixers.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	audioMixers.clear();

	//Delete all video mixers
	for(VideoMixers::iterator it=videoMixers.begin(); it!=videoMixers.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	videoMixers.clear();

	//Delete all endpoints
	for(Endpoints::iterator it=endpoints.begin(); it!=endpoints.end(); ++it) {
		//Delete object
		delete(it->second);
	}
	//Clean map
	endpoints.clear();

	Log("<End media session");

	return 1;
}


int MediaSession::GetPlayerCount()
{
	return players.size();
}

int MediaSession::PlayerCreate(std::wstring tag)
{
	//Create ID
	int playerId = maxPlayersId++;
	//Create player
	Player* player = new Player(tag);
	//Set event listener
	player->SetListener(this,(void*)playerId);
	//Append the player
	players[playerId] = player;
	//Return it
	return playerId;
}

int MediaSession::PlayerOpen(int playerId,const char* filename)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Open(filename);
}

int MediaSession::PlayerGetDuration(int playerId, DWORD* duration)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;
	*duration = player->GetDuration();

	return 1;
}

int MediaSession::PlayerPlay(int playerId)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Play();
}

int MediaSession::PlayerPause(int playerId)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Pause();
}

int MediaSession::PlayerResume(int playerId)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Resume();
}

int MediaSession::PlayerSeek(int playerId,QWORD time)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Seek(time * 1000);
}

int MediaSession::PlayerStop(int playerId)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Stop();
}

int MediaSession::PlayerClose(int playerId)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	return player->Close();
}

int MediaSession::PlayerDelete(int playerId)
{
	//Get Player
	Players::iterator it = players.find(playerId);

	//If not found
	if (it==players.end())
		//Exit
		return Error("Player not found");
	//Get it
	Player* player = it->second;

	//Remove from list
	players.erase(it);

	//Delete player
	delete(player);

	return 1;
}


int MediaSession::GetRecorderCount()
{
	return recorders.size();
}

int MediaSession::RecorderCreate(std::wstring tag, const char* filename,int size,int fps,int bitrate,int intraPeriod)
{
	//Create ID
	int recorderId = maxRecordersId++;
	//Create recorder
	Recorder* recorder = new Recorder(tag);
	//create recording
	if (!recorder->Create(filename,size,fps,bitrate,intraPeriod))
		//Error
		return Error("-Could not create file");
	
	//Append the recorder
	recorders[recorderId] = recorder;
	//Return it
	return recorderId;
}

int MediaSession::RecorderRecord(int recorderId)
{
	//Get recorder
	Recorders::iterator it = recorders.find(recorderId);
	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;
	//Start recording
	return recorder->Record();
}

int MediaSession::RecorderStop(int recorderId)
{
	//Get recorder
	Recorders::iterator it = recorders.find(recorderId);
	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;
	//Stop recording
	return recorder->Stop();
}

int MediaSession::RecorderDelete(int recorderId)
{
	//Get recorder
	Recorders::iterator it = recorders.find(recorderId);

	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;

	//Remove from list
	recorders.erase(it);

	//Delete recorder
	delete(recorder);

	return 1;
}

/* Change From ===>
*/
int MediaSession::RecorderAttachToAudioTranscoder(int recorderId,int transcoderId)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);
	
	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;
	
	//Get Audio transcoder
	AudioTranscoders::iterator itTranscoder = audioTranscoders.find(transcoderId);
	
	//If not found
	if (itTranscoder == audioTranscoders.end())
		//Exit
		return Error("AudioTranscoder not found[%d]", transcoderId);
	
	//Get it
	AudioTranscoder* audioTranscoder = itTranscoder->second;
	
	//Attach
	return recorder->Attach(MediaFrame::Audio, audioTranscoder);
}

int MediaSession::RecorderAttachToVideoTranscoder(int recorderId,int transcoderId)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);
	
	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;
	
	//Get Video transcoder
	VideoTranscoders::iterator itTranscoder = videoTranscoders.find(transcoderId);
	
	//If not found
	if (itTranscoder == videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found[%d]", transcoderId);
	
	//Get it
	VideoTranscoder* videoTranscoder = itTranscoder->second;
	
	//Attach
	return recorder->Attach(MediaFrame::Video, videoTranscoder);
}
// <=== To

int MediaSession::RecorderAttachToAudioMixerPort(int recorderId,int mixerId,int portId)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);

	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;

	//Get AudioMixer
	AudioMixers::iterator itMixer = audioMixers.find(mixerId);

	//If not found
	if (itMixer==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");

	//Get it
	AudioMixerResource* audioMixer = itMixer->second;

	//Attach
	return recorder->Attach(MediaFrame::Audio,audioMixer->GetJoinable(portId));
}

int MediaSession::RecorderAttachToVideoMixerPort(int recorderId,int mixerId,int portId)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);

	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;

	//Get VideoMixer
	VideoMixers::iterator itMixer = videoMixers.find(mixerId);

	//If not found
	if (itMixer==videoMixers.end())
		//Exit
		return Error("AudioMixerResource not found");

	//Get it
	VideoMixerResource* videoMixer = itMixer->second;

	//And attach
	return recorder->Attach(MediaFrame::Video,videoMixer->GetJoinable(portId));
}

int MediaSession::RecorderAttachToEndpoint(int recorderId,int endpointId,MediaFrame::Type media)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);

	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;

	//Get source endpoint
	Endpoints::iterator itEndpoints = endpoints.find(endpointId);

	//If not found
	if (itEndpoints==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* source = itEndpoints->second;

	//Request iframe
	//source->RequestUpdate(media);

	//Attach
	return recorder->Attach(media,source->GetJoinable(media));
}

int MediaSession::RecorderAttachToNetJoinable(int recorderId,NetJoinable* join,MediaFrame::Type media)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);

	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;

	//Attach
	return recorder->Attach(media,join);
}

int MediaSession::RecorderDettach(int recorderId,MediaFrame::Type media)
{
	//Get Recorder
	Recorders::iterator it = recorders.find(recorderId);

	//If not found
	if (it==recorders.end())
		//Exit
		return Error("Recorder not found");
	//Get it
	Recorder* recorder = it->second;

	//Dettach
	return recorder->Dettach(media);
}


int MediaSession::GetEndpointCount()
{
	return endpoints.size();
}

int MediaSession::EndpointCreate(std::wstring name,bool audioSupported,bool videoSupported,bool textSupport)
{
	//Create endpoint
	Endpoint* endpoint = new Endpoint(name,audioSupported,videoSupported,textSupport);
	//Init it
	endpoint->Init();
	//Create ID
	int endpointId = maxEndpointId++;
	//Log endpoint tag name
	Log("-EndpointCreate [%d,%ls]",endpointId,endpoint->GetName().c_str());
	//Log("-EndpointCreate [%p]",endpoint);
	//Append
	endpoints[endpointId] = endpoint;

	endpoint->SetListener(this, (void*)endpointId);

	//Return it
	return endpointId;

}

/* Change From ===>
*/
int MediaSession::EndpointGetPorts(int endpointId,int* audioPort,int* videoPort,int* textPort)
{
	//Get Endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if(it == endpoints.end())
		return Error("Endpoint no found");

	//Get it
	Endpoint* endpoint = it->second;

	//Call it
	return endpoint->GetPorts(audioPort, videoPort, textPort);
}
// <=== To

int MediaSession::EndpointDelete(int endpointId)
{
	//Get Endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointDelete [%ls]",endpoint->GetName().c_str());

	//Remove from list
	endpoints.erase(it);

	//End it
	endpoint->End();

	//Delete endpoint
	delete(endpoint);

	return 1;
}

int MediaSession::EndpointSetMaxLimit(int endpointId, int kbps)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;
	
	return endpoint->SetMaxLimit(kbps);
}

int MediaSession::EndpointSetVADLevel(int endpointId, int decibel)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;
	
	return endpoint->SetVADLevel(decibel);
}

int MediaSession::EndpointSetLocalCryptoSDES(int endpointId,MediaFrame::Type media,const char *suite,const char* key)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Call it
	return endpoint->SetLocalCryptoSDES(media,suite,key);
}

int MediaSession::EndpointSetRemoteCryptoSDES(int endpointId,MediaFrame::Type media,const char *suite,const char* key)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Call it
	return endpoint->SetRemoteCryptoSDES(media,suite,key);
}

int MediaSession::EndpointSetRemoteCryptoDTLS(int endpointId,MediaFrame::Type media,const char *setup,const char *hash,const char *fingerprint)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Call it
	return endpoint->SetRemoteCryptoDTLS(media,setup,hash,fingerprint);
}


int MediaSession::EndpointSetLocalSTUNCredentials(int endpointId,MediaFrame::Type media,const char *username,const char* pwd)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Call it
	return endpoint->SetLocalSTUNCredentials(media,username,pwd);
}

int MediaSession::EndpointSetRemoteSTUNCredentials(int endpointId,MediaFrame::Type media,const char *username,const char* pwd)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Call it
	return endpoint->SetRemoteSTUNCredentials(media,username,pwd);
}

int MediaSession::EndpointSetRTPProperties(int endpointId,MediaFrame::Type media,const Properties& properties)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;
	Log("-EndpointSetRTPProperties [endpointId=%d][media=%d]", endpointId, media);
	//Call it
	return endpoint->SetRTPProperties(media,properties);
}

//Endpoint Video functionality
int MediaSession::EndpointStartSending(int endpointId,MediaFrame::Type media,char *sendIp,int sendPort,RTPMap& rtpMap)
{
	//Get Endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if(it == endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointStartSending [%ls]", endpoint->GetName().c_str());

	//Execute
	return endpoint->StartSending(media, sendIp, sendPort, rtpMap);
}

int MediaSession::EndpointStopSending(int endpointId,MediaFrame::Type media)
{
	//Get Endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointStopSending [%ls]",endpoint->GetName().c_str());

	//Execute
	return endpoint->StopSending(media);
}

int MediaSession::EndpointStartReceiving(int endpointId,MediaFrame::Type media,RTPMap& rtpMap)
{
	//Get Endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointStartReceiving [%ls]",endpoint->GetName().c_str());

	//Execute
	return endpoint->StartReceiving(media,rtpMap);
}
int MediaSession::EndpointStopReceiving(int endpointId,MediaFrame::Type media)
{
	//Get Endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointStopReceiving [%ls]",endpoint->GetName().c_str());

	//Execute
	return endpoint->StopReceiving(media);
}

int MediaSession::EndpointRequestUpdate(int endpointId,MediaFrame::Type media)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointRequestUpdate [%ls]",endpoint->GetName().c_str());

	//Execute
	return endpoint->RequestUpdate(media);
}

int MediaSession::EndpointStartAudioCapture(int endpointId, const char* filename)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if(it == endpoints.end())
		//Exit
		return Error("Endpoint no found");
	//Get it
	Endpoint *endpoint = it->second;

	//Log
	Log("-EndpointStartAudioCapture [%s]", filename);

	//Execute
	return endpoint->StartAudioCapture(filename);
}

int MediaSession::EndpointAttachToPlayer(int endpointId,int playerId,MediaFrame::Type media)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointAttachToPlayer [%ls]",endpoint->GetName().c_str());

	//Get Player
	Players::iterator itPlayer = players.find(playerId);

	//If not found
	if (itPlayer==players.end())
		//Exit
		return Error("Player not found");

	//Get it
	Player* player = itPlayer->second;

	//Attach
	return endpoint->Attach(media,player->GetJoinable(media));
}

int MediaSession::EndpointAttachToAudioMixerPort(int endpointId,int mixerId,int portId)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointAttachToAudioMixerPort [%ls]",endpoint->GetName().c_str());

	//Get AudioMixer
	AudioMixers::iterator itMixer = audioMixers.find(mixerId);

	//If not found
	if (itMixer==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");

	//Get it
	AudioMixerResource* audioMixer = itMixer->second;

	//Attach
	return endpoint->Attach(MediaFrame::Audio,audioMixer->GetJoinable(portId));
}

int MediaSession::EndpointAttachToVideoMixerPort(int endpointId,int mixerId,int portId)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointAttachToVideoMixerPort [%ls]",endpoint->GetName().c_str());

	//Get VideoMixer
	VideoMixers::iterator itMixer = videoMixers.find(mixerId);

	//If not found
	if (itMixer==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found");

	//Get it
	VideoMixerResource* videoMixer = itMixer->second;

	//And attach
	return endpoint->Attach(MediaFrame::Video,videoMixer->GetJoinable(portId));
}

int MediaSession::EndpointAttachToAudioTranscoder(int endpointId,int audioTranscoderId)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found [%d]",endpointId);
	//Get it
	Endpoint* endpoint = it->second;

	//Get Audio transcoder
	AudioTranscoders::iterator itTranscoder = audioTranscoders.find(audioTranscoderId);

	//If not found
	if (itTranscoder==audioTranscoders.end())
		//Exit
		return Error("AudioTranscoder not found[%d]",audioTranscoderId);

	//Get it
	AudioTranscoder* audioTranscoder = itTranscoder->second;

	//Log endpoint tag name
	Log("-EndpointAttachToAudioTranscoder [endpoint:%ls,transcoder:%ls]",endpoint->GetName().c_str(),audioTranscoder->GetName().c_str());

	//And attach
	return endpoint->Attach(MediaFrame::Audio,audioTranscoder);
}

int MediaSession::EndpointAttachToVideoTranscoder(int endpointId,int videoTranscoderId)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found [%d]",endpointId);
	//Get it
	Endpoint* endpoint = it->second;

	//Get Video transcoder
	VideoTranscoders::iterator itTranscoder = videoTranscoders.find(videoTranscoderId);

	//If not found
	if (itTranscoder==videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found[%d]",videoTranscoderId);

	//Get it
	VideoTranscoder* videoTranscoder = itTranscoder->second;

	//Log endpoint tag name
	Log("-EndpointAttachToVideoTranscoder [endpoint:%ls,transcoder:%ls]",endpoint->GetName().c_str(),videoTranscoder->GetName().c_str());

	//And attach
	return endpoint->Attach(MediaFrame::Video,videoTranscoder);
}

int MediaSession::EndpointAttachToEndpoint(int endpointId,int sourceId,MediaFrame::Type media)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointAttachToEndpoint [%ls]",endpoint->GetName().c_str());

	//Get source endpoint
	it = endpoints.find(sourceId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* source = it->second;

	//Attach
	return endpoint->Attach(media,source->GetJoinable(media));
}

int MediaSession::EndpointAttachToNetJoinable(int endpointId,NetJoinable* join,MediaFrame::Type media)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");
	//Get it
	Endpoint* endpoint = it->second;

	//Attach
	return endpoint->Attach(media,join);
}

int MediaSession::EndpointDettach(int endpointId,MediaFrame::Type media)
{
	//Get endpoint
	Endpoints::iterator it = endpoints.find(endpointId);

	//If not found
	if (it==endpoints.end())
		//Exit
		return Error("Endpoint not found");

	//Get it
	Endpoint* endpoint = it->second;

	//Log endpoint tag name
	Log("-EndpointDettach [%ls]",endpoint->GetName().c_str());

	//Dettach
	return endpoint->Dettach(media);
}


int MediaSession::GetAudioMixerCount()
{
	return audioMixers.size();
}

int MediaSession::AudioMixerCreate(std::wstring tag)
{
	//Create ID
	int audioMixerId = maxAudioMixerId++;
	//Create AudioMixer
	AudioMixerResource* audioMixer = new AudioMixerResource(tag);
	//Init it
	audioMixer->Init();
	//Append the mixer
	audioMixers[audioMixerId] = audioMixer;
	//Return it
	return audioMixerId;
}

int MediaSession::AudioMixerDelete(int mixerId)
{
	//Get AudioMixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Remove from list
	audioMixers.erase(it);

	//End it
	audioMixer->End();

	//Delete audioMixer
	delete(audioMixer);

	return 1;
}

int MediaSession::AudioMixerPortCreate(int mixerId,std::wstring tag)
{
	//Get AudioMixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Execute
	return audioMixer->CreatePort(tag);
}

int MediaSession::AudioMixerPortSetCodec(int mixerId,int portId,AudioCodec::Type codec)
{
	//Get AudioMixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Execute
	return audioMixer->SetPortCodec(portId,codec);
}

int MediaSession::AudioMixerPortDelete(int mixerId,int portId)
{
	//Get AudioMixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Execute
	return audioMixer->DeletePort(portId);
}


int MediaSession::AudioMixerPortAttachToEndpoint(int mixerId,int portId,int endpointId)
{
	//Get AudioMixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Get endpoint
	Endpoints::iterator itEnd = endpoints.find(endpointId);

	//If not found
	if (itEnd==endpoints.end())
		//Exit
		return Error("Endpoint not found");

	//Get it
	Endpoint* endpoint = itEnd->second;

	//Log endpoint tag name
	Log("-AudioMixerPortAttachToEndpoint [%ls]",endpoint->GetName().c_str());

	//Attach
	return audioMixer->Attach(portId,endpoint->GetJoinable(MediaFrame::Audio));
}

int MediaSession::AudioMixerPortAttachToPlayer(int mixerId,int portId,int playerId)
{
	//Get mixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Get Player
	Players::iterator itPlayer = players.find(playerId);

	//If not found
	if (itPlayer==players.end())
		//Exit
		return Error("Player not found");

	//Get it
	Player* player = itPlayer->second;
	audioMixer->SetStreamer(portId, player);

	//Attach
	return audioMixer->Attach(portId,player->GetJoinable(MediaFrame::Audio));
}

int MediaSession::AudioMixerPortAttachToNetJoinable(int mixerId,int portId,NetJoinable* join)
{
	//Get mixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	if(join->GetJoinId().GetType() == NET_PlayerAudio) {
		audioMixer->SetStreamer(portId, join);
	}

	//Attach
	return audioMixer->Attach(portId,join);
}

int MediaSession::AudioMixerPortDettach(int mixerId,int portId)
{
	//Get mixer
	AudioMixers::iterator it = audioMixers.find(mixerId);

	//If not found
	if (it==audioMixers.end())
		//Exit
		return Error("AudioMixerResource not found");
	//Get it
	AudioMixerResource* audioMixer = it->second;

	//Dettach
	return audioMixer->Dettach(portId);
}


int MediaSession::GetAudioTranscoderCount()
{
	return audioTranscoders.size();
}

int MediaSession::AudioTranscoderCreate(std::wstring tag)
{
	// Create ID
	int audioTranscoderId = maxAudioTranscoderId++;
	// Create transcoder
	AudioTranscoder* audioTranscoder = new AudioTranscoder(tag);
	// Init
	audioTranscoder->Init();
	audioTranscoders[audioTranscoderId] = audioTranscoder;
	return audioTranscoderId;
}

int MediaSession::AudioTranscoderSetCodec(int transcoderId, AudioCodec::Type codec)
{
	// Get
	AudioTranscoders::iterator it = audioTranscoders.find(transcoderId);

	// If not found
	if(it == audioTranscoders.end())
		// Exit
		return Error("AudioTranscoder not found [%d]", transcoderId);

	// Get it
	AudioTranscoder* audioTranscoder = it->second;

	// Execute
	return audioTranscoder->SetCodec(codec);
}

int MediaSession::AudioTranscoderDelete(int transcoderId)
{
	// Get
	AudioTranscoders::iterator it = audioTranscoders.find(transcoderId);

	// If not found
	if(it == audioTranscoders.end())
		// Exit
		return Error("AudioTranscoder not found [%d]", transcoderId);

	// Get it
	AudioTranscoder* audioTranscoder = it->second;

	// Remove from list
	audioTranscoders.erase(it);

	// End it
	audioTranscoder->End();

	// Delete
	delete(audioTranscoder);

	return 1;
}

int MediaSession::AudioTranscoderAttachToEndpoint(int transcoderId, int endpointId)
{
	// Get
	AudioTranscoders::iterator it = audioTranscoders.find(transcoderId);

	// If not found
	if(it == audioTranscoders.end())
		// Exit
		return Error("AudioTranscoder not found [%d]", transcoderId);

	// Get it
	AudioTranscoder* audioTranscoder = it->second;

	//Get endpoint
	Endpoints::iterator itEnd = endpoints.find(endpointId);

	//If not found
	if (itEnd==endpoints.end())
		//Exit
		return Error("Endpoint not found [%d]",endpointId);

	//Get it
	Endpoint* endpoint = itEnd->second;

	//Log endpoint tag name
	Log("-AudioTranscoderAttachToEndpoint [transcoder:%ls,endpoint:%ls]",audioTranscoder->GetName().c_str(),endpoint->GetName().c_str());

	// Attch
	return audioTranscoder->Attach(endpoint->GetJoinable(MediaFrame::Audio));
}

int MediaSession::AudioTranscoderAttachToPlayer(int transcoderId, int playerId)
{
	// Get
	AudioTranscoders::iterator it = audioTranscoders.find(transcoderId);
	// If not found
	if(it == audioTranscoders.end())
		return Error("AudioTranscoder not found [%d]", transcoderId);
	// Get it
	AudioTranscoder *audioTranscoder = it->second;

	// Get Player
	Players::iterator itPlayer = players.find(playerId);
	// If not found
	if(itPlayer == players.end())
		return Error("Player not found [%d]", playerId);
	//Get it
	Player *player = itPlayer->second;
	audioTranscoder->SetStreamer(player);

	// Log
	Log("-AudioTranscoderAttachToPlayer [transcoder:%ls,player:%ls]", audioTranscoder->GetName().c_str(), player->GetTag().c_str());

	// Attch
	return audioTranscoder->Attach(player->GetJoinable(MediaFrame::Audio));
}

int MediaSession::AudioTranscoderAttachToNetJoinable(int transcoderId,NetJoinable* join)
{
	// Get
	AudioTranscoders::iterator it = audioTranscoders.find(transcoderId);
	// If not found
	if(it == audioTranscoders.end())
		return Error("AudioTranscoder not found [%d]", transcoderId);
	// Get it
	AudioTranscoder *audioTranscoder = it->second;

	if(join->GetJoinId().GetType() == NET_PlayerAudio) {
		audioTranscoder->SetStreamer(join);
	}

	// Attch
	return audioTranscoder->Attach(join);
}

int MediaSession::AudioTranscoderDettach(int transcoderId)
{
	// Get
	AudioTranscoders::iterator it = audioTranscoders.find(transcoderId);

	// If not found
	if(it == audioTranscoders.end())
		// Exit
		return Error("AudioTranscoder not found [%d]", transcoderId);

	// Get it
	AudioTranscoder* audioTranscoder = it->second;

	// Dettach
	return audioTranscoder->Dettach();
}


int MediaSession::GetVideoMixerCount()
{
	return videoMixers.size();
}

int MediaSession::VideoMixerCreate(std::wstring tag)
{
	//Create ID
	int videoMixerId = maxVideoMixerId++;
	//Create mixer
	VideoMixerResource* videoMixer = new VideoMixerResource(tag);
	//Init
	videoMixer->Init(Mosaic::mosaic2x2,PAL);
	//Append the mixer
	videoMixers[videoMixerId] = videoMixer;
	//Return it
	return videoMixerId;
}

int MediaSession::VideoMixerDelete(int mixerId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Remove from list
	videoMixers.erase(it);

	//End it
	videoMixer->End();

	//Delete videoMixer
	delete(videoMixer);

	return 1;
}

int MediaSession::VideoMixerPortCreate(int mixerId,std::wstring tag, int mosaicId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->CreatePort(tag,mosaicId);
}

int MediaSession::VideoMixerPortSetName(int mixerId, int portId, std::wstring name)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);
	
	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;
	
	//Execute
	return videoMixer->SetPortName(portId, name);
}

int MediaSession::VideoMixerPortSetCodec(int mixerId,int portId,VideoCodec::Type codec,int size,int fps,int bitrate,int intraPeriod,const Properties& properties)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->SetPortCodec(portId,codec,size,fps,bitrate,intraPeriod,properties);
}

int MediaSession::VideoMixerPortDelete(int mixerId,int portId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->DeletePort(portId);
}


int MediaSession::VideoMixerPortAttachToEndpoint(int mixerId,int portId,int endpointId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Get endpoint
	Endpoints::iterator itEnd = endpoints.find(endpointId);

	//If not found
	if (itEnd==endpoints.end())
		//Exit
		return Error("Endpoint not found");

	//Get it
	Endpoint* endpoint = itEnd->second;

	//Log endpoint tag name
	Log("-VideoMixerPortAttachToEndpoint [%ls]",endpoint->GetName().c_str());

	//Attach
	return videoMixer->Attach(portId,endpoint->GetJoinable(MediaFrame::Video));
}

int MediaSession::VideoMixerPortAttachToPlayer(int mixerId,int portId,int playerId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Get Player
	Players::iterator itPlayer = players.find(playerId);

	//If not found
	if (itPlayer==players.end())
		//Exit
		return Error("Player not found");

	//Get it
	Player* player = itPlayer->second;
	videoMixer->SetStreamer(portId, player);

	//Attach
	return videoMixer->Attach(portId,player->GetJoinable(MediaFrame::Video));
}

int MediaSession::VideoMixerPortAttachToVideoMixerPort(int mixerId, int portId, int joinMixerId, int joinPortId)
{
	VideoMixers::iterator it = videoMixers.find(mixerId);
	if(it == videoMixers.end()) {
		return Error("VideoMixerResource not found [%d]", mixerId);
	}
	VideoMixerResource *videoMixer = it->second;

	it = videoMixers.find(joinMixerId);
	if(it == videoMixers.end()) {
		return Error("VideoMixerResource not found [%d]", mixerId);
	}
	VideoMixerResource *joinVideoMixer = it->second;

	return videoMixer->Attach(portId, joinVideoMixer->GetJoinable(joinPortId));
}

int MediaSession::VideoMixerPortAttachToNetJoinable(int mixerId,int portId,NetJoinable* join)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	if(join->GetJoinId().GetType() == NET_PlayerVideo) {
		videoMixer->SetStreamer(portId, join);
	}

	//Attach
	return videoMixer->Attach(portId,join);
}

int MediaSession::VideoMixerPortDettach(int mixerId,int portId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Dettach
	return videoMixer->Dettach(portId);
}

int MediaSession::VideoMixerMosaicCreate(int mixerId,Mosaic::Type comp,int size)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->CreateMosaic(comp,size);
}

int MediaSession::VideoMixerMosaicDelete(int mixerId,int mosaicId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->DeleteMosaic(mosaicId);
}

int MediaSession::VideoMixerMosaicSetSlot(int mixerId,int mosaicId,int num,int portId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->SetSlot(mosaicId,num,portId);
}

int MediaSession::VideoMixerMosaicSetCompositionType(int mixerId,int mosaicId,Mosaic::Type comp,int size)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->SetCompositionType(mosaicId,comp,size);
}

int MediaSession::VideoMixerMosaicSetOverlayPNG(int mixerId,int mosaicId,const char* overlay)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->SetOverlayPNG(mosaicId,overlay);
}

int MediaSession::VideoMixerMosaicResetSetOverlay(int mixerId,int mosaicId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->ResetOverlay(mosaicId);
}

int MediaSession::VideoMixerMosaicFadeInOverlay(int mixerId, int mosaicId, int ms)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if(it == videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]", mixerId);
	//Get it
	VideoMixerResource *videoMixer = it->second;

	//Execute
	return videoMixer->FadeInOverlay(mosaicId, ms);
}

int MediaSession::VideoMixerMosaicAddPort(int mixerId,int mosaicId,int portId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->AddMosaicParticipant(mosaicId,portId);
}

int MediaSession::VideoMixerMosaicRemovePort(int mixerId,int mosaicId,int portId)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it==videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->RemoveMosaicParticipant(mosaicId,portId);
}

int MediaSession::VideoMixerMosaicMutePort(int mixerId, int mosaicId, int portId, bool mute)
{
	//Get mixer
	VideoMixers::iterator it = videoMixers.find(mixerId);

	//If not found
	if (it == videoMixers.end())
		//Exit
		return Error("VideoMixerResource not found [%d]",mixerId);
	//Get it
	VideoMixerResource* videoMixer = it->second;

	//Execute
	return videoMixer->MuteMosaicParticipant(mosaicId, portId, mute);
}

int MediaSession::GetVideoTranscoderCount()
{
	return videoTranscoders.size();
}

int MediaSession::VideoTranscoderCreate(std::wstring tag)
{
	//Create ID
	int videoTranscoderId = maxVideoTranscoderId++;
	//Create trascoder
	VideoTranscoder* videoTranscoder = new VideoTranscoder(tag);
	//Init
	videoTranscoder->Init();
	//Append the transcoder
	videoTranscoders[videoTranscoderId] = videoTranscoder;
	//Return it
	return videoTranscoderId;
}

int MediaSession::VideoTranscoderFPU(int videoTranscoderId)
{
	//Get transcoder
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);

	//If not found
	if (it==videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found [%d]",videoTranscoderId);
	//Get it
	VideoTranscoder* videoTranscoder = it->second;

	//Execute
	videoTranscoder->Update();

	//OK
	return 1;
}

int MediaSession::VideoTranscoderSetCodec(int videoTranscoderId,VideoCodec::Type codec,int size,int fps,int bitrate,int intraPeriod,Properties & props)
{
	//Get transcoder
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);

	//If not found
	if (it==videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found [%d]",videoTranscoderId);
	//Get it
	VideoTranscoder* videoTranscoder = it->second;

	//Execute
	return videoTranscoder->SetCodec(codec,size,fps,bitrate,intraPeriod,props);
}

int MediaSession::VideoTranscoderDelete(int videoTranscoderId)
{
	//Get transcoder
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);

	//If not found
	if (it==videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found [%d]",videoTranscoderId);
	//Get it
	VideoTranscoder* videoTranscoder = it->second;

	//Remove from list
	videoTranscoders.erase(it);

	//End it
	videoTranscoder->End();

	//Delete videoMixer
	delete(videoTranscoder);

	return 1;
}

int MediaSession::VideoTranscoderAttachToEndpoint(int videoTranscoderId,int endpointId)
{
	//Get transcoder
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);

	//If not found
	if (it==videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found [%d]",videoTranscoderId);
	//Get it
	VideoTranscoder* videoTranscoder = it->second;

	//Get endpoint
	Endpoints::iterator itEnd = endpoints.find(endpointId);

	//If not found
	if (itEnd==endpoints.end())
		//Exit
		return Error("Endpoint not found [%d]",endpointId);

	//Get it
	Endpoint* endpoint = itEnd->second;

	//Log endpoint tag name
	Log("-VideoTranscoderAttachToEndpoint [transcoder:%ls,endpoint:%ls]",videoTranscoder->GetName().c_str(),endpoint->GetName().c_str());

	//Attach
	return videoTranscoder->Attach(endpoint->GetJoinable(MediaFrame::Video));
}

int MediaSession::VideoTranscoderAttachToPlayer(int videoTranscoderId, int playerId)
{
	// Get
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);
	if(it == videoTranscoders.end())
		return Error("VideoTranscoder not found [%d]", videoTranscoderId);
	VideoTranscoder *videoTranscoder = it->second;

	// Get Player
	Players::iterator itPlayer = players.find(playerId);
	if(itPlayer == players.end())
		return Error("Player not found [%d]", playerId);
	Player *player = itPlayer->second;
	videoTranscoder->SetStreamer(player);

	// Log
	Log("-VideoTranscoderAttachToPlayer [transcoder:%ls,player=%ls]", videoTranscoder->GetName().c_str(), player->GetTag().c_str());

	// Attach
	return videoTranscoder->Attach(player->GetJoinable(MediaFrame::Video));
}

int MediaSession::VideoTranscoderAttachToNetJoinable(int videoTranscoderId,NetJoinable* join)
{
	// Get
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);
	if(it == videoTranscoders.end())
		return Error("VideoTranscoder not found [%d]", videoTranscoderId);
	VideoTranscoder *videoTranscoder = it->second;

	if(join->GetJoinId().GetType() == NET_PlayerVideo) {
		videoTranscoder->SetStreamer(join);
	}

	// Attach
	return videoTranscoder->Attach(join);
}

int MediaSession::VideoTranscoderDettach(int videoTranscoderId)
{
	//Get mixer
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);

	//If not found
	if (it==videoTranscoders.end())
		//Exit
		return Error("VideoTranscoder not found [%d]",videoTranscoderId);
	//Get it
	VideoTranscoder* videoTranscoder = it->second;

	//Dettach
	return videoTranscoder->Dettach();
}


void MediaSession::onNoRTPPacket(Endpoint* endpoint, MediaFrame::Type media, void* endpointId)
{
	if(listener)
		listener->onEndpointNoRTPPacket(this, endpoint, (intptr_t)endpointId, media, param);
}

void MediaSession::onVideoBandwidthState(Endpoint* endpoint, RemoteRateControl::BandwidthState state, void* endpointId)
{
	if(listener)
		listener->onEndpointVideoBandwidthState(this, endpoint, (intptr_t)endpointId, state, param);
}

void MediaSession::onVoiceActivityState(Endpoint* endpoint, RemoteVAD::VoiceState state, void* endpointId)
{
	if(listener)
		listener->onEndpointVoiceActivityState(this, endpoint, (intptr_t)endpointId, state, param);
}

void MediaSession::onEndOfFile(Player *player,void* playerId)
{
	//Check for listener
	if (listener)
		//Send
		listener->onPlayerEndOfFile(this,player,(intptr_t)playerId,param);
}


Player* MediaSession::GetPlayer(int playerId)
{
	Players::iterator it = players.find(playerId);

	if(it == players.end()) {
		return NULL;
	}
	Player *player = it->second;

	return player;
}

Joinable* MediaSession::GetJoinableOfPlayer(int playerId, MediaFrame::Type media)
{
	Players::iterator it = players.find(playerId);

	if(it == players.end()) {
		return NULL;
	}
	Player *player = it->second;

	return player->GetJoinable(media);
}

Joinable* MediaSession::GetJoinableOfEndpoint(int endpointId, MediaFrame::Type media)
{
	Endpoints::iterator it = endpoints.find(endpointId);

	if(it == endpoints.end()) {
		return NULL;
	}
	Endpoint *endpoint = it->second;

	return endpoint->GetJoinable(media);
}

Joinable* MediaSession::GetJoinableOfAudioTranscoder(int audioTranscoderId)
{
	AudioTranscoders::iterator it = audioTranscoders.find(audioTranscoderId);

	if(it == audioTranscoders.end()) {
		return NULL;
	}
	AudioTranscoder *audioTranscoder = it->second;

	return audioTranscoder;
}

Joinable* MediaSession::GetJoinableOfVideoTranscoder(int videoTranscoderId)
{
	VideoTranscoders::iterator it = videoTranscoders.find(videoTranscoderId);

	if(it == videoTranscoders.end()) {
		return NULL;
	}
	VideoTranscoder *videoTranscoder = it->second;

	return videoTranscoder;
}

Joinable* MediaSession::GetJoinableOfAudioMixerPort(int mixerId, int portId)
{
	AudioMixers::iterator it = audioMixers.find(mixerId);

	if(it == audioMixers.end()) {
		return NULL;
	}
	AudioMixerResource *audioMixer = it->second;

	return audioMixer->GetJoinable(portId);
}

Joinable* MediaSession::GetJoinableOfVideoMixerPort(int mixerId, int portId)
{
	VideoMixers::iterator it = videoMixers.find(mixerId);

	if(it == videoMixers.end()) {
		return NULL;
	}
	VideoMixerResource *videoMixer = it->second;

	return videoMixer->GetJoinable(portId);
}
