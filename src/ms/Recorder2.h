/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : Recorder2.h
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#ifndef RECORDER_H
#define RECORDER_H

#include "config.h"
#include <string>
#include "Joinable.h"
#include "mfrecorder.h"

class RecTrack :
	public Joinable::Listener
{
public:
	class Listener
	{
	public:
		virtual ~Listener() {};
	public:
		virtual void onRTPPacket(RTPPacket& packet) = 0;
	};
public:
	RecTrack(MediaFrame::Type type, Listener* listener);
	virtual ~RecTrack();

	//Joinable::Listener
	virtual void onRTPPacket(RTPPacket& packet);
	virtual void onResetStream();
	virtual void onEndStream();

	//Attach
	int Attach(Joinable* join);
	int Dettach();

private:
	Listener		*listener;
	Joinable		*joined;
	MediaFrame::Type type;
};

class Recorder :
	public MFRecorder,
	public RecTrack::Listener
{
public:
	Recorder(std::wstring tag);
	virtual ~Recorder();

	//RecTrack::Listener
	virtual void onRTPPacket(RTPPacket& packet);
	
	//Attach
	int Attach(MediaFrame::Type media, Joinable* join);
	int Dettach(MediaFrame::Type media);

private:
	RecTrack		*audioTrack;
	RecTrack		*videoTrack;

private:
	std::wstring	tag;
};

#endif	/* RECORDER_H */
