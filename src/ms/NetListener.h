/*
 * File:	NetListener.h
 * Author:	endryu
 */

#ifndef NETLISTENER_H
#define NETLISTENER_H

#include <set>
#include "NetJoinable.h"


/*
 * class NetListenerFactory
 */
class NetListener;
class NetListenerFactory
{
public:
	class Listener
	{
	public:
		virtual ~Listener() {}
		virtual void onNetRTPPacket(NetListener* netListener, in_addr_t node, uint32_t rtpPktPort, RTPPacket& packet, void* param) = 0;
		virtual void onNetResetStream(NetListener* netListener, in_addr_t node, void* param) = 0;
		virtual void onNetEndStream(NetListener* netListener, in_addr_t node, void* param) = 0;
	};
	
public:
	NetListenerFactory();
	~NetListenerFactory();

	void SetListener(NetListenerFactory::Listener* listener, void* param);
	NetListenerFactory::Listener* GetListener() { return listener; }
	void* GetListenerParam() { return param; }

	NetListener* GetInstance(in_addr_t node, NetJoinType type, int id, int port);
	NetListener* CreateInstance(in_addr_t node, NetJoinType type, int id, int port, Joinable* join);
	void RemoveInstance(NetListener* netListener);
	int GetCount();

private:
	typedef std::pair<NetJoinId, NetListener*> NetListenerPair;
	typedef std::map<NetJoinId, NetListener*> NetListeners;

protected:
	NetListeners	netListeners;

	NetListenerFactory::Listener *listener;
	void			*param;
};


/*
 * class NetListener
 */
class NetListener : public Joinable::Listener 
{
public:
	NetListener(NetJoinId& joinId, Joinable* join, NetListenerFactory* factory);
	virtual ~NetListener();

public:
	void StartListen();
	
	//From network
	void AddNetJoinable(in_addr_t node, uint32_t rtpPktPort);
	void RemoveNetJoinable(in_addr_t node);
	void UpdateNetJoinable();

public:
	//Joinable::Listener interface
	virtual void onRTPPacket(RTPPacket& packet);
	virtual void onResetStream();
	virtual void onEndStream();

public:
	NetJoinId GetJoinId() { return joinId; }
	void ResetFactory() { factory = NULL; }

private:
	typedef std::pair<in_addr_t, uint32_t> NetJoinAddrPair;
	typedef std::map<in_addr_t, uint32_t> NetJoinAddrs;

protected:
	NetJoinAddrs		netJoinAddrs;
	pthread_mutex_t		mutex;

	Joinable			*joined;
	bool				listening;

	NetJoinId			joinId;
	NetListenerFactory	*factory;

	NetListenerFactory::Listener *listener;
	void				*param;
};

#endif
