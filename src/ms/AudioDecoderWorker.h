/* 
 * File:   AudioDecoderWorker.h
 * Author: Sergio
 *
 * Created on 4 de octubre de 2011, 20:06
 */

#ifndef AUDIODECODERWORKER_H
#define	AUDIODECODERWORKER_H
#include "codecs.h"
#include "audio.h"
#include "waitqueue.h"
#include "Joinable.h"
#include "ffstreamer.h"

class AudioDecoderJoinableWorker:
	public Joinable::Listener
{
public:
	class DTMFListener {
	public:
		virtual ~DTMFListener() {};
	public:
		virtual void onDTMFPacket(RTPPacket &packet) = 0;
	};
	
	void setDTMFListener(DTMFListener* listener) { this->dtmflistener = listener; }
	
public:
	AudioDecoderJoinableWorker();
	virtual ~AudioDecoderJoinableWorker();

	int Init(AudioOutput *output);
	int End();

	//Virtuals from Joinable::Listener
	virtual void onRTPPacket(RTPPacket &packet);
	virtual void onResetStream();
	virtual void onEndStream();

	//Attach
	int Attach(Joinable *join);
	int Dettach();

	int SetStreamer(FFStreamer* streamer) { this->streamer = streamer; return 1; }

private:
	int Start();
	int Stop();
protected:
	int Decode();

private:
	static void *startDecoding(void *par);

private:
	AudioOutput *output;
	WaitQueue<RTPPacket*> packets;
	pthread_t thread;
	bool decoding;
	Joinable *joined;
	
	FFStreamer *streamer;
	DTMFListener *dtmflistener;

};

#endif	/* AUDIODECODERWORKER_H */

