/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : Recorder2.cpp
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#include "Recorder2.h"
#include "log.h"

RecTrack::RecTrack(MediaFrame::Type type, Listener* listener)
{
	this->listener = listener;
	joined = NULL;
	this->type = type;
}

RecTrack::~RecTrack()
{
	Dettach();
}

void RecTrack::onRTPPacket(RTPPacket& packet)
{
	listener->onRTPPacket(packet);
}

void RecTrack::onResetStream()
{
	//Do noting by now
}

void RecTrack::onEndStream()
{
	joined = NULL;
}

int RecTrack::Attach(Joinable* join)
{
	if(joined) {
		joined->RemoveListener(this);
	}

	joined = join;	
	if(joined) {
		joined->AddListener(this);
		if(type == MediaFrame::Video) joined->Update();
	}

	return 1;
}

int RecTrack::Dettach()
{
	if(joined) {
		joined->RemoveListener(this);
	}
	joined = NULL;

	return 1;
}

Recorder::Recorder(std::wstring tag)
{
	this->tag = tag;

	audioTrack = new RecTrack(MediaFrame::Audio, this);
	videoTrack = new RecTrack(MediaFrame::Video, this);
}

Recorder::~Recorder()
{
	delete audioTrack;
	delete videoTrack;
}

void Recorder::onRTPPacket(RTPPacket& packet)
{
	switch(packet.GetMediaType()) {
	case MediaFrame::Audio:
		onAudioRTPPacket(packet);
		break;
	case MediaFrame::Video:
		onVideoRTPPacket(packet);
		break;
	}
}

int Recorder::Attach(MediaFrame::Type media, Joinable* join)
{
	Log("-Recorder attaching [media:%d]", media);

	switch(media) {
	case MediaFrame::Audio:
		audioTrack->Attach(join);
		break;
	case MediaFrame::Video:
		videoTrack->Attach(join);
		break;
	}

	return 1;
}

int Recorder::Dettach(MediaFrame::Type media)
{
	Log("-Recorder dettaching [media:%d]", media);

	switch(media) {
	case MediaFrame::Audio:
		audioTrack->Dettach();
		break;
	case MediaFrame::Video:
		videoTrack->Dettach();
		break;
	}

	return 1;
}
