/*
 * File:	NetJoinable.h
 * Author:	endryu
 */

#ifndef NETJOINABLE_H
#define NETJOINABLE_H

#include <set>
#include "Joinable.h"
#include "ffstreamer.h"


typedef enum {
	NET_EPAudio, NET_EPVideo, NET_PlayerAudio, NET_PlayerVideo, NET_AudioTrans, NET_VideoTrans, NET_AudioMixPort, NET_VideoMixPort
} NetJoinType;

class NetJoinId {
public:
	NetJoinId() {}
	NetJoinId(in_addr_t node, NetJoinType type, int id, int port) {
		this->node = node;
		this->type = type;
		this->id = id;
		this->port = port;
	}
	
	in_addr_t	GetNode() { return node; }
	NetJoinType	GetType() { return type; }
	int			GetId() { return id; }
	int			GetPort() { return port; }

private:
	in_addr_t	node;
	NetJoinType	type;
	int			id;
	int			port;

public:
	bool operator<(const NetJoinId& o) const
	{
		return
			(node < o.node) ||
			(node == o.node && type < o.type) ||
			(node == o.node && type == o.type && id < o.id) ||
			(node == o.node && type == o.type && id == o.id && port < o.port);
	}
};


/*
 * class NetJoinableFactory
 */
class NetJoinable;
class NetJoinableFactory
{
public:
	class Listener
	{
	public:
		virtual ~Listener() {}
		virtual void onNewNetJoinable(NetJoinable* netJoinable, void* param) = 0;
		virtual void onDelNetJoinable(NetJoinable* netJoinable, void* param) = 0;
		virtual void onUpdateNetJoinable(NetJoinable* netJoinable, void* param) = 0;
	};
	
public:
	NetJoinableFactory();
	~NetJoinableFactory();

	void SetListener(NetJoinableFactory::Listener* listener, void* param);

	NetJoinable* GetInstance(in_addr_t node, NetJoinType type, int id, int port);
	NetJoinable* CreateInstance(in_addr_t node, NetJoinType type, int id, int port);
	void RemoveInstance(NetJoinable* netJoinable);
	void UpdateInstance(NetJoinable* netJoinable);
	int GetCount();

private:
	typedef std::pair<NetJoinId, NetJoinable*> NetJoinablePair;
	typedef std::map<NetJoinId, NetJoinable*> NetJoinables;

protected:
	NetJoinables	netJoinables;

	NetJoinableFactory::Listener *listener;
	void			*param;

};


/*
 * class NetJoinable
 */
class NetJoinable :
	public Joinable,
	public FFStreamer
{
public:
	NetJoinable(NetJoinId& joinId, NetJoinableFactory* factory);
	virtual ~NetJoinable();

public:
	//From network
	void onNetRTPPacket(RTPPacket& packet);
	void onNetResetStream();
	void onNetEndStream();

public:
	//Joinable interface
	virtual void AddListener(Listener* listener);
	virtual void RemoveListener(Listener* listener);
	virtual void Update();
	virtual void SetREMB(DWORD estimation);
	
	void SetUserPtr(void* ptr) { usrPtr = ptr; }
	void* GetUserPtr() { return usrPtr; }

public:
	NetJoinId GetJoinId() { return joinId; }
	void ResetFactory() { factory = NULL; }
	bool IsStreamEnd() { return streamEnd; }
	
public:
	void DeserializeCodecCtx(char* serial);

public:
	virtual AVCodecContext* GetAudioCodecCtx();
	virtual AVCodecContext* GetVideoCodecCtx();

private:
	typedef std::set<Joinable::Listener*> Listeners;

protected:
	Listeners			listeners;

	NetJoinId			joinId;
	NetJoinableFactory	*factory;
	bool				streamEnd;

protected:
	AVCodecContext		*codecCtx;
	void				*usrPtr;

};

#endif
