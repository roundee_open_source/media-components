/*
 * File:	NetJoinable.c
 * Author:	endryu
 */

#include "NetJoinable.h"


/*
 * class NetJoinableFactory
 */

NetJoinableFactory::NetJoinableFactory()
{
	listener = NULL;
	param = NULL;
}

NetJoinableFactory::~NetJoinableFactory()
{
	for(NetJoinables::iterator it = netJoinables.begin(); it != netJoinables.end(); ++it) {
		it->second->ResetFactory();
		delete it->second;
	}
	netJoinables.clear();
}

void NetJoinableFactory::SetListener(NetJoinableFactory::Listener* listener, void* param)
{
	this->listener = listener;
	this->param = param;
}

NetJoinable* NetJoinableFactory::GetInstance(in_addr_t node, NetJoinType type, int id, int port)
{
	NetJoinId	joinId(node, type, id, port);

	NetJoinables::iterator it = netJoinables.find(joinId);
	if(it == netJoinables.end()) {
		return NULL;
	}
	return it->second;
}

NetJoinable* NetJoinableFactory::CreateInstance(in_addr_t node, NetJoinType type, int id, int port)
{
	NetJoinId	joinId(node, type, id, port);
	NetJoinable	*netJoinable;
	bool		ret;

	Log("-NetJoinable CreateInstance [node=%d][type=%d][id=%d][port=%d]", node, type, id, port);

	netJoinable = new NetJoinable(joinId, this);
	ret = netJoinables.insert(NetJoinablePair(joinId, netJoinable)).second;
	if(!ret) {
		Error("Insert fail!!!");
		netJoinable->ResetFactory();
		delete netJoinable;
		return NULL;
	}

	if(listener) {
		listener->onNewNetJoinable(netJoinable, param);
	}

	return netJoinable;
}

void NetJoinableFactory::RemoveInstance(NetJoinable* netJoinable)
{
	NetJoinId	joinId = netJoinable->GetJoinId();

	Log("-NetJoinable RemoveInstance [node=%d][type=%d][id=%d][port=%d]",
			joinId.GetNode(), joinId.GetType(), joinId.GetId(), joinId.GetPort());

	if(listener) {
		listener->onDelNetJoinable(netJoinable, param);
	}

	NetJoinables::iterator it = netJoinables.find(joinId);
	if(it != netJoinables.end()) {
		netJoinables.erase(it);
	}
	else {
		Error("-can't find NetJoinable Instance");
	}

	return;
}

void NetJoinableFactory::UpdateInstance(NetJoinable* netJoinable)
{
	if(listener) listener->onUpdateNetJoinable(netJoinable, param);
}

int NetJoinableFactory::GetCount()
{
	return netJoinables.size();
}


/*
 * class NetJoinable
 */

NetJoinable::NetJoinable(NetJoinId& joinId, NetJoinableFactory* factory)
{
	this->joinId = joinId;
	this->factory = factory;
	streamEnd = false;

	codecCtx = NULL;
}

NetJoinable::~NetJoinable()
{
	if(codecCtx) {
		/*
		avcodec_close(codecCtx);
		*/
		avcodec_free_context(&codecCtx);
	}

	if(factory) factory->RemoveInstance(this);
}

void NetJoinable::onNetRTPPacket(RTPPacket& packet)
{
	for(Listeners::iterator it = listeners.begin(); it != listeners.end(); ++it) {
		(*it)->onRTPPacket(packet);
	}
	return;
}

void NetJoinable::onNetResetStream()
{
	for(Listeners::iterator it = listeners.begin(); it != listeners.end(); ++it) {
		(*it)->onResetStream();
	}
	return;
}

void NetJoinable::onNetEndStream()
{
	for(Listeners::iterator it = listeners.begin(); it != listeners.end(); ++it) {
		(*it)->onEndStream();
	}
	listeners.clear();
	streamEnd = true;

	delete this;

	return;
}

void NetJoinable::AddListener(Listener* listener)
{
	listener->onResetStream();
	listeners.insert(listener);

	return;
}

void NetJoinable::RemoveListener(Listener* listener)
{
	Listeners::iterator it = listeners.find(listener);
	//If present erase it
	if (it!=listeners.end()) {
		listeners.erase(it);
	}

	//If there ar no more
	if(listeners.empty()) {
		delete this;
	}
	return;
}

void NetJoinable::Update()
{
	if(factory) factory->UpdateInstance(this);
}

void NetJoinable::SetREMB(DWORD estimation)
{
	//Should be overriden
}

void NetJoinable::DeserializeCodecCtx(char* serial)
{
	if(codecCtx) return;

	codecCtx = avcodec_alloc_context3(NULL);

	//codec_type
	codecCtx->codec_type = *(AVMediaType*)serial;
	serial += sizeof(AVMediaType);

	//codec_id
	codecCtx->codec_id = *(AVCodecID*)serial;
	serial += sizeof(AVCodecID);

	//codec_tag
	codecCtx->codec_tag = *(unsigned int*)serial;
	serial += sizeof(unsigned int);

	//bit_rate
	codecCtx->bit_rate = *(int64_t*)serial;
	serial += sizeof(int64_t);

	//extradata_size & extradata
	codecCtx->extradata_size = *(int*)serial;
	serial += sizeof(int);
	if(codecCtx->extradata_size > 0) {
		codecCtx->extradata = (uint8_t*)av_mallocz(codecCtx->extradata_size);
		memcpy(codecCtx->extradata, serial, codecCtx->extradata_size);
		serial += codecCtx->extradata_size;
	}

	//time_base
	codecCtx->time_base = *(AVRational*)serial;
	serial += sizeof(AVRational);

	//ticks_per_frame
	codecCtx->ticks_per_frame = *(int*)serial;
	serial += sizeof(int);

	//width
	codecCtx->width = *(int*)serial;
	serial += sizeof(int);

	//height
	codecCtx->height = *(int*)serial;
	serial += sizeof(int);

	//coded_width
	codecCtx->coded_width = *(int*)serial;
	serial += sizeof(int);

	//coded_height
	codecCtx->coded_height = *(int*)serial;
	serial += sizeof(int);

	//gop_size
	codecCtx->gop_size = *(int*)serial;
	serial += sizeof(int);

	//pix_fmt
	codecCtx->pix_fmt = *(AVPixelFormat*)serial;
	serial += sizeof(AVPixelFormat);

	//max_b_frames
	codecCtx->max_b_frames = *(int*)serial;
	serial += sizeof(int);

	//has_b_frames
	codecCtx->has_b_frames = *(int*)serial;
	serial += sizeof(int);

	//sample_aspect_ratio
	codecCtx->sample_aspect_ratio = *(AVRational*)serial;
	serial += sizeof(AVRational);

	//refs
	codecCtx->refs = *(int*)serial;
	serial += sizeof(int);

	//chroma_sample_location
	codecCtx->chroma_sample_location = *(AVChromaLocation*)serial;
	serial += sizeof(AVChromaLocation);

	//slices
	codecCtx->slices = *(int*)serial;
	serial += sizeof(int);

	//field_order
	codecCtx->field_order = *(AVFieldOrder*)serial;
	serial += sizeof(AVFieldOrder);

	//sample_rate
	codecCtx->sample_rate = *(int*)serial;
	serial += sizeof(int);

	//channels
	codecCtx->channels = *(int*)serial;
	serial += sizeof(int);

	//sample_fmt
	codecCtx->sample_fmt = *(AVSampleFormat*)serial;
	serial += sizeof(AVSampleFormat);

	//frame_size
	codecCtx->frame_size = *(int*)serial;
	serial += sizeof(int);

	//frame_number
	codecCtx->frame_number = *(int*)serial;
	serial += sizeof(int);

	//idct_algo
	codecCtx->idct_algo = *(int*)serial;
	serial += sizeof(int);

	//bits_per_coded_sample
	codecCtx->bits_per_coded_sample = *(int*)serial;
	serial += sizeof(int);

	//bits_per_raw_sample
	codecCtx->bits_per_raw_sample = *(int*)serial;
	serial += sizeof(int);

	//profile
	codecCtx->profile = *(int*)serial;
	serial += sizeof(int);

	//level
	codecCtx->level = *(int*)serial;
	serial += sizeof(int);

	//framerate
	codecCtx->framerate = *(AVRational*)serial;
	serial += sizeof(AVRational);

	//pkt_timebase
	codecCtx->pkt_timebase = *(AVRational*)serial;
	serial += sizeof(AVRational);

	/*
	AVCodec *codec = avcodec_find_decoder(codecCtx->codec_id);
	avcodec_open2(codecCtx, codec, NULL);
	*/
}

AVCodecContext* NetJoinable::GetAudioCodecCtx()
{
	if(codecCtx && codecCtx->codec_type == AVMEDIA_TYPE_AUDIO) {
		return codecCtx;
	}
	return NULL;
}

AVCodecContext* NetJoinable::GetVideoCodecCtx()
{
	if(codecCtx && codecCtx->codec_type == AVMEDIA_TYPE_VIDEO) {
		return codecCtx;
	}
	return NULL;
}
