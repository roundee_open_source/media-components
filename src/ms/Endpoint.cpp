/* 
 * File:   Endpoint.cpp
 * Author: Sergio
 * 
 * Created on 7 de septiembre de 2011, 0:59
 */
#include "log.h"
#include "Endpoint.h"

Endpoint::Endpoint(std::wstring name,bool audioSupported,bool videoSupported,bool textSupported) : estimator(), vad()
{
	//Store name
	this->name = name;
	//Nullify
	audio = NULL;
	video = NULL;
	text = NULL;
	//If audio
	if (audioSupported)
		//Create endpoint
		audio = new RTPEndpoint(MediaFrame::Audio);
	//If video
	if (videoSupported)
		//Create endpoint
		video = new RTPEndpoint(MediaFrame::Video);
	//If video
	if (textSupported)
		//Create endpoint
		text = new RTPEndpoint(MediaFrame::Text);
}

Endpoint::~Endpoint()
{
	vad.Reset();

	//If endpoint for audio
	if (audio)
		//Delete endpoint
		delete(audio);
	//If endpoint for video
	if (video)
		//Delete endpoint
		delete(video);
	//If endpoint for audio
	if (text)
		//Delete endpoint
		delete(text);

}

void Endpoint::SetListener(Endpoint::Listener* listener, void* param)
{
	this->listener = listener;
	this->param = param;

	if(audio) audio->SetListener(this);
	if(video) video->SetListener(this);
}

void Endpoint::onNoRTPPacket(RTPEndpoint* rtpep)
{
	if(listener) {
		if(rtpep == audio) {
			vad.Reset();
			listener->onNoRTPPacket(this, MediaFrame::Audio, param);
		}
		else if(rtpep == video) {
			listener->onNoRTPPacket(this, MediaFrame::Video, param);
		}
	}
}

void Endpoint::onVideoBandwidthState(RTPEndpoint* rtpep, RemoteRateControl::BandwidthState state)
{
	if(listener) {
		listener->onVideoBandwidthState(this, state, param);
	}
}

void Endpoint::onVoiceActivityState(RTPEndpoint* rtpep, RemoteVAD::VoiceState state)
{
	if(listener) {
		listener->onVoiceActivityState(this, state, param);
	}
}

//Methods
int Endpoint::Init()
{
	//If endpoint for audio
	if (audio) {
		//Init it
		audio->Init();
		audio->SetRemoteVAD(&vad);
	}
	//If endpoint for video
	if (video)
	{
		//Init it
		video->Init();
		//Set estimator
		//estimator.SetTemporalMaxLimit(500000);	// default, 500 kbps
		video->SetRemoteRateEstimator(&estimator);
	}
	//If endpoint for audio
	if (text)
		//Init it
		text->Init();

	return 1;
}

int Endpoint::SetMaxLimit(int kbps)
{
	if(video) {
		estimator.SetTemporalMaxLimit(kbps * 1000);
	}
	return 1;
}

int Endpoint::SetVADLevel(int decibel)
{
	if(audio) {
		vad.SetVADLevel(decibel);
	}
	return 1;
}

/* Change From ===>
*/
int Endpoint::GetPorts(int* audioPort,int* videoPort,int* textPort)
{
	*audioPort = (audio)? audio->GetLocalPort() : 0;
	*videoPort = (video)? video->GetLocalPort() : 0;
	*textPort = (text)? text->GetLocalPort() : 0;

	return 1;
}
// <=== To

int Endpoint::End()
{
	Log(">End endpoint");

	//If endpoint for audio
	if (audio)
		//Init it
		audio->End();
	//If endpoint for video
	if (video)
		//Init it
		video->End();
	//If endpoint for audio
	if (text)
		//Init it
		text->End();

	Log("<End endpoint");
	return 1;
}

int Endpoint::StartSending(MediaFrame::Type media,char *sendIp,int sendPort,RTPMap& rtpMap)
{
	//Get enpoint
	RTPEndpoint* rtp = GetRTPEndpoint(media);
	
	//Check 
	if (!rtp)
		//Init it
		return Error("No media supported");

	//Stop sending for a while
	rtp->StopSending();

	//Set remote endpoint
	if(!rtp->SetRemotePort(sendIp,sendPort))
		//Error
		return Error("Error SetRemotePort");

	//Set sending map
	rtp->SetSendingRTPMap(rtpMap, rtpMap);

	//And send
	return rtp->StartSending();
}

int Endpoint::StopSending(MediaFrame::Type media)
{
	//Get rtp enpoint for media
	RTPEndpoint* rtp = GetRTPEndpoint(media);

	//Check
	if (!rtp)
		//Init it
		return Error("No media supported");
	
	//Stop sending for a while
	return rtp->StopSending();
}

int Endpoint::StartReceiving(MediaFrame::Type media,RTPMap& rtpMap)
{
	Log("-StartReceiving endpoint [name:%ls,media:%s]",name.c_str(),MediaFrame::TypeToString(media));
	
	//Get rtp enpoint for media
	RTPEndpoint* rtp = GetRTPEndpoint(media);

	//Check
	if (!rtp)
		//Init it
		return Error("No media supported");
	
	//Set map
	rtp->SetReceivingRTPMap(rtpMap, rtpMap);
	
	//Start
	if (!rtp->StartReceiving())
		//Exit
		return Error("Error starting receiving media");

	//Return port
	return rtp->GetLocalPort();
}

int Endpoint::StopReceiving(MediaFrame::Type media)
{
	//Get rtp enpoint for media
	RTPEndpoint* rtp = GetRTPEndpoint(media);

	//Check
	if (!rtp)
		//Init it
		return Error("No media supported");
	//Start
	return rtp->StopReceiving();
}

//Attach
int Endpoint::Attach(MediaFrame::Type media, Joinable *join)
{
	Log("-Endpoint attaching [media:%d]",media);
	
	//Depending on the type
	switch (media)
	{
		case MediaFrame::Audio:
			//Check if audio is supported
			if (!audio)
				//Error
				return Error("Audio not supported");
			//Attach audio
			return audio->Attach(join);
		case MediaFrame::Video:
			//Check if video is supported
			if (!video)
				//Error
				return Error("Video not supported");
			//Attach audio
			return video->Attach(join);
		case MediaFrame::Text:
			//Check if text is supported
			if (!text)
				//Error
				return Error("Text not supported");
			//Attach audio
			return text->Attach(join);
	}

	return 1;
}

int Endpoint::Dettach(MediaFrame::Type media)
{
	Log("-Endpoint detaching [media:%d]",media);

	//Depending on the type
	switch (media)
	{
		case MediaFrame::Audio:
			//Check if audio is supported
			if (!audio)
				//Error
				return Error("Audio not supported");
			//Attach audio
			return audio->Dettach();
		case MediaFrame::Video:
			//Check if video is supported
			if (!video)
				//Error
				return Error("Video not supported");
			//Attach audio
			return video->Dettach();
		case MediaFrame::Text:
			//Check if text is supported
			if (!text)
				//Error
				return Error("Text not supported");
			//Attach audio
			return text->Dettach();
	}

	return 1;
}
Joinable* Endpoint::GetJoinable(MediaFrame::Type media)
{
	//Depending on the type
	switch (media) {
	case MediaFrame::Audio:
		return audio;
	case MediaFrame::Video:
		return video;
	case MediaFrame::Text:
		return text;
	}

	//Uhh?
	return NULL;
}


int Endpoint::RequestUpdate(MediaFrame::Type media)
{
	//Depending on the type
	switch (media)
	{
		case MediaFrame::Audio:
			//Check
			if (audio)
				// audio
				return audio->RequestUpdate();
		case MediaFrame::Video:
			//Check
			if (video)
				//Attach audio
				return video->RequestUpdate();
		case MediaFrame::Text:
			//Check
			if (text)
				//text
				return text->RequestUpdate();
		default:
			return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}

int Endpoint::StartAudioCapture(const char* filename)
{
	if(audio)
		audio->StartCapture(filename);

	return 1;
}

int Endpoint::SetLocalCryptoSDES(MediaFrame::Type media,const char* suite, const char* key)
{
	switch (media)
	{
		case MediaFrame::Audio:
			if (audio)
				return audio->SetLocalCryptoSDES(suite,key);
		case MediaFrame::Video:
			if (video)
				return video->SetLocalCryptoSDES(suite,key);
		case MediaFrame::Text:
			if (text)
				return text->SetLocalCryptoSDES(suite,key);
		default:
			return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}

int Endpoint::SetRemoteCryptoDTLS(MediaFrame::Type media,const char *setup,const char *hash,const char *fingerprint)
{
	switch (media)
	{
		case MediaFrame::Audio:
			return audio->SetRemoteCryptoDTLS(setup,hash,fingerprint);
		case MediaFrame::Video:
			return video->SetRemoteCryptoDTLS(setup,hash,fingerprint);
		case MediaFrame::Text:
			return text->SetRemoteCryptoDTLS(setup,hash,fingerprint);
		default:
			return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}

int Endpoint::SetRemoteCryptoSDES(MediaFrame::Type media,const char* suite, const char* key)
{
	switch (media)
	{
		case MediaFrame::Audio:
			return audio->SetRemoteCryptoSDES(suite,key);
		case MediaFrame::Video:
			return video->SetRemoteCryptoSDES(suite,key);
		case MediaFrame::Text:
			return text->SetRemoteCryptoSDES(suite,key);
		default:
			return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}

int Endpoint::SetLocalSTUNCredentials(MediaFrame::Type media,const char* username, const char* pwd)
{
	switch (media)
	{
		case MediaFrame::Audio:
			return audio->SetLocalSTUNCredentials(username,pwd);
		case MediaFrame::Video:
			return video->SetLocalSTUNCredentials(username,pwd);
		case MediaFrame::Text:
			return text->SetLocalSTUNCredentials(username,pwd);
		default:
			return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}

int Endpoint::SetRTPProperties(MediaFrame::Type media,const Properties& properties)
{
	switch(media) {
	case MediaFrame::Audio:
		return audio->SetProperties(properties);
	case MediaFrame::Video:
		return video->SetProperties(properties);
	case MediaFrame::Text:
		return text->SetProperties(properties);
	default:
		return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}

int Endpoint::SetRemoteSTUNCredentials(MediaFrame::Type media,const char* username, const char* pwd)
{
	switch (media)
	{
		case MediaFrame::Audio:
			return audio->SetRemoteSTUNCredentials(username,pwd);
		case MediaFrame::Video:
			return video->SetRemoteSTUNCredentials(username,pwd);
		case MediaFrame::Text:
			return text->SetRemoteSTUNCredentials(username,pwd);
		default:
			return Error("Unknown media [%d]",media);
	}

	//OK
	return 1;
}
