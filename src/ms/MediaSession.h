
#ifndef MEDIASESSION_H
#define	MEDIASESSION_H

#include "config.h"
#include "media.h"
#include "codecs.h"
#include "video.h"
//#include "Player.h"
#include "Player2.h"
//#include "Recorder.h"
#include "Recorder2.h"
#include "Endpoint.h"
#include "AudioMixerResource.h"
#include "AudioTranscoder.h"
#include "VideoMixerResource.h"
#include "VideoTranscoder.h"
#include "NetJoinable.h"

class MediaSession :
	public Endpoint::Listener,
	public Player::Listener
{
public:
	class Listener
	{
	public:
		//Virtual desctructor
		virtual ~Listener() {}
	public:
		//Interface
		virtual void onEndpointNoRTPPacket(MediaSession* sess, Endpoint* endpoint, int endpointId, MediaFrame::Type media, void* param) = 0;
		virtual void onEndpointVideoBandwidthState(MediaSession* sess, Endpoint* endpoint, int endpointId, RemoteRateControl::BandwidthState state, void* param) = 0;
		virtual void onEndpointVoiceActivityState(MediaSession* sess, Endpoint* endpoint, int endpointId, RemoteVAD::VoiceState state, void* param) = 0;
		virtual void onPlayerEndOfFile(MediaSession* sess, Player* player, int playerId, void* param) = 0;
	};

public:
	MediaSession(std::wstring tag);
	~MediaSession();

	void SetListener(MediaSession::Listener *listener,void* param);

	int Init();
	int End();

	//Player management
	int GetPlayerCount();
	int PlayerCreate(std::wstring tag);
	int PlayerDelete(int playerId);
	//Player functionality
	int PlayerOpen(int playerId,const char* filename);
	int PlayerGetDuration(int playerId, DWORD* duration);
	int PlayerPlay(int playerId);
	int PlayerPause(int playerId);
	int PlayerResume(int playerId);
	int PlayerSeek(int playerId,QWORD time);
	int PlayerStop(int playerId);
	int PlayerClose(int playerId);

	//Recorder management
	int GetRecorderCount();
	int RecorderCreate(std::wstring tag,const char* filename,int size,int fps,int bitrate,int intraPeriod);
	int RecorderDelete(int recorderId);
	//Recorder functionality
	int RecorderRecord(int recorderId);
	int RecorderStop(int recorderId);

	//Join other objects
	int RecorderAttachToEndpoint(int recorderId,int endpointId,MediaFrame::Type media);
	/* Change From ===>
	*/
	int RecorderAttachToAudioTranscoder(int recorderId,int transcoderId);
	int RecorderAttachToVideoTranscoder(int recorderId,int transcoderId);
	// <=== To
	int RecorderAttachToAudioMixerPort(int recorderId,int mixerId,int portId);
	int RecorderAttachToVideoMixerPort(int recorderId,int mixerId,int portId);
	int RecorderAttachToNetJoinable(int recorderId,NetJoinable* join,MediaFrame::Type media);
	int RecorderDettach(int recorderId,MediaFrame::Type media);
	

	//Endpoint management
	int GetEndpointCount();
	int EndpointCreate(std::wstring name,bool audioSupported,bool videoSupported,bool textSupported);
	int EndpointGetPorts(int id,int* audioPort,int* videoPort,int* textPort);
	int EndpointDelete(int endpointId);
	int EndpointSetMaxLimit(int endpointId, int kbps);
	int EndpointSetVADLevel(int endpointId, int decibel);
	int EndpointSetLocalCryptoSDES(int id,MediaFrame::Type media,const char *suite,const char* key);
	int EndpointSetRemoteCryptoSDES(int id,MediaFrame::Type media,const char *suite,const char* key);
	int EndpointSetRemoteCryptoDTLS(int id,MediaFrame::Type media,const char *setup,const char *hash,const char *fingerprint);
	int EndpointSetLocalSTUNCredentials(int id,MediaFrame::Type media,const char *username,const char* pwd);
	int EndpointSetRemoteSTUNCredentials(int id,MediaFrame::Type media,const char *username,const char* pwd);
	int EndpointSetRTPProperties(int id,MediaFrame::Type media,const Properties& properties);
	//Endpoint Video functionality
	int EndpointStartSending(int endpointId,MediaFrame::Type media,char *sendVideoIp,int sendVideoPort,RTPMap& rtpMap);
	int EndpointStopSending(int endpointId,MediaFrame::Type media);
	int EndpointStartReceiving(int endpointId,MediaFrame::Type media,RTPMap& rtpMap);
	int EndpointStopReceiving(int endpointId,MediaFrame::Type media);

	int EndpointRequestUpdate(int endpointId,MediaFrame::Type media);
	int EndpointStartAudioCapture(int endpointId, const char* filename);
	//Attach intput to
	int EndpointAttachToPlayer(int endpointId,int playerId,MediaFrame::Type media);
	int EndpointAttachToEndpoint(int endpointId,int sourceId,MediaFrame::Type media);
	int EndpointAttachToAudioMixerPort(int endpointId,int mixerId,int portId);
	int EndpointAttachToAudioTranscoder(int endpointId,int transcoderId);
	int EndpointDettach(int endpointId,MediaFrame::Type media);
	int EndpointAttachToVideoMixerPort(int endpointId,int mixerId,int portId);
	int EndpointAttachToVideoTranscoder(int endpointId,int transcoderId);
	int EndpointAttachToNetJoinable(int endpointId,NetJoinable* join,MediaFrame::Type media);

	//AudioMixer management
	int GetAudioMixerCount();
	int AudioMixerCreate(std::wstring tag);
	int AudioMixerDelete(int mixerId);
	//AudioMixer port management
	int AudioMixerPortCreate(int mixerId,std::wstring tag);
	int AudioMixerPortSetCodec(int mixerId,int portId,AudioCodec::Type codec);
	int AudioMixerPortDelete(int mixerId,int portId);
	//Filters type: 0=pre 1=post
	//int AudioMixerPortAddFilter(int mixerId,int portId,int type,...);
	//int AudioMixerPortUpdatedFilter(int mixerId,int portId,...);
	//int AudioMixerPortDeleteFilter(int mixerId,int portId,int filterId);
	//int AudioMixerPortClearFilters(int mixerId,int portId);
	//Port Attach  to
	int AudioMixerPortAttachToEndpoint(int mixerId,int portId,int endpointId);
	int AudioMixerPortAttachToPlayer(int mixerId,int portId,int playerId);
	int AudioMixerPortAttachToNetJoinable(int mixerId,int portId,NetJoinable* join);
	int AudioMixerPortDettach(int mixerId,int portId);

	// AudioTranscoder management
	int GetAudioTranscoderCount();
	int AudioTranscoderCreate(std::wstring tag);
	int AudioTranscoderDelete(int transcoderId);
	int AudioTranscoderSetCodec(int transcoderId,AudioCodec::Type codec);
	int AudioTranscoderAttachToEndpoint(int transcoderId,int endpointId);
	int AudioTranscoderAttachToPlayer(int transcoderId,int playerId);
	int AudioTranscoderAttachToNetJoinable(int transcoderId,NetJoinable* join);
	int AudioTranscoderDettach(int transcoderId);
	
	//Video Mixer management
	int GetVideoMixerCount();
	int VideoMixerCreate(std::wstring tag);
	int VideoMixerDelete(int mixerId);
	//Video mixer port management
	int VideoMixerPortCreate(int mixerId,std::wstring tag,int mosiacId);
	int VideoMixerPortSetName(int mixerId, int portId, std::wstring name);
	int VideoMixerPortSetCodec(int mixerId,int portId,VideoCodec::Type codec,int size,int fps,int bitrate,int intraPeriod,const Properties& properties);
	int VideoMixerPortDelete(int mixerId,int porId);
	int VideoMixerPortAttachToEndpoint(int mixerId,int portId,int endpointId);
	int VideoMixerPortAttachToPlayer(int mixerId,int portId,int playerId);
	int VideoMixerPortAttachToVideoMixerPort(int mixerId,int portId,int joinMixerId,int joinPortId);
	int VideoMixerPortAttachToNetJoinable(int mixerId,int portId,NetJoinable* join);
	int VideoMixerPortDettach(int mixerId,int portId);
	//Video mixer mosaic management
	int VideoMixerMosaicCreate(int mixerId,Mosaic::Type comp,int size);
	int VideoMixerMosaicDelete(int mixerId,int portId);
	int VideoMixerMosaicSetSlot(int mixerId,int mosaicId,int num,int portId);
	int VideoMixerMosaicSetCompositionType(int mixerId,int mosaicId,Mosaic::Type comp,int size);
	int VideoMixerMosaicSetOverlayPNG(int mixerId,int mosaicId,const char* overlay);
	int VideoMixerMosaicResetSetOverlay(int mixerId,int mosaicId);
	int VideoMixerMosaicFadeInOverlay(int mixerId, int mosaicId, int ms);
	int VideoMixerMosaicAddPort(int mixerId,int mosaicId,int portId);
	int VideoMixerMosaicRemovePort(int mixerId,int mosaicId,int portId);
	int VideoMixerMosaicMutePort(int mixerId, int mosaicId, int portId, bool mute);

	int GetVideoTranscoderCount();
	int VideoTranscoderCreate(std::wstring tag);
	int VideoTranscoderDelete(int transcoderId);
	int VideoTranscoderSetCodec(int transcoderId,VideoCodec::Type codec,int size,int fps,int bitrate,int intraPeriod,Properties & props);
	int VideoTranscoderFPU(int transcoderId);
	int VideoTranscoderAttachToEndpoint(int transcoderId,int endpointId);
	int VideoTranscoderAttachToPlayer(int transcoderId,int playerId);
	int VideoTranscoderAttachToNetJoinable(int transcoderId,NetJoinable* join);
	int VideoTranscoderDettach(int transcoderId);

	//Events
	virtual void onNoRTPPacket(Endpoint* endpoint, MediaFrame::Type media, void* param);
	virtual void onVideoBandwidthState(Endpoint* endpoint, RemoteRateControl::BandwidthState state, void* param);
	virtual void onVoiceActivityState(Endpoint* endpoint, RemoteVAD::VoiceState state, void* param);
	virtual void onEndOfFile(Player* player,void* param);

	//Getters
	std::wstring& GetTag() { return tag;	}

	Player* GetPlayer(int playerId);
	Joinable* GetJoinableOfPlayer(int playerId, MediaFrame::Type media);
	Joinable* GetJoinableOfEndpoint(int endpointId, MediaFrame::Type media);
	Joinable* GetJoinableOfAudioTranscoder(int audioTranscoderId);
	Joinable* GetJoinableOfVideoTranscoder(int videoTranscoderId);
	Joinable* GetJoinableOfAudioMixerPort(int mixerId, int portId);
	Joinable* GetJoinableOfVideoMixerPort(int mixerId, int portId);

private:
	typedef std::map<int,Endpoint*> Endpoints;
	typedef std::map<int,Recorder*> Recorders;
	typedef std::map<int,Player*> Players;
	typedef std::map<int,AudioMixerResource*> AudioMixers;
	typedef std::map<int,AudioTranscoder*> AudioTranscoders;
	typedef std::map<int,VideoMixerResource*> VideoMixers;
	typedef std::map<int,VideoTranscoder*> VideoTranscoders;
private:
	std::wstring tag;
	
	MediaSession::Listener *listener;
	void* param;

	Endpoints endpoints;
	int maxEndpointId;

	Players players;
	int maxPlayersId;

	Recorders recorders;
	int maxRecordersId;

	AudioMixers	audioMixers;
	int maxAudioMixerId;

	AudioTranscoders audioTranscoders;
	int maxAudioTranscoderId;

	VideoMixers	videoMixers;
	int maxVideoMixerId;

	VideoTranscoders videoTranscoders;
	int maxVideoTranscoderId;
 
};

#endif	/* MEDIASESSION_H */

