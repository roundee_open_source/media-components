/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : Player2.cpp
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#include "log.h"
#include "Player2.h"
#include "mfstreamer.h"

Player::Player(std::wstring tag) : MFStreamer(this)
{
	this->tag= tag;
}

void Player::SetListener(Player::Listener* listener, void* param)
{
	this->listener = listener;
	this->param = param;
}

Joinable* Player::GetJoinable(MediaFrame::Type media)
{
	switch(media) {
	case MediaFrame::Video:
		return &video;
	case MediaFrame::Audio:
		return &audio;
	}
}

void Player::onRTPPacket(RTPPacket& packet)
{
	switch(packet.GetMediaType()) {
	case MediaFrame::Video:
		video.Multiplex(packet);
		break;
	case MediaFrame::Audio:
		audio.Multiplex(packet);
		break;
	}
}

void Player::onEnd()
{
	//reset audio stream
	audio.ResetStream();
	//reset video stream
	video.ResetStream();

	//check for listener
	if(listener) {
		//send event
		listener->onEndOfFile(this, param);
	}
}

void Player::onMediaFrame(MediaFrame& frame)
{
}

void Player::onMediaFrame(DWORD ssrc, MediaFrame& frame)
{
}
