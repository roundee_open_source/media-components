/*
 * File:	NetListener.c
 * Author:	endryu
 */

#include "NetListener.h"


/*
 * class NetListenerFactory
 */

NetListenerFactory::NetListenerFactory()
{
	listener = NULL;
	param = NULL;
}

NetListenerFactory::~NetListenerFactory()
{
	for(NetListeners::iterator it = netListeners.begin(); it != netListeners.end(); ++it) {
		it->second->ResetFactory();
		delete it->second;
	}
	netListeners.clear();
}

void NetListenerFactory::SetListener(NetListenerFactory::Listener* listener, void* param)
{
	this->listener = listener;
	this->param = param;
}

NetListener* NetListenerFactory::GetInstance(in_addr_t node, NetJoinType type, int id, int port)
{
	NetJoinId	joinId(node, type, id, port);

	NetListeners::iterator it = netListeners.find(joinId);
	if(it == netListeners.end()) {
		return NULL;
	}
	return it->second;
}

NetListener* NetListenerFactory::CreateInstance(in_addr_t node, NetJoinType type, int id, int port, Joinable* join)
{
	NetJoinId	joinId(node, type, id, port);
	NetListener	*netListener;
	bool		ret;

	Log("-NetListener CreateInstance [node=%d][type=%d][id=%d][port=%d]", node, type, id, port);

	netListener = new NetListener(joinId, join, this);
	ret = netListeners.insert(NetListenerPair(joinId, netListener)).second;
	if(!ret) {
		Error("Insert fail!!!");
		netListener->ResetFactory();
		delete netListener;
		return NULL;
	}

	netListener->StartListen();
	return netListener;
}

void NetListenerFactory::RemoveInstance(NetListener* netListener)
{
	NetJoinId	joinId = netListener->GetJoinId();

	Log("-NetListener RemoveInstance [node=%d][type=%d][id=%d][port=%d]",
			joinId.GetNode(), joinId.GetType(), joinId.GetId(), joinId.GetPort());

	NetListeners::iterator it = netListeners.find(joinId);
	if(it != netListeners.end()) {
		netListeners.erase(it);
	}
	else {
		Error("-can't find NetListener Instance");
	}

	return;
}

int NetListenerFactory::GetCount()
{
	return netListeners.size();
}


/*
 * class NetListener
 */

NetListener::NetListener(NetJoinId& joinId, Joinable* join, NetListenerFactory* factory)
{
	//Create mutex
	pthread_mutex_init(&mutex,NULL);

	this->joinId = joinId;
	this->factory = factory;

	if(factory) {
		listener = factory->GetListener();
		param = factory->GetListenerParam();
	}

	joined = join;
	listening = false;
}

NetListener::~NetListener()
{
	if(factory) factory->RemoveInstance(this);

	//Destroy mutex
	pthread_mutex_destroy(&mutex);
}

void NetListener::AddNetJoinable(in_addr_t node, uint32_t rtpPktPort)
{
	pthread_mutex_lock(&mutex);

	Log("-AddNetJoinable [node=%d][port=%d]", node, rtpPktPort);
	netJoinAddrs.insert(NetJoinAddrPair(node, rtpPktPort));

	pthread_mutex_unlock(&mutex);
	return;
}

void NetListener::RemoveNetJoinable(in_addr_t node)
{
	pthread_mutex_lock(&mutex);

	Log("-RemoveNetJoinable [node=%d]", node);
	NetJoinAddrs::iterator it = netJoinAddrs.find(node);
	if(it != netJoinAddrs.end()) {
		netJoinAddrs.erase(it);
	}
	else {
		Error("-can't find joined node [node=%d]", node);
	}

	pthread_mutex_unlock(&mutex);

	if(netJoinAddrs.empty() && listening) {
		joined->RemoveListener(this);
		delete this;
	}
	return;
}

void NetListener::UpdateNetJoinable()
{
	if(listening) {
		joined->Update();
	}
}

void NetListener::StartListen()
{
	if(!listening) {
		joined->AddListener(this);
		listening = true;
	}
}

void NetListener::onRTPPacket(RTPPacket& packet)
{
	if(listener) {
		pthread_mutex_lock(&mutex);

		for(NetJoinAddrs::iterator it = netJoinAddrs.begin(); it != netJoinAddrs.end(); ++it) {
			listener->onNetRTPPacket(this, it->first, it->second, packet, param);
		}

		pthread_mutex_unlock(&mutex);
	}
	return;
}

void NetListener::onResetStream()
{
	if(listener) {
		pthread_mutex_lock(&mutex);

		for(NetJoinAddrs::iterator it = netJoinAddrs.begin(); it != netJoinAddrs.end(); ++it) {
			listener->onNetResetStream(this, it->first, param);
		}

		pthread_mutex_unlock(&mutex);
	}
	return;
}

void NetListener::onEndStream()
{
	if(listener) {
		pthread_mutex_lock(&mutex);

		for(NetJoinAddrs::iterator it = netJoinAddrs.begin(); it != netJoinAddrs.end(); ++it) {
			listener->onNetEndStream(this, it->first, param);
		}

		pthread_mutex_unlock(&mutex);
	}

	joined = NULL;
	delete this;

	return;
}
