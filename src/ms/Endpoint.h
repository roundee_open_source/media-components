/* 
 * File:   Endpoint.h
 * Author: Sergio
 *
 * Created on 7 de septiembre de 2011, 0:59
 */

#ifndef ENDPOINT_H
#define	ENDPOINT_H
#include "Joinable.h"
#include "rtpsession.h"
#include "RTPEndpoint.h"

class Endpoint : public RTPEndpoint::Listener
{
public:
	class Listener
	{
	public:
		//Virtual destructor
		virtual ~Listener() {};
	public:
		//Interface
		virtual void onNoRTPPacket(Endpoint* endpoint, MediaFrame::Type media, void* param) = 0;
		virtual void onVideoBandwidthState(Endpoint* endpoint, RemoteRateControl::BandwidthState state, void* param) = 0;
		virtual void onVoiceActivityState(Endpoint* endpoint, RemoteVAD::VoiceState state, void* param) = 0;
	};

public:
	Endpoint(std::wstring name,bool audioSupported,bool videoSupported,bool textSupport);
	~Endpoint();
	void SetListener(Endpoint::Listener* listener, void* param);

	//Interface
	virtual void onNoRTPPacket(RTPEndpoint* rtpep);
	virtual void onVideoBandwidthState(RTPEndpoint* rtpep, RemoteRateControl::BandwidthState state);
	virtual void onVoiceActivityState(RTPEndpoint* rtpep, RemoteVAD::VoiceState state);
	
	//Methods
	int Init();
	int SetMaxLimit(int kbps);
	int SetVADLevel(int decibel);
	int GetPorts(int* audioPort,int* videoPort,int* textPort);
	int End();
	
	//Endpoint  functionality
	int StartSending(MediaFrame::Type media,char *sendIp,int sendPort,RTPMap& rtpMap);
	int StopSending(MediaFrame::Type media);
	int StartReceiving(MediaFrame::Type media,RTPMap& rtpMap);
	int StopReceiving(MediaFrame::Type media);
	int RequestUpdate(MediaFrame::Type media);
	int StartAudioCapture(const char* filename);

	int SetLocalCryptoSDES(MediaFrame::Type media,const char* suite, const char* key64);
	int SetRemoteCryptoSDES(MediaFrame::Type media,const char* suite, const char* key64);
	int SetRemoteCryptoDTLS(MediaFrame::Type media,const char *setup,const char *hash,const char *fingerprint);
	int SetLocalSTUNCredentials(MediaFrame::Type media,const char* username, const char* pwd);
	int SetRemoteSTUNCredentials(MediaFrame::Type media,const char* username, const char* pwd);
	int SetRTPProperties(MediaFrame::Type media,const Properties& properties);

	//Attach
	int Attach(MediaFrame::Type media, Joinable *join);
	int Dettach(MediaFrame::Type media);
	Joinable* GetJoinable(MediaFrame::Type media);

	std::wstring& GetName() { return name; }
private:
	RTPEndpoint* GetRTPEndpoint(MediaFrame::Type media)
	{
		switch(media)
		{
			case MediaFrame::Audio:
				return audio;
			case MediaFrame::Video:
				return video;
			case MediaFrame::Text:
				return text;
		}

		return NULL;
	}
private:
	std::wstring name;
	//RTP sessions
	RTPEndpoint *audio;
	RTPEndpoint *video;
	RTPEndpoint *text;
	RemoteRateEstimator estimator;
	RemoteVAD						vad;

	Endpoint::Listener	*listener;
	void								*param;
};

#endif	/* ENDPOINT_H */

