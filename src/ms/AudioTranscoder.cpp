/* 
 * File:   AudioTranscoder.cpp
 * Author: endryu
 * 
 * Created on 2016-06-29
 */

#include "AudioTranscoder.h"

AudioTranscoder::AudioTranscoder(std::wstring &name)
{
	//Store tag
	this->tag = name;

	//Not inited
	inited = false;
}

AudioTranscoder::~AudioTranscoder()
{
	//Check if ended properly
	if (inited)
		//End!!
		End();
}

int AudioTranscoder::Init()
{
	Log("-Init AudioTranscoder [%ls,encoder:%p,decoder:%p]",tag.c_str(),&encoder,&decoder);
	
	//Init pipe
	pipe.Init();
	//Start encoder
	encoder.Init(&pipe);
	//Star decoder
	decoder.Init(&pipe);
	decoder.setDTMFListener(this);
	//Inited
	inited = true;
	//OK
	return 1;
}
int AudioTranscoder::SetCodec(AudioCodec::Type codec)
{
	return encoder.SetCodec(codec);
}
int AudioTranscoder::End()
{
	Log("-End AudioTranscoder [%ls]",tag.c_str());
	//End encoder and decoder
	encoder.End();
	decoder.End();
	//End pipe
	pipe.End();
	//Not inited
	inited = false;
	//OK
	return 1;
}

void AudioTranscoder::AddListener(Joinable::Listener *listener)
{
	encoder.AddListener(listener);
}

void AudioTranscoder::Update()
{
	encoder.Update();
}

void AudioTranscoder::SetREMB(DWORD estimation)
{
	encoder.SetREMB(estimation);
}

void AudioTranscoder::RemoveListener(Joinable::Listener *listener)
{
	encoder.RemoveListener(listener);
}

void AudioTranscoder::onRTPPacket(RTPPacket &packet)
{
	decoder.onRTPPacket(packet);
}
void AudioTranscoder::onResetStream()
{
	decoder.onResetStream();
}
void AudioTranscoder::onEndStream()
{
	decoder.onEndStream();
}

int AudioTranscoder::Attach(Joinable *join)
{
	decoder.Attach(join);
}

int AudioTranscoder::Dettach()
{
	decoder.Dettach();
}

int AudioTranscoder::SetStreamer(FFStreamer* streamer)
{
	return decoder.SetStreamer(streamer);
}

void AudioTranscoder::onDTMFPacket(RTPPacket &packet)
{
	encoder.Multiplex(packet);
}
