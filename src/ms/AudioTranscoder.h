/* 
 * File:   AudioTranscoder.h
 * Author: endryu
 *
 * Created on 2016-06-29
 */

#ifndef AUDIOTRANSCODER_H
#define	AUDIOTRANSCODER_H
#include "AudioEncoderWorker.h"
#include "AudioDecoderWorker.h"
#include "audiopipe.h"
#include "ffstreamer.h"
#include <string>

class AudioTranscoder :
	public Joinable,
	public Joinable::Listener,
	public AudioDecoderJoinableWorker::DTMFListener
{
public:
	AudioTranscoder(std::wstring &name);
	virtual ~AudioTranscoder();

	int Init();
	int SetCodec(AudioCodec::Type codec);
	int End();

	//Joinable interface
	virtual void AddListener(Joinable::Listener *listener);
	virtual void Update();
	virtual void SetREMB(DWORD estimation);
	virtual void RemoveListener(Joinable::Listener *listener);

	//Virtuals from Joinable::Listener
	virtual void onRTPPacket(RTPPacket &packet);
	virtual void onResetStream();
	virtual void onEndStream();

	//Attach
	int Attach(Joinable *join);
	int Dettach();

	int SetStreamer(FFStreamer* streamer);

	const std::wstring& GetName() { return tag;	}

	virtual void onDTMFPacket(RTPPacket &packet);

private:
	AudioEncoderMultiplexerWorker	encoder;
	AudioDecoderJoinableWorker	decoder;
	AudioPipe	pipe;
	std::wstring	tag;
	bool		inited;
};

#endif	/* AUDIOTRANSCODER_H */

