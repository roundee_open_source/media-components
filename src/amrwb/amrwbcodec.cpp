#include "amrwbcodec.h"
#include "log.h"
#include <amrwbenc/inc/cnst.h>

AMRWBEncoder::AMRWBEncoder(const Properties &properties)
{
	dtx = 1;
	mode = MODE_20k;
	enc = NULL;

	type = AudioCodec::AMRWB;
	numFrameSamples = 320;

	for(Properties::const_iterator it=properties.begin(); it!=properties.end(); ++it) {
		//Check
		if (it->first.compare("dtx")==0) {
			dtx = atoi(it->second.c_str());
		}
		if (it->first.compare("mode")==0) {
			mode = atoi(it->second.c_str());
		}
	}

	enc = E_IF_init();
	if(enc == NULL)
		Error("Could not open AMRWB encoder");
}

AMRWBEncoder::~AMRWBEncoder()
{
	E_IF_exit(enc);
}

int AMRWBEncoder::Encode (SWORD* in, int inLen, BYTE* out, int outLen)
{
	return E_IF_encode(enc, mode, in, (unsigned char*)out, dtx);
}

AMRWBDecoder::AMRWBDecoder()
{
	dec = NULL;

	type = AudioCodec::AMRWB;
	numFrameSamples = 320;

	dec = D_IF_init();
	if(dec == NULL)
		Error("Could not open AMRWB decoder");
}

AMRWBDecoder::~AMRWBDecoder()
{
	D_IF_exit(dec);
}

int AMRWBDecoder::Decode(BYTE* in, int inLen, SWORD* out, int outLen)
{
	D_IF_decode(dec, (unsigned char*)in, out, 0);

	return 320;
}
