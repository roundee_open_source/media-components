#ifndef AMRWB_H
#define AMRWB_H


#ifdef __cplusplus
extern "C" {
#endif
#include <enc_if.h>
#include <dec_if.h>
#ifdef __cplusplus
}
#endif
#include "audio.h"


class AMRWBEncoder : public AudioEncoder
{
public:
	AMRWBEncoder(const Properties &properties);
	virtual ~AMRWBEncoder();
	virtual int Encode(SWORD* in,int inLen,BYTE* out,int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return 16000;	}
	virtual DWORD GetRate()					{ return 16000;	}
	virtual DWORD GetClockRate()			{ return 16000;	}
private:
	int		dtx;
	int		mode;
	void	*enc;

};


class AMRWBDecoder : public AudioDecoder
{
public:
	AMRWBDecoder();
	virtual ~AMRWBDecoder();
	virtual int Decode(BYTE* in,int inLen,SWORD* out,int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return 16000;	}
	virtual DWORD GetRate()					{ return 16000;	}
private:
	void	*dec;

};


#endif	/* AMRWB_H */
