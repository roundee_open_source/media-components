/*============================== File Header =================================
  FILE NAME     : mfrecorder.cpp
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         :
  VERSION       :
  DATE          :
  AUTHOR        :
  HISTORY       :
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "log.h"
#include "mfrecorder.h"
#include "aacconfig.h"
#include "aac/aacencoder.h"
#include "h264/h264encoder2.h"
#include "mp3/mp3encoder.h"


MFRecorder::MFRecorder()
{
	oc = NULL;
	audioSt = NULL;

	handle = MP4_INVALID_FILE_HANDLE;
	audioTrackId = MP4_INVALID_TRACK_ID;
	videoTrackId = MP4_INVALID_TRACK_ID;

	recording = false;

	//Create mutex
	pthread_mutex_init(&mutex,0);

}

MFRecorder::~MFRecorder()
{
	//Close just in case
	Close();

	pthread_mutex_destroy(&mutex);
}

bool MFRecorder::Create(const char* filename, int size, int fps, int bitrate, int intraPeriod)
{
	Log("-Opening record [%s]", filename);

	Close();

	char	*ext = (char*)(filename + strlen(filename) - 3);
	if(strcmp(ext, "mp3") == 0 || strcmp(ext, "aac") == 0) {
		int		ret;

		ret = avformat_alloc_output_context2(&oc, NULL, NULL, filename);
		if(ret < 0) {
			return false;
		}
		ret = avio_open(&oc->pb, filename, AVIO_FLAG_WRITE);
		if(ret < 0) {
			avformat_free_context(oc);
			oc = NULL;
			return false;
		}

		//add audio stream
		audioSt = avformat_new_stream(oc, NULL);
		if(oc->oformat->audio_codec == AV_CODEC_ID_MP3) {
			MP3Encoder *audioEnc;
			audioEnc = (MP3Encoder*)AudioCodecFactory::CreateEncoder(AudioCodec::MP3);
			avcodec_copy_context(audioSt->codec, audioEnc->GetCodecContext());
			delete audioEnc;
		}
		else if(oc->oformat->audio_codec == AV_CODEC_ID_AAC) {
			AACEncoder *audioEnc;
			audioEnc = (AACEncoder*)AudioCodecFactory::CreateEncoder(AudioCodec::AAC);
			avcodec_copy_context(audioSt->codec, audioEnc->GetCodecContext());
			delete audioEnc;
		}
		//audioSt->disposition = AV_DISPOSITION_DEFAULT;
		audioSt->time_base = (AVRational){ 1, audioSt->codec->sample_rate };

		//write header
		ret = avformat_write_header(oc, NULL);
		if(ret < 0) {
			Close();
			return false;
		}
	}
	else if(strcmp(ext, "mp4") == 0) {
		handle = MP4Create(filename, 0);
		if(handle == MP4_INVALID_FILE_HANDLE) return false;

		audioTrackId = MP4AddAudioTrack(handle, 44100, 1024, MP4_MPEG4_AUDIO_TYPE);
		if(audioTrackId == MP4_INVALID_TRACK_ID) return false;

		/*
		 * https://wiki.multimedia.cx/index.php/MPEG-4_Audio
		 * object type		: MP4_MPEG4_AAC_LC_AUDIO_TYPE
		 * frequency index: 4 (44100Hz)
		 * channel config	: 1
		*/
		uint8_t config[2];
		config[0] = 0x12;
		config[1] = 0x08;
		if(MP4SetTrackESConfiguration(handle, audioTrackId, config, sizeof(config)) == false) return false;

		this->fps = fps;
		this->size = size;
		//add video when first SPS
	}

	//Success
	return true;
}

bool MFRecorder::Record()
{
	if(oc == NULL && handle == MP4_INVALID_FILE_HANDLE)
		//Error
		return Error("No MP4 file opened for recording");

	pthread_mutex_lock(&mutex);

	if(!recording) {
		//Recording
		recording = true;
	}

	pthread_mutex_unlock(&mutex);

	//Exit
	return recording;
}

bool MFRecorder::Stop()
{
	pthread_mutex_lock(&mutex);

	//not recording anymore
	if(recording) {
		recording = false;
	}

	pthread_mutex_unlock(&mutex);

	return true;
}

bool MFRecorder::Close()
{
	//Stop always
	Stop();

	pthread_mutex_lock(&mutex);

	if(oc) {
		av_write_trailer(oc);
		avio_close(oc->pb);
		avformat_free_context(oc);
		oc = NULL;
	}
	if(handle != MP4_INVALID_FILE_HANDLE) {
		MP4Close(handle);
		handle = MP4_INVALID_FILE_HANDLE;
	}

	pthread_mutex_unlock(&mutex);

	return true;
}

void MFRecorder::onAudioRTPPacket(RTPPacket& packet)
{
	if(!recording) return;

	pthread_mutex_lock(&mutex);

	if(oc && audioSt) {
		AVPacket	pkt;
		int				ret;

		av_new_packet(&pkt, packet.GetMediaLength());
		memcpy(pkt.data, packet.GetMediaData(), pkt.size);
		pkt.stream_index = audioSt->index;

		if((ret = av_interleaved_write_frame(oc, &pkt)) < 0) {
			char strErr[AV_ERROR_MAX_STRING_SIZE];
			Error("-onAudioRTPPacket: %s(%d)", av_make_error_string(strErr, AV_ERROR_MAX_STRING_SIZE, ret), ret);
		}
		av_free_packet(&pkt);
	}

	if(handle != MP4_INVALID_FILE_HANDLE && audioTrackId != MP4_INVALID_TRACK_ID) {
		if(MP4WriteSample(handle, audioTrackId, packet.GetMediaData(), packet.GetMediaLength()) == false) {
			Error("-onAudioRTPPacket: error on MP4WriteSample");
		}
	}

	pthread_mutex_unlock(&mutex);
}

void MFRecorder::onVideoRTPPacket(RTPPacket& packet)
{
	if(!recording) return;

	pthread_mutex_lock(&mutex);

	if(handle != MP4_INVALID_FILE_HANDLE) {
		int			pkt_len = packet.GetMediaLength();
		uint8_t	*data = packet.GetMediaData();

		int			nal_unit_type = data[0] & 0x1F;
		if(nal_unit_type == 7) {
			//SPS
			if(videoTrackId == MP4_INVALID_TRACK_ID) {
				videoTrackId = MP4AddH264VideoTrack(handle, 90000, 90000 / fps, GetWidth(size), GetHeight(size), data[1], data[2], data[3], 3);
			}
			MP4AddH264SequenceParameterSet(handle, videoTrackId, data, pkt_len);
		}
		else if(nal_unit_type == 8) {
			//PPS
			MP4AddH264PictureParameterSet(handle, videoTrackId, data, pkt_len);
		}
		else {
			data -= 4;
			data[0] = (pkt_len >> 24) & 0xFF;
			data[1] = (pkt_len >> 16) & 0xFF;
			data[2] = (pkt_len >> 8) & 0xFF;
			data[3] = (pkt_len) & 0xFF;

			pkt_len += 4;
			if(MP4WriteSample(handle, videoTrackId, data, pkt_len, MP4_INVALID_DURATION, 0, true) == false) {
				Error("-onVideoRTPPacket: error on MP4WriteSample");
			}
		}
	}

	pthread_mutex_unlock(&mutex);
}
