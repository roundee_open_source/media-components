#include "remotevad.h"

RemoteVAD::RemoteVAD()
{
	cutDecibel = 0;
	voiceState = Silence;

	memset(vadPacket, 0, sizeof(vadPacket));
	vadCnt = 0;
	vadIdx = 0;
}

RemoteVAD::~RemoteVAD()
{
}

void RemoteVAD::SetListener(Listener* listener)
{
	//Store listener
	this->listener = listener;
}

void RemoteVAD::SetVADLevel(int decibel)
{
	Log("-SetVADLevel %d", decibel);
	cutDecibel = decibel;
}

void RemoteVAD::Update(RTPPacket* packet)
{
	if(!packet->GetVAD() || cutDecibel == 0) return;

	if(vadIdx == MAX_VAD_IDX) vadIdx = 0;
	if(vadPacket[vadIdx]) vadCnt--;

	if(packet->GetLevel() <= cutDecibel) {
		vadPacket[vadIdx++] = true;
		vadCnt++;
	}
	else {
		vadPacket[vadIdx++] = false;
	}

	if(voiceState == Silence) {
		if(vadCnt == MAX_VAD_IDX / 4) {	//25%
			voiceState = Speaking;
			if(listener) listener->onVoiceStateChanged(voiceState);
		}
	}
	else {
		if(vadCnt == 0) {
			voiceState = Silence;
			if(listener) listener->onVoiceStateChanged(voiceState);
		}
	}
}

void RemoteVAD::Reset()
{
	if(voiceState == Speaking) {
		voiceState = Silence;
		if(listener) listener->onVoiceStateChanged(voiceState);
	
		memset(vadPacket, 0, sizeof(vadPacket));
		vadCnt = 0;
		vadIdx = 0;
	}
}
