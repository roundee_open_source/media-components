#include "rtp/RTPIncomingSourceGroup.h"

#include <math.h>

#include "VideoLayerSelector.h"

RTPIncomingSourceGroup::RTPIncomingSourceGroup(MediaFrame::Type type) 
	: losts(128)
{
	this->rtt = 0;
	this->rttrtxSeq = 0;
	this->rttrtxTime = 0;
	this->type = type;
	//Small initial buffer of 100ms
	packets.SetMaxWaitTime(100);
}

RTPIncomingSourceGroup::~RTPIncomingSourceGroup() 
{
	Stop();
}

RTPIncomingSource* RTPIncomingSourceGroup::GetSource(DWORD ssrc)
{
	if (ssrc == media.ssrc)
		return &media;
	else if (ssrc == rtx.ssrc)
		return &rtx;
	else if (ssrc == fec.ssrc)
		return &fec;
	return NULL;
}

void RTPIncomingSourceGroup::AddListener(Listener* listener) 
{
	Debug("-RTPIncomingSourceGroup::AddListener() [listener:%p]",listener);
		
	ScopedLock scoped(mutex);
	listeners.insert(listener);
}

void RTPIncomingSourceGroup::RemoveListener(Listener* listener) 
{
	Debug("-RTPIncomingSourceGroup::RemoveListener() [listener:%p]",listener);
		
	ScopedLock scoped(mutex);
	listeners.erase(listener);
}

int RTPIncomingSourceGroup::AddPacket(RTPPacket* packet)
{
	//Check if it is the rtx packet used to calculate rtt
	if (rttrtxTime && packet->GetSeqNum()==rttrtxSeq)
	{
		//Get packet time
		const auto time = packet->GetTime();
		//Calculate rtt
		const auto rtt = time - rttrtxTime;
		//Set RTT
		SetRTT(rtt);
		//Done
		return 0;
	}

	//Add to lost packets and get count regardeless if this was discarded or not
	int lost = losts.AddPacket(packet);

	//Add to packet queue
	if (!packets.Add(packet))
		//Rejected packet
		return -1;
	
	//Return lost packets
	return lost;
}

void RTPIncomingSourceGroup::ResetPackets()
{
	//Reset packet queue and lost count
	packets.Reset();
	losts.Reset();
}

void RTPIncomingSourceGroup::Update()
{
	Update(getTimeMS());
}

void RTPIncomingSourceGroup::Update(QWORD now)
{
	//Refresh instant bitrates
	media.bitrate.Update(now);
	rtx.bitrate.Update(now);
	fec.bitrate.Update(now);
}

void RTPIncomingSourceGroup::SetRTT(DWORD rtt)
{
	//Store rtt
	this->rtt = rtt;
	//Set max packet wait time
	packets.SetMaxWaitTime(fmin(500,fmax(60,rtt*4)+40));
}

void RTPIncomingSourceGroup::Start()
{
	if (!isZeroThread(dispatchThread))
		return;

	//Create dispatch trhead
	createPriorityThread(&dispatchThread,[](void* arg) -> void* {
			Log(">RTPIncomingSourceGroup dispatch %p",arg);
			//Get group
			RTPIncomingSourceGroup* group = (RTPIncomingSourceGroup*) arg;
			//Loop until canceled
			RTPPacket *packet;
			int lost;
			while ((packet=group->packets.Wait(lost)))
			{
			ScopedLock scoped(group->mutex);
			//Deliver to all listeners
			for (auto listener : group->listeners)
			//Dispatch rtp packet
			listener->onRTP(group,packet);
			}
			Log("<RTPIncomingSourceGroup dispatch");
			return nullptr;
			},
			(void*)this,
			0
			);
}

void RTPIncomingSourceGroup::Stop()
{
	if (isZeroThread(dispatchThread))
		return;
	//Cacnel packet wait
	packets.Cancel();

	//Wait for thread
	pthread_join(dispatchThread,NULL);

	//Clean it
	setZeroThread(&dispatchThread);

	ScopedLock scoped(mutex);
	//Deliver to all listeners
	for (Listeners::const_iterator it=listeners.begin();it!=listeners.end();++it)
		//Dispatch rtp packet
		(*it)->onEnded(this);
	//Clear listeners
	listeners.clear();
}
