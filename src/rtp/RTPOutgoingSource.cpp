#include "rtp/RTPOutgoingSource.h"

	

RTCPSenderReport* RTPOutgoingSource::CreateSenderReport(QWORD now)
{
	//Create Sender report
	RTCPSenderReport *sr = new RTCPSenderReport();

	//Append data
	sr->SetSSRC(ssrc);
	sr->SetTimestamp(now);
	sr->SetRtpTimestamp(lastTime);
	sr->SetOctectsSent(totalBytes);
	sr->SetPacketsSent(numPackets);
	
	//Store last sending time
	lastSenderReport = now;
	//Store last send SR 32 middle bits
	lastSenderReportNTP = sr->GetNTPTimestamp();

	//Return it
	return sr;
}
