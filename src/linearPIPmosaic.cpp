#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "log.h"
#include "linearPIPmosaic.h"
extern "C" {
#include <libswscale/swscale.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libavutil/common.h>
}

/***********************
* LinearPIPMosaic
* Constructor
************************/
LinearPIPMosaic::LinearPIPMosaic(Mosaic::Type type, DWORD size) : Mosaic(type, size)
{
	//Get width and height
	mosaicTotalWidth = ::GetWidth(size);
	mosaicTotalHeight = ::GetHeight(size);

	//Allocate memory for under image
	underBuffer = (BYTE*)malloc32(mosaicSize);
	//Get aligned
	under = ALIGNTO32(underBuffer);
	//Reset mosaic
	memset(underBuffer, (BYTE)-128, mosaicSize);
}

/***********************
* LinearPIPMosaic
* Destructor
************************/
LinearPIPMosaic::~LinearPIPMosaic()
{
	//Free memory
	free(underBuffer);
}

/*****************************
* Update
* Update slot of mosaic with given image
*****************************/
int LinearPIPMosaic::Update(int pos, BYTE* image, int imgWidth, int imgHeight, bool keepAspectRatio)
{
	//Check it's in the mosaic
	if(pos < 0 || pos >= numSlots)
		return 0;

	//Check size
	if(!image || !(imgWidth > 0 && imgHeight > 0)) {
		//Clean position
		Clean(pos);
		//Exit
		return 0;
	}

	//Get plane pointers from destination
	DWORD numSlotsPixels = mosaicTotalWidth * mosaicTotalHeight;
	BYTE *lineY = mosaic;
	BYTE *lineU = mosaic + numSlotsPixels;
	BYTE *lineV = lineU + numSlotsPixels / 4;

	//Get plane pointers from incoming image
	DWORD imgNumPixels = imgHeight * imgWidth;
	BYTE *imageY = image;
	BYTE *imageU = imageY + imgNumPixels;
	BYTE *imageV = imageU + imgNumPixels / 4;

	DWORD mosaicTotalWidthUV = mosaicTotalWidth / 2;

	DWORD mosaicWidth		= 0;
	DWORD mosaicHeight  = 0;
	DWORD mosaicWidthUV	= 0;

	/*
	//2 ~ 3 slots
	if(numSlots >= 2 && numSlots <= 3) {
		mosaicWidth		= SIZE2MUL(mosaicTotalWidth / 5);
		mosaicHeight	= SIZE2MUL(mosaicTotalHeight / 5);
		mosaicWidthUV	= mosaicWidth / 2;
	}
	//4 ~ 6 slots
	else if(numSlots >= 4 && numSlots <= 6) {
		mosaicWidth		= SIZE2MUL(mosaicTotalWidth * 10 / 77);
		mosaicHeight	= SIZE2MUL(mosaicTotalHeight * 10 / 77);
		mosaicWidthUV	= mosaicWidth / 2;
	}
	*/
	mosaicWidth		= SIZE2MUL(mosaicTotalWidth * 10 / 77);
	mosaicHeight	= SIZE2MUL(mosaicTotalHeight * 10 / 77);
	mosaicWidthUV	= mosaicWidth / 2;

	//Get top position
	DWORD pipHeight		= mosaicHeight * (numSlots - 1);
	DWORD pipIni			= SIZE2MUL((mosaicTotalHeight - pipHeight) / 2);

	//Check which position is it
	if(pos == 0) {
		//Check if the sizes are equal
		if((imgWidth != mosaicTotalWidth) || (imgHeight != mosaicTotalHeight)) {
			//Get pointers of the under image
			BYTE *underY = under;
			BYTE *underU = under  + numSlotsPixels;
			BYTE *underV = underU + numSlotsPixels / 4;

			//Set resize
			resizer[pos]->SetResize(imgWidth, imgHeight, imgWidth, mosaicTotalWidth, mosaicTotalHeight, mosaicTotalWidth, keepAspectRatio);
			//Resize and set to slot
			resizer[pos]->Resize(imageY, imageU, imageV, underY, underU, underV);

			//Change pointers from image to resized one
			imageY = underY;
			imageU = underU;
			imageV = underV;
		}

		//Copy to the begining of the pip zone
		for(int i = 0; i < pipIni / 2; ++i) {
			//Copy Y line
			memcpy(lineY, imageY, mosaicTotalWidth);
			//Go to next
			lineY  += mosaicTotalWidth;
			imageY += mosaicTotalWidth;

			//Copy Y line
			memcpy(lineY, imageY, mosaicTotalWidth);
			//Go to next
			lineY  += mosaicTotalWidth;
			imageY += mosaicTotalWidth;

			//Copy U and V lines
			memcpy(lineU, imageU, mosaicTotalWidthUV);
			memcpy(lineV, imageV, mosaicTotalWidthUV);
			//Go to next
			lineU  += mosaicTotalWidthUV;
			lineV  += mosaicTotalWidthUV;
			imageU += mosaicTotalWidthUV;
			imageV += mosaicTotalWidthUV;
		}

		//Copy in the pip area
		for(int i = 0; i < pipHeight / 2; ++i) {
			//Copy skipping pip images
			memcpy(lineY, imageY, mosaicTotalWidth - mosaicWidth);
			lineY  += mosaicTotalWidth;
			imageY += mosaicTotalWidth;
			memcpy(lineY, imageY, mosaicTotalWidth - mosaicWidth);
			lineY  += mosaicTotalWidth;
			imageY += mosaicTotalWidth;

			memcpy(lineU, imageU, mosaicTotalWidthUV - mosaicWidthUV);
			lineU  += mosaicTotalWidthUV;
			imageU += mosaicTotalWidthUV;

			memcpy(lineV, imageV, mosaicTotalWidthUV - mosaicWidthUV);
			lineV  += mosaicTotalWidthUV;
			imageV += mosaicTotalWidthUV;
		}

		//Copy from the end pip zone
		for(int i = 0; i < pipIni / 2; ++i) {
			//Copy Y line
			memcpy(lineY, imageY, mosaicTotalWidth);
			//Go to next
			lineY  += mosaicTotalWidth;
			imageY += mosaicTotalWidth;

			//Copy Y line
			memcpy(lineY, imageY, mosaicTotalWidth);
			//Go to next
			lineY  += mosaicTotalWidth;
			imageY += mosaicTotalWidth;

			//Copy U and V lines
			memcpy(lineU, imageU, mosaicTotalWidthUV);
			memcpy(lineV, imageV, mosaicTotalWidthUV);
			//Go to next
			lineU  += mosaicTotalWidthUV;
			lineV  += mosaicTotalWidthUV;
			imageU += mosaicTotalWidthUV;
			imageV += mosaicTotalWidthUV;
		}
	}
	else {
		//Get offsets
		DWORD offsetY  = mosaicTotalWidth * (pipIni + mosaicHeight * (pos - 1))     + (mosaicTotalWidth - mosaicWidth);
		DWORD offsetUV = mosaicTotalWidth * (pipIni + mosaicHeight * (pos - 1)) / 4 + (mosaicTotalWidthUV - mosaicWidthUV);

		//Get plane pointers
		lineY += offsetY;
		lineU += offsetUV;
		lineV += offsetUV;

		//Check if the sizes are equal
		if((imgWidth == mosaicWidth) && (imgHeight == mosaicHeight)) {
			for(int i = 0; i < mosaicHeight / 2; i++) {
				//Copy Y line
				memcpy(lineY, imageY, mosaicWidth);
				//Go to next
				lineY  += mosaicTotalWidth;
				imageY += mosaicWidth;

				//Copy Y line
				memcpy(lineY, imageY, mosaicWidth);
				//Go to next
				lineY  += mosaicTotalWidth;
				imageY += mosaicWidth;

				//Copy U and V lines
				memcpy(lineU, imageU, mosaicWidthUV);
				memcpy(lineV, imageV, mosaicWidthUV);
				//Go to next
				lineU  += mosaicTotalWidthUV;
				lineV  += mosaicTotalWidthUV;
				imageU += mosaicWidthUV;
				imageV += mosaicWidthUV;
			}
		}
		else {
			//Set resize
			resizer[pos]->SetResize(imgWidth, imgHeight, imgWidth, mosaicWidth, mosaicHeight, mosaicTotalWidth, keepAspectRatio);
			//Resize and set to slot
			resizer[pos]->Resize(imageY, imageU, imageV, lineY, lineU, lineV);
		}
	}

	//We have changed
	SetChanged();

	return 1;
}

/*****************************
* Clear
* 	Clear slot imate
*****************************/
int LinearPIPMosaic::Clean(int pos)
{
}


/***********************
* GetWidth
*	Get mosaic width
************************/
int LinearPIPMosaic::GetWidth()
{
	return mosaicTotalWidth;
}

/***********************
* GetHeight
*	Get mosaic height
************************/
int LinearPIPMosaic::GetHeight()
{
	return mosaicTotalHeight;
}

/***********************
* GetFrame
*	Get mosaic frame
************************/
BYTE* LinearPIPMosaic::GetFrame()
{
	return mosaic;
}


int LinearPIPMosaic::GetWidth(int pos)
{
	//Check it's in the mosaic
	if(pos >= numSlots)
		//Exit
		return 0;
	//Main
	if(!pos)
		return mosaicTotalWidth;

	/*
	if(numSlots >= 2 && numSlots <= 3)
		return SIZE2MUL(mosaicTotalWidth / 5);
	*/

	return SIZE2MUL(mosaicTotalWidth * 10 / 77);
}

int LinearPIPMosaic::GetHeight(int pos)
{
	//Check it's in the mosaic
	if(pos >= numSlots)
		//Exit
		return 0;
	//Main
	if(!pos)
		return mosaicTotalHeight;
	
	/*
	if(numSlots >= 2 && numSlots <= 3)
		return SIZE2MUL(mosaicTotalHeight / 5);
	*/
	
	return SIZE2MUL(mosaicTotalHeight * 10 / 77);
}

int LinearPIPMosaic::GetTop(int pos)
{
	//Check it's in the mosaic
	if(pos >= numSlots)
		//Exit
		return 0;
	//Main
	if(!pos)
		return 0;

	DWORD mosaicHeight = GetHeight(pos);
	return SIZE2MUL((mosaicTotalHeight - mosaicHeight * (numSlots - 2 * pos + 1)) / 2);
}

int LinearPIPMosaic::GetLeft(int pos)
{
	//Check it's in the mosaic
	if(pos >= numSlots)
		//Exit
		return 0;
		//Main
	if(!pos)
		return 0;

	DWORD mosaicWidth = GetWidth(pos);
	return SIZE2MUL(mosaicTotalWidth - mosaicWidth);
}
