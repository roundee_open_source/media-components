
#include <opus/opus.h>
#include "oggopus_container.h"
#include "log.h"


// helper, write a little-endian 32 bit int to memory
void le32(unsigned char *p, int v)
{
  p[0] = v & 0xff;
  p[1] = (v >> 8) & 0xff;
  p[2] = (v >> 16) & 0xff;
  p[3] = (v >> 24) & 0xff;
}

// helper, write a little-endian 16 bit int to memory
void le16(unsigned char *p, int v)
{
  p[0] = v & 0xff;
  p[1] = (v >> 8) & 0xff;
}


// manufacture a generic OpusHead packet
ogg_packet* op_opushead(void)
{
  int size = 19;
	ogg_packet *op = (ogg_packet*)malloc(sizeof(ogg_packet));
  unsigned char *data = (unsigned char*)malloc(size);

	if(op == NULL || data == NULL) {
    Error("Couldn't allocate data buffer");
		if(op) free(op);
		if(data) free(data);
    return NULL;
  }

  memcpy(data, "OpusHead", 8);  // identifier
  data[8] = 1;                  // version
  data[9] = 2;                  // channels
  le16(data+10, 0);             // pre-skip
  le32(data + 12, 48000);       // original sample rate
  le16(data + 16, 0);           // gain
  data[18] = 0;                 // channel mapping family

  op->packet = data;
  op->bytes = size;
  op->b_o_s = 1;
  op->e_o_s = 0;
  op->granulepos = 0;
  op->packetno = 0;

  return op;
}

// manufacture a generic OpusTags packet
ogg_packet* op_opustags(void)
{
  char *identifier = "OpusTags";
  char *vendor = "opus rtp packet dump";
  int size = strlen(identifier) + 4 + strlen(vendor) + 4;
	ogg_packet *op = (ogg_packet*)malloc(sizeof(ogg_packet));
  unsigned char *data = (unsigned char*)malloc(size);

	if(op == NULL || data == NULL) {
    Error("Couldn't allocate data buffer");
		if(op) free(op);
		if(data) free(data);
    return NULL;
  }

  memcpy(data, identifier, 8);
  le32(data + 8, strlen(vendor));
  memcpy(data + 12, vendor, strlen(vendor));
  le32(data + 12 + strlen(vendor), 0);

  op->packet = data;
  op->bytes = size;
  op->b_o_s = 0;
  op->e_o_s = 0;
  op->granulepos = 0;
  op->packetno = 1;

  return op;
}

ogg_packet* op_from_pkt(const unsigned char* pkt, int len)
{
	ogg_packet *op = (ogg_packet*)malloc(sizeof(ogg_packet));
	if(op == NULL) {
    Error("Couldn't allocate data buffer");
    return NULL;
  }

  op->packet = (unsigned char *)pkt;
  op->bytes = len;
  op->b_o_s = 0;
  op->e_o_s = 0;

  return op;
}

// free a packet and its contents
void op_free(ogg_packet *op) {
  if(op) {
    if(op->packet) free(op->packet);
    free(op);
  }
}

// calculate the number of samples in an opus packet
int opus_samples(const unsigned char* packet, int size)
{
  // number of samples per frame at 48 kHz
  int samples = opus_packet_get_samples_per_frame(packet, 48000);
  // number "frames" in this packet
  int frames = opus_packet_get_nb_frames(packet, size);

  return samples * frames;
}

// helper, flush remaining ogg data
int ogg_flush(OGGOPUS_ctx* pCtx)
{
	ogg_page	page;
	size_t		written;

	if(!pCtx || !pCtx->out) {
		return -1;
	}

	while(ogg_stream_flush(&pCtx->stream, &page)) {
		written = fwrite(page.header, 1, page.header_len, pCtx->out);
		if(written != (size_t)page.header_len) {
			Error("Error writing Ogg page header");
			return -2;
		}
		written = fwrite(page.body, 1, page.body_len, pCtx->out);
		if(written != (size_t)page.body_len) {
			Error("Error writing Ogg page body");
			return -3;
		}
	}

	return 0;
}


void OGGOPUS_write(OGGOPUS_ctx* pCtx, const u_char* data, size_t size)
{
	ogg_page	page;
	size_t		written;

	if(!pCtx || !pCtx->out) {
		return;
	}

  ogg_packet *op = op_from_pkt(data, size);

  // write the payload to our opus file
  op->packetno = ++pCtx->seq;
  pCtx->granulepos += opus_samples(data, size);
  op->granulepos = pCtx->granulepos;
  ogg_stream_packetin(&pCtx->stream, op);
  free(op);

	while(ogg_stream_pageout(&pCtx->stream, &page)) {
    written = fwrite(page.header, 1, page.header_len, pCtx->out);
    if (written != (size_t)page.header_len) {
      Error("Error writing Ogg page header");
      return;
    }
    written = fwrite(page.body, 1, page.body_len, pCtx->out);
    if (written != (size_t)page.body_len) {
      Error("Error writing Ogg page body");
      return;
    }
  }
}

OGGOPUS_ctx* OGGOPUS_open(const char* ogg_file)
{
  OGGOPUS_ctx	*pCtx;
  ogg_packet	*op;

	pCtx = (OGGOPUS_ctx*)malloc(sizeof(OGGOPUS_ctx));
	if(pCtx == NULL || ogg_stream_init(&pCtx->stream, rand()) < 0) {
		Error("-OGGOPUS container: initialize fail");
		if(pCtx) free(pCtx);
		return NULL;
	}
  pCtx->out = fopen(ogg_file, "wb");
  if(!pCtx->out) {
		Error("-OGGOPUS container: can't open output file [file=%s]", ogg_file);
		free(pCtx);
    return NULL;
  }
  pCtx->seq = 0;
  pCtx->granulepos = 0;

  // wrtie stream header
  op = op_opushead();
  ogg_stream_packetin(&pCtx->stream, op);
  op_free(op);

  op = op_opustags();
  ogg_stream_packetin(&pCtx->stream, op);
  op_free(op);

  ogg_flush(pCtx);
  // flush a ogg-data for avoiding a size-zero problem when a recording ends. 
  fflush(pCtx->out);

  Log("-OGGOPUS container opened [file=%s]", ogg_file);

  return pCtx;
}

void OGGOPUS_close(OGGOPUS_ctx* pCtx)
{
	if(!pCtx || !pCtx->out) {
		return;
	}

	// write outstanding data
	ogg_flush(pCtx);

	// clean up
	fclose(pCtx->out);
	ogg_stream_clear(&pCtx->stream);

	free(pCtx);

	Log("-OGGOPUS container closed");
}
