#include "log.h"
#include "video.h"
#include "h264/h264encoder.h"
#include "h264/h264encoder2.h"
#include "h264/h264decoder.h"
#include "vp8/vp8decoder.h"
#include "vp8/vp8encoder.h"

VideoDecoder* VideoCodecFactory::CreateDecoder(VideoCodec::Type codec)
{
	Log("-CreateVideoDecoder[%d,%s]",codec,VideoCodec::GetNameFor(codec));

	//Depending on the codec
	switch(codec)
	{
		case VideoCodec::H264:
			return new H264Decoder();
		case VideoCodec::VP8:
			return new VP8Decoder();
		default:
			Error("Video decoder not found [%d]",codec);
	}
	return NULL;
}

VideoEncoder* VideoCodecFactory::CreateEncoder(VideoCodec::Type codec)
{
	//Empty properties
	Properties properties;

	//Create codec
	return CreateEncoder(codec,properties);
}


VideoEncoder* VideoCodecFactory::CreateEncoder(VideoCodec::Type codec,const Properties& properties)
{
	Log("-CreateVideoEncoder[%d,%s]",codec,VideoCodec::GetNameFor(codec));

	//Depending on the codec
	switch(codec)
	{
		case VideoCodec::H264:
			return new H264Encoder(properties);
		case VideoCodec::H264Rec:
			return new H264Encoder2(properties);
		case VideoCodec::VP8:
			return new VP8Encoder(properties);
		default:
			Error("Video Encoder not found");
	}
	return NULL;
}
