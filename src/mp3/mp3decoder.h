/* 
 * File:   mp3decoder.h
 * Author: endryu
 */

#ifndef MP3DECODER_H
#define	MP3dECODER_H

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavresample/avresample.h>
#include <libavutil/opt.h>
}
#include "config.h"
#include "codecs.h"
#include "audio.h"
#include "ffstreamer.h"


class MP3Decoder : public AudioDecoder
{
public:
	MP3Decoder();
	void Init(FFStreamer* streamer);
	virtual ~MP3Decoder();
	virtual int Decode(BYTE* in, int inLen, SWORD* out, int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return this->rate; }
	virtual DWORD GetRate()					{ return rate; }
private:
	AVCodecContext		*ctx;
	AVFrame				*frame;
	DWORD				rate;
};

#endif	/* MP3DECODER_H */

