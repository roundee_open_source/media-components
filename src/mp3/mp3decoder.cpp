/* 
 * File:   mp3decoder.cpp
 * Author: endryu
 */

#include <limits>

#include "mp3decoder.h"
#include "avcodecutils.h"
#include "log.h"

MP3Decoder::MP3Decoder()
{
	ctx = NULL;
	type = AudioCodec::MP3;

	frame = av_frame_alloc();
}

void MP3Decoder::Init(FFStreamer* streamer)
{
	ctx = cloneCodecCtx(streamer->GetAudioCodecCtx());
	rate = ctx->sample_rate;
}

MP3Decoder::~MP3Decoder()
{
	if(frame)
		av_free(frame);
	if(ctx)
	{
		avcodec_close(ctx);
		free(ctx);
	}
}

int MP3Decoder::Decode(BYTE* in, int inLen, SWORD* out, int outLen)
{
	int			got_frame;
	AVPacket	packet;
	int			planar;
	int			i;

	numFrameSamples = 0;
	if(inLen <= 0) return 0;
	if(inLen == 4) {
		Log("seek - flush buffers");
		avcodec_flush_buffers(ctx);
		return 0;
	}

	av_init_packet(&packet);
	packet.data = (BYTE*)in;
	packet.size = inLen;

	if(avcodec_decode_audio4(ctx, frame, &got_frame, &packet) < 0) {
		return Error("Error decoding MP3");
	}
	if(got_frame) {
		planar = av_sample_fmt_is_planar((AVSampleFormat)frame->format);
		numFrameSamples = frame->nb_samples;
		if(planar || frame->channels == 1) {
			switch(ctx->sample_fmt) {
			case AV_SAMPLE_FMT_S16:
			case AV_SAMPLE_FMT_S16P:
				for(i = 0; i < frame->nb_samples; i++) {
					out[i] = ((SWORD*)frame->extended_data[0])[i];
				}
				break;
			case AV_SAMPLE_FMT_FLT:
			case AV_SAMPLE_FMT_FLTP:
				for(i = 0; i < frame->nb_samples; i++) {
					out[i] = (SWORD)(((float*)frame->extended_data[0])[i] * std::numeric_limits<short>::max());
				}
				break;
			}
		}
		else {
			switch(ctx->sample_fmt) {
			case AV_SAMPLE_FMT_S16:
			case AV_SAMPLE_FMT_S16P:
				for(i = 0; i < frame->nb_samples; i++) {
					out[i] = ((SWORD*)frame->extended_data[0])[i * 2];
				}
				break;
			case AV_SAMPLE_FMT_FLT:
			case AV_SAMPLE_FMT_FLTP:
				for(i = 0; i < frame->nb_samples; i++) {
					out[i] = (SWORD)(((float*)frame->extended_data[0])[i * 2] * std::numeric_limits<short>::max());
				}
				break;
			}
		}
	}
	av_frame_unref(frame);
	
	return numFrameSamples;
}
