/* 
 * File:   mp3encoder.h
 * Author: endryu
 *
 */

#ifndef MP3ENCODER_H
#define	MP3ENCODER_H

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavresample/avresample.h>
#include <libavutil/opt.h>
}
#include "config.h"
#include "codecs.h"
#include "audio.h"

class MP3Encoder : public AudioEncoder
{
public:
	MP3Encoder(const Properties &properties);
	virtual ~MP3Encoder();
	virtual int Encode(SWORD *in,int inLen,BYTE* out,int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return GetRate();				}
	virtual DWORD GetRate()			{ return ctx->sample_rate?ctx->sample_rate:0;	}
	virtual DWORD GetClockRate()		{ return GetRate();				}

	AVCodecContext* GetCodecContext() { return ctx; }

private:
	AVCodec 	*codec;
	AVCodecContext	*ctx;
	AVAudioResampleContext *avr;
	AVFrame         *frame;
	BYTE *samples;
	int samplesSize;
	int samplesNum;
};

#endif	/* MP3ENCODER_H */

