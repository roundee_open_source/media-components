/* 
 * File:   mp3encoder.cpp
 * Author: endryu
 * 
 */

#include "mp3encoder.h"
#include "log.h"

MP3Encoder::MP3Encoder(const Properties &properties)
{
	//NO ctx yet
	ctx = NULL;
	frame = NULL;
	///Set type
	type = AudioCodec::MP3;

	// Get encoder
	codec = avcodec_find_encoder(AV_CODEC_ID_MP3);

	// Check codec
	if(!codec)
	{
		Error("MP3: No encoder found");
		return;
	}
	//Alocamos el conto y el picture
	ctx = avcodec_alloc_context3(codec);

	//Check
	if (ctx == NULL)
	{
		Error("MP3: could not allocate context");
		return;
	}

	//Set params
	ctx->channels		= 1;
	ctx->sample_fmt		= AV_SAMPLE_FMT_S16P;
	ctx->bit_rate		= 128000;
	ctx->sample_rate	= 44100;
	//ctx->bit_rate		= 64000;
	//ctx->sample_rate		= 8000;
	ctx->thread_count	= 1;

	//Allow experimental codecs
	ctx->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;

	//OPEN it
	if (avcodec_open2(ctx, codec, NULL) < 0)
	{
		//Error
	        Error("MP3: could not open codec");
		//Exit
		return;
	}

	//Get the number of samples
	numFrameSamples = ctx->frame_size;

	//Create context for converting sampel formats
	avr = avresample_alloc_context();

	//Set options
	av_opt_set_int(avr, "in_channel_layout",  AV_CH_LAYOUT_MONO,	0);
	av_opt_set_int(avr, "out_channel_layout", AV_CH_LAYOUT_MONO,	0);
	av_opt_set_int(avr, "in_sample_rate",     ctx->sample_rate,     0);
	av_opt_set_int(avr, "out_sample_rate",    ctx->sample_rate,     0);
	av_opt_set_int(avr, "in_sample_fmt",      AV_SAMPLE_FMT_S16,   0);
	av_opt_set_int(avr, "out_sample_fmt",     AV_SAMPLE_FMT_S16P,    0);

	//Open context
	avresample_open(avr);

	//Temporal buffer samples
	samplesNum = numFrameSamples;

	//Allocate float samples
	av_samples_alloc(&samples, &samplesSize, 1, samplesNum, AV_SAMPLE_FMT_S16P, 0);

	//Create frame
	frame = av_frame_alloc();
	//Set defaults
	frame->nb_samples     = ctx->frame_size;
	frame->format         = ctx->sample_fmt;
	frame->channel_layout = ctx->channel_layout;

	//Log
	Log("MP3: Encoder open with frame size %d", numFrameSamples);
}

MP3Encoder::~MP3Encoder()
{
	//Check
	if (ctx)
	{
		//Close
		avcodec_close(ctx);
		av_free(ctx);
	}
	if(avr)
	{
		avresample_close(avr);
		avresample_free(&avr);
	}
	if (samples)
		av_freep(&samples);
	if (frame)
		av_frame_free(&frame);
}

int MP3Encoder::Encode (SWORD *in,int inLen,BYTE* out,int outLen)
{
	AVPacket pkt;
	int	pktSize;
	int got_output;

	if (!inLen)
		return 0;
	
	if (ctx == NULL)
		return Error("MP3: no context");

	//If not enought samples
	if (inLen!=numFrameSamples)
		//Exit
		return Error("MP3: sample size %d is not correct. Should be %d", inLen, numFrameSamples);

	//Convert
	int len = avresample_convert(avr, &samples, samplesSize, samplesNum, (BYTE**)&in, inLen*sizeof(SWORD), inLen);

	//Check 
	if (avcodec_fill_audio_frame(frame, ctx->channels, ctx->sample_fmt, (BYTE*)samples, samplesSize, 0)<0)
		//Exit
		return Error("MP3: could not fill audio frame");

	//Reset packet
	av_init_packet(&pkt);

	//Set output
	pkt.data = out;
	pkt.size = outLen;

	//Encode audio
	if (avcodec_encode_audio2(ctx, &pkt, frame, &got_output)<0)
		//Exit
		return Error("MP3: could not encode audio frame");

	//Check if we got output
	if (!got_output)
	//Exit
		return Error("MP3: could not get output packet");

	pktSize = pkt.size;
	av_packet_unref(&pkt);

	//Return encoded size
	return pktSize;
}
