#include "log.h"
#include "OpenSSL.h"
#include "dtls.h"
extern "C" {
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
}

int lock_ffmpeg(void **param, enum AVLockOp op)
{
	//Get mutex pointer
	pthread_mutex_t* mutex = (pthread_mutex_t*)(*param);
	//Depending on the operation
	switch(op) {
		case AV_LOCK_CREATE:
			//Create mutex
			mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
			//Init it
			pthread_mutex_init(mutex,NULL);
			//Store it
			*param = mutex;
			break;
		case AV_LOCK_OBTAIN:
			//Lock
			pthread_mutex_lock(mutex);
			break;
		case AV_LOCK_RELEASE:
			//Unlock
			pthread_mutex_unlock(mutex);
			break;
		case AV_LOCK_DESTROY:
			//Destroy mutex
			pthread_mutex_destroy(mutex);
			//Free memory
			free(mutex);
			//Clean
			*param = NULL;
			break;
	}
	return 0;
}

void log_ffmpeg(void* ptr, int level, const char* fmt, va_list vl)
{
	static int                  print_prefix = 1;
	char                        line[1024];

	if(level > AV_LOG_ERROR) {
		return;
	}
	//Format the
	av_log_format_line(ptr, level, fmt, vl, line, sizeof(line), &print_prefix);
	//Remove buffer errors
	if(strstr(line, "vbv buffer overflow") != NULL) {
		return;
	}
	//Log
	Log(line);
}

int initmediacomponents()
{
	// Register mutex for ffmpeg
	av_lockmgr_register(lock_ffmpeg);
	// Set log level
	av_log_set_callback(log_ffmpeg);

	// Ignore SIGPIPE 
	signal(SIGPIPE, SIG_IGN);

	// Init OpenSSL lib
	if(!OpenSSL::ClassInit()) {
		Error("-OpenSSL failed to initialize");
		return -1;
	}

	// Init avcodecs
	av_register_all();
	avcodec_register_all();

	// Set DTLS certificate
	DTLSConnection::SetCertificate(
			"/etc/linearhub/mediacomponents/lmp.crt",
			"/etc/linearhub/mediacomponents/lmp.key"
			);	

	// Init DTLS
	if(DTLSConnection::Initialize()) {
		Log("-DTLS SHA1   local fingerprint \"%s\"", DTLSConnection::GetCertificateFingerPrint(DTLSConnection::SHA1).c_str());
		Log("-DTLS SHA224 local fingerprint \"%s\"", DTLSConnection::GetCertificateFingerPrint(DTLSConnection::SHA224).c_str());
		Log("-DTLS SHA256 local fingerprint \"%s\"", DTLSConnection::GetCertificateFingerPrint(DTLSConnection::SHA256).c_str());
		Log("-DTLS SHA384 local fingerprint \"%s\"", DTLSConnection::GetCertificateFingerPrint(DTLSConnection::SHA384).c_str());
		Log("-DTLS SHA512 local fingerprint \"%s\"", DTLSConnection::GetCertificateFingerPrint(DTLSConnection::SHA512).c_str());
	}
	// DTLS not available
	else {
		Error("DTLS initialization failed, no DTLS available");
	}

	return 1;
}
