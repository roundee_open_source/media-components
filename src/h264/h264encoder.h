#ifndef _H264ENCODER_H_
#define _H264ENCODER_H_
#include "codecs.h"
#include "video.h"
#include <wels/codec_api.h>

class H264Encoder : public VideoEncoder
{
public:
	H264Encoder(const Properties& properties);
	virtual ~H264Encoder();
	virtual VideoFrame* EncodeFrame(BYTE *in,DWORD len);
	virtual int FastPictureUpdate();
	virtual int SetSize(int width,int height);
	virtual int SetFrameRate(int fps,int kbits,int intraPeriod);

private:
	int OpenCodec();

	ISVCEncoder			*enc;
	SEncParamExt		params;
	int							width;
	int							height;
	int							numPixels;
	int							bitrate;
	int							fps;

	SSourcePicture	pic;
	VideoFrame			*frame;
	bool						keyFrameRequest;

	int							opened;
	int							intraPeriod;

};

#endif 
