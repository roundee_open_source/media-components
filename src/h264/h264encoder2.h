#ifndef _H264ENCODER2_H_
#define _H264ENCODER2_H_
#include "codecs.h"
#include "video.h"
#include <wels/codec_api.h>

class H264Encoder2 : public VideoEncoder
{
public:
	H264Encoder2(const Properties& properties);
	virtual ~H264Encoder2();
	virtual VideoFrame* EncodeFrame(BYTE *in,DWORD len);
	virtual int FastPictureUpdate();
	virtual int SetSize(int width,int height);
	virtual int SetFrameRate(int fps,int kbits,int intraPeriod);

private:
	int OpenCodec();

	ISVCEncoder			*enc;
	SEncParamBase		params;
	int							width;
	int							height;
	int							numPixels;
	int							bitrate;
	int							fps;

	SSourcePicture	pic;
	VideoFrame			*frame;
	bool						keyFrameRequest;

	int							opened;
	int							intraPeriod;

};

#endif 
