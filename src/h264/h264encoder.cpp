#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "log.h"
#include "h264encoder.h"


/**********************
* H264Encoder
*	Constructor
***********************/
H264Encoder::H264Encoder(const Properties& properties)
{
	//Set default values
	type    		= VideoCodec::H264;
	frame				= NULL;

	//No estamos abiertos
	opened			= false;

	//No bitrate
	bitrate			= 0;
	fps					= 0;
	intraPeriod	= 0;

	//Reset values
	enc					= NULL;

	keyFrameRequest = false;
}

/**********************
* ~H264Encoder
*	Destructor
***********************/
H264Encoder::~H264Encoder()
{
	//If we have an encoder
	if(enc) {
		//Close it
		enc->Uninitialize();
		WelsDestroySVCEncoder(enc);
	}
	//If we have created a frame
	if(frame) {
		//Delete it
		delete(frame);
	}
}

/**********************
* SetSize
***********************/
int H264Encoder::SetSize(int width, int height)
{
	//Save values
	this->width		= width;
	this->height	= height;

	//calculate number of pixels in  input image
	numPixels			= width * height;

	//Open codec
	return OpenCodec();
}

/************************
* SetFrameRate
**************************/
int H264Encoder::SetFrameRate(int frames, int kbits, int intraPeriod)
{
	//Save frame rate
	fps = (frames > 0)? frames : 10;

	//Save bitrate
	bitrate = kbits;

	//Save intra period
	this->intraPeriod = intraPeriod;
	
	return 1;
}

/**********************
* OpenCodec
***********************/
int H264Encoder::OpenCodec()
{
	Log("-OpenCodec H264 [%dkbps,%dfps,%dintra]", bitrate, fps, intraPeriod);

	//Check 
	if(opened)
		return Error("Codec already opened");

	//Create encoder.
	if(WelsCreateSVCEncoder(&enc) != 0) {
		//Failed to create encoder
		return Error("Could not open h264 encoder");
	}

	enc->GetDefaultParams(&params);
	params.iUsageType = CAMERA_VIDEO_REAL_TIME;
  params.fMaxFrameRate = fps;
	params.iPicWidth = width;
  params.iPicHeight = height;
  params.iTargetBitrate = bitrate * 1024;
  // Rate Control mode
  params.iRCMode = RC_BITRATE_MODE;

	// The following parameters are extension parameters (they're in SEncParamExt,
  // not in SEncParamBase).
  params.bEnableFrameSkip = true;
  // |uiIntraPeriod|    - multiple of GOP size
  // |keyFrameInterval| - number of frames
  params.uiIntraPeriod = intraPeriod;
  params.uiMaxNalSize = 1500;
  // Threading model: use auto.
  //  0: auto (dynamic imp. internal encoder)
  //  1: single thread (default value)
  // >1: number of threads
  params.iMultipleThreadIdc = 1;
  // The base spatial layer 0 is the only one we use.
  params.sSpatialLayers[0].iVideoWidth = params.iPicWidth;
  params.sSpatialLayers[0].iVideoHeight = params.iPicHeight;
  params.sSpatialLayers[0].fFrameRate = params.fMaxFrameRate;
  params.sSpatialLayers[0].iSpatialBitrate = params.iTargetBitrate;
  params.sSpatialLayers[0].uiProfileIdc = PRO_BASELINE;
  params.sSpatialLayers[0].uiLevelIdc = LEVEL_3_1;
	/*
  params.sSpatialLayers[0].uiProfileIdc = PRO_HIGH;
  params.sSpatialLayers[0].uiLevelIdc = LEVEL_5_0;
  */
  params.sSpatialLayers[0].iDLayerQp = 0;

	// SingleNalUnit:
	// Limit the size of the packets produced.
	params.sSpatialLayers[0].sSliceArgument.uiSliceNum = 1;
	params.sSpatialLayers[0].sSliceArgument.uiSliceMode = SM_SIZELIMITED_SLICE;

	//Initialize.
	if(enc->InitializeExt(&params) != 0) {
		return Error("Failed to initialize h264 encoder");
	}

	int video_format = EVideoFormatType::videoFormatI420;
	enc->SetOption(ENCODER_OPTION_DATAFORMAT, &video_format);

	//We are opened
	opened = true;

	// Exit
	return 1;
}


/**********************
* EncodeFrame
***********************/
VideoFrame* H264Encoder::EncodeFrame(BYTE *buffer, DWORD bufferSize)
{
	if(!opened) {
		Error("-Codec not opened");
		return NULL;
	}

	if(numPixels * 3 /2 != bufferSize) {
		Error("-EncodeFrame length error [%d,%d]",numPixels*5/4,bufferSize);
		return NULL;
	}

	bool send_key_frame = keyFrameRequest;

	memset(&pic, 0, sizeof(SSourcePicture));
	pic.iPicWidth = width;
	pic.iPicHeight = height;
	pic.iColorFormat = EVideoFormatType::videoFormatI420;
	pic.iStride[0] = width;
	pic.iStride[1] = width / 2;
	pic.iStride[2] = width / 2;
	pic.pData[0] = buffer;
	pic.pData[1] = buffer + numPixels;
	pic.pData[2] = buffer + numPixels * 5 / 4;

	if(send_key_frame) {
		// API doc says ForceIntraFrame(false) does nothing, but calling this
		// function forces a key frame regardless of the |bIDR| argument's value.
		// (If every frame is a key frame we get lag/delays.)
		enc->ForceIntraFrame(true);
		keyFrameRequest = false;
	}

	//EncodeFrame output.
	SFrameBSInfo info;
	memset(&info, 0, sizeof(SFrameBSInfo));

	//Encode!
	int enc_ret = enc->EncodeFrame(&pic, &info);
	if(enc_ret != 0) {
		Error("Error encoding frame [ret=%d]", enc_ret);
		return NULL;
	}

	if(info.eFrameType == videoFrameTypeSkip)
		return NULL;

	//Check size
	if(!frame) {
		//Create new frame
		frame = new VideoFrame(type, 0);
	}

	//Emtpy rtp info
	frame->ClearRTPPacketizationInfo();
	frame->SetLength(0);

	//Set the media
	for(int layer = 0; layer < info.iLayerNum; ++layer) {
		const SLayerBSInfo& layerInfo = info.sLayerInfo[layer];
		size_t layer_len = 0;
		for(int nal = 0; nal < layerInfo.iNalCount; ++nal) {
			DWORD sc_len = (layerInfo.pBsBuf[layer_len + 2] == 0x01)? 3 : 4;
			DWORD fragSize = layerInfo.pNalLengthInByte[nal] - sc_len;
			DWORD fragOffset = frame->GetLength() + layer_len + sc_len;
			frame->AddRtpPacket(fragOffset, fragSize, NULL, 0);

			layer_len += layerInfo.pNalLengthInByte[nal];
		}
		frame->AppendMedia(layerInfo.pBsBuf, layer_len);
	}

	if(frame->GetLength() == 0)
		return NULL;

	//Set width and height
	frame->SetWidth(width);
	frame->SetHeight(height);

	//Set intra
	frame->SetIntra((info.eFrameType == videoFrameTypeIDR)? true : false);

	return frame;
}

/**********************
* FastPictureUpdate
***********************/
int H264Encoder::FastPictureUpdate()
{
	keyFrameRequest = true;
	return 1;
}

