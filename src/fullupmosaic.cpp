#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "log.h"
#include "fullupmosaic.h"
#include <stdexcept>

/***********************
* FullupMosaic
*	Constructor
************************/
FullupMosaic::FullupMosaic(Type type, DWORD size) : Mosaic(type,size)
{
	clipImgWidth = 0;
	clipImgHeight = 0;
	clipBufferSize = 0;
	clipBuffer = NULL;
}

/***********************
* FullupMosaic
*	Destructor
************************/
FullupMosaic::~FullupMosaic()
{
	if(clipBuffer) free(clipBuffer);
}

/*****************************
* Update
* 	Update slot of mosaic with given image
*****************************/
int FullupMosaic::Update(int pos, BYTE *image, int imgWidth, int imgHeight, bool keepAspectRatio)
{
	//Check it's in the mosaic
	if (pos < 0 || pos >= numSlots)
		return 0;

	//Check size
	if (!image || !(imgHeight > 0 && imgHeight > 0))
	{
		//Clean position
		Clean(pos);
		//Exit
		return 0;
	}

	DWORD mosaicNumPixels = mosaicTotalWidth * mosaicTotalHeight;
	DWORD offset,offset2;
	BYTE *lineaY;
	BYTE *lineaU;
	BYTE *lineaV;

	//Get positions
	int left = GetLeft(pos);
	int top = GetTop(pos);
	int mosaicWidth = GetWidth(pos);
	int mosaicHeight = GetHeight(pos);

	//Get offsets
	offset = (mosaicTotalWidth * top) + left;
	offset2 = (mosaicTotalWidth * top) / 4 + left / 2;

	//Get plane pointers
	lineaY = mosaic + offset;
	lineaU = mosaic + mosaicNumPixels + offset2;
	lineaV = lineaU + mosaicNumPixels / 4;

	DWORD imgNumPixels = imgHeight * imgWidth;
	double imageRatio = (double)imgWidth / (double)imgHeight;
	double mosaicRatio = (double)mosaicWidth / (double)mosaicHeight;
	int adjustImgWidth = imgWidth;
	int adjustImgHeight = imgHeight;
	int imageleft = 0;
	int imagetop = 0;
	if(imageRatio > mosaicRatio) {
		adjustImgWidth = imgHeight * mosaicRatio;
		adjustImgWidth = (adjustImgWidth - 1) / 2 * 2;
		imageleft = (imgWidth - adjustImgWidth) / 2;
		imageleft = (imageleft - 1) / 2 * 2;
	}
	else {
		adjustImgHeight = imgWidth / mosaicRatio;
		adjustImgHeight = (adjustImgHeight - 1) / 2 * 2;
		imagetop = (imgHeight - adjustImgHeight) / 2;
		imagetop = (imagetop - 1) / 2 * 2;
	}

	BYTE *imageY = image + (imgWidth * imagetop) + imageleft;
	BYTE *imageU  = image  + imgNumPixels + (imgWidth * imagetop) / 4 + imageleft / 2;
	BYTE *imageV  = imageU + imgNumPixels / 4;

	//Check if the sizes are equal 
	if ((adjustImgWidth == mosaicWidth) && (adjustImgHeight == mosaicHeight))
	{
		//Copy Y plane
		for (int i = 0; i < adjustImgHeight; i++) 
		{
			//Copy Y line
			memcpy(lineaY, imageY, adjustImgWidth);

			//Go to next
			lineaY += mosaicTotalWidth;
			imageY += imgWidth;
		}

		//Copy U and V planes
		for (int i = 0; i < adjustImgHeight / 2; i++) 
		{
			//Copy U and V lines
			memcpy(lineaU, imageU, adjustImgWidth / 2);
			memcpy(lineaV, imageV, adjustImgWidth / 2);

			//Go to next
			lineaU += mosaicTotalWidth / 2;
			lineaV += mosaicTotalWidth / 2;

			imageU += imgWidth / 2;
			imageV += imgWidth / 2;
		}
	}
	else {
		if(clipImgWidth != adjustImgWidth || clipImgHeight != adjustImgHeight) {
			clipImgWidth = adjustImgWidth;
			clipImgHeight = adjustImgHeight;

			int tmpBufferSize = adjustImgWidth * adjustImgHeight * 3 / 2 + AV_INPUT_BUFFER_PADDING_SIZE + 32;
			if(clipBufferSize != tmpBufferSize) {
				if(clipBuffer) {
					free(clipBuffer);
					clipBuffer = NULL;
				}
				clipBufferSize = tmpBufferSize;
				clipBuffer = (BYTE*)malloc32(clipBufferSize);
			}

			DWORD clipNumPixels = clipImgWidth * clipImgHeight;
			clipY = clipBuffer;
			clipU = clipBuffer + clipNumPixels;
			clipV = clipU + clipNumPixels / 4;
		}

		BYTE *tmpY = clipY;
		for(int i = 0; i < adjustImgHeight; i++) {
			memcpy(tmpY, imageY, adjustImgWidth);
			tmpY += adjustImgWidth;
			imageY += imgWidth;
		}
		BYTE *tmpU = clipU;
		BYTE *tmpV = clipV;
		for(int i = 0; i < adjustImgHeight / 2; i++) {
			memcpy(tmpU, imageU, adjustImgWidth / 2);
			memcpy(tmpV, imageV, adjustImgWidth / 2);

			tmpU += adjustImgWidth / 2;
			tmpV += adjustImgWidth / 2;
			imageU += imgWidth / 2;
			imageV += imgWidth / 2;
		}

		//Set resize
		resizer[pos]->SetResize(clipImgWidth, clipImgHeight, clipImgWidth, mosaicWidth, mosaicHeight, mosaicTotalWidth, keepAspectRatio);

		//And resize
		resizer[pos]->Resize(clipY, clipU, clipV, lineaY, lineaU, lineaV);
	}

	//We have changed
	SetChanged();

	return 1;
}

/*****************************
* Clean
* 	Clean slot image
*****************************/
int FullupMosaic::Clean(int pos)
{
	//Check it's in the mosaic
	if (pos<0 || pos >= numSlots)
		return 0;

	DWORD mosaicNumPixels = mosaicTotalWidth * mosaicTotalHeight;
	DWORD offset,offset2;
	BYTE *lineaY;
	BYTE *lineaU;
	BYTE *lineaV;

	//Get positions in mosaic
	int left = GetLeft(pos);
	int top = GetTop(pos);
	int mosaicWidth = GetWidth(pos);
	int mosaicHeight = GetHeight(pos);

	//Get offsets
	offset = (mosaicTotalWidth * top) + left;
	offset2 = (mosaicTotalWidth * top) / 4 + left / 2;

	//Get plane pointers
	lineaY = mosaic + offset;
	lineaU = mosaic + mosaicNumPixels + offset2;
	lineaV = lineaU + mosaicNumPixels / 4;

	//Copy Y plane
	for (int i = 0; i < mosaicHeight; i++)
	{
		//Copy Y line
		//memset(lineaY, 0, mosaicWidth);
		memset(lineaY, MOSAIC_BG_Y, mosaicWidth);
		//Go to next
		lineaY += mosaicTotalWidth;
	}

	//Copy U and V planes
	for (int i = 0; i < mosaicHeight / 2; i++)
	{
		//Copy U and V lines
		//memset(lineaU, (BYTE)-128, mosaicWidth / 2);
		//memset(lineaV, (BYTE)-128, mosaicWidth / 2);
		memset(lineaU, MOSAIC_BG_U, mosaicWidth / 2);
		memset(lineaV, MOSAIC_BG_V, mosaicWidth / 2);

		//Go to next
		lineaU += mosaicTotalWidth / 2;
		lineaV += mosaicTotalWidth / 2;
	}

	//We have changed
	SetChanged();

	return 1;
}

int FullupMosaic::GetWidth(int pos)
{
	//Check it's in the mosaic
	if (pos >= numSlots)
		return 0;

	BYTE cols = 1;

	switch(mosaicType) {

	case mosaic1fu:
		cols = 1;
		break;

	case mosaic2fu:
		cols = 2;
		break;

	case mosaic3fu:
		cols = 2;
		break;

	case mosaic5fu:
		if(pos < 2)	cols = 2;
		else				cols = 3;
		break;
	}

	return (mosaicTotalWidth / cols);
}

int FullupMosaic::GetHeight(int pos)
{
	//Check it's in the mosaic
	if (pos >= numSlots)
		return 0;

	BYTE rows = 1;

	switch(mosaicType) {

	case mosaic1fu:
		rows = 1;
		break;

	case mosaic2fu:
		rows = 1;
		break;

	case mosaic3fu:
		if(pos < 1)	rows = 1;
		else				rows = 2;
		break;

	case mosaic5fu:
		rows = 2;

	}

	return (mosaicTotalHeight / rows);
}

int FullupMosaic::GetTop(int pos)
{
	//Check it's in the mosaic
	if (pos >= numSlots)
		return 0;

	BYTE index;
	BYTE rows = 1;

	switch(mosaicType) {

	case mosaic1fu:
		rows = 1;
		index = ((BYTE[]){0})[pos];
		break;
	
	case mosaic2fu:
		rows = 1;
		index = ((BYTE[]){0,0})[pos];
		break;

	case mosaic3fu:
		if(pos < 1)	rows = 1;
		else				rows = 2;
		index = ((BYTE[]){0,0,1})[pos];
		break;

	case mosaic5fu:
		rows = 2;
		index = ((BYTE[]){0,0,1,1,1})[pos];
		break;

	}

	int mosaicHeight = mosaicTotalHeight / rows;
	return (mosaicHeight * index);
}

int FullupMosaic::GetLeft(int pos)
{
	//Check it's in the mosaic
	if (pos >= numSlots)
		return 0;

	BYTE index;
	BYTE cols = 1;

	switch(mosaicType) {

	case mosaic1fu:
		cols = 1;
		index = ((BYTE[]){0})[pos];
		break;

	case mosaic2fu:
		cols = 2;
		index = ((BYTE[]){0,1})[pos];
		break;

	case mosaic3fu:
		cols = 2;
		index = ((BYTE[]){0,1,1})[pos];
		break;

	case mosaic5fu:
		if(pos < 2)	cols = 2;
		else				cols = 3;
		index = ((BYTE[]){0,1,0,1,2})[pos];
		break;

	}

	int mosaicWidth = mosaicTotalWidth / cols;
	return (mosaicWidth * index);
}
