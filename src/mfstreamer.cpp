/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : mfstreamer.cpp
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2016.09.23
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "log.h"
#include "codecs.h"
#include "rtp.h"
#include "mfstreamer.h"
#include "video.h"
#include "audio.h"


MFStreamer::MFStreamer(Listener* listener)
{
	//Save listener
	this->listener = listener;
	//NO file
	fmtCtx = NULL;
	//Not playing
	playing = false;
	//Not opened
	opened = false;
	//No tracks
	audio = NULL;
	video = NULL;
	useAudioCodec = false;
	useVideoCodec = false;
	t = 0;
	//Inciamos lso mutex y la condicion
	pthread_mutex_init(&mutex,0);
	pthread_cond_init(&cond,0);
	//Clean thread
	setZeroThread(&thread);
}

MFStreamer::~MFStreamer()
{
	//Check if still opened
	if (opened)
		//Close us
		Close();
	
	//Check tracks and delete
	if (audio)
		delete (audio);
	if (video)
		delete (video);
	//Liberamos los mutex
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

int MFStreamer::Open(const char* filename)
{
	//Log
	Log(">Media File opening [%s]",filename);

	//Lock
	pthread_mutex_lock(&mutex);

	//If already opened
	if (opened)
	{
		//Unlock
		pthread_mutex_unlock(&mutex);
		//Return error
		return Error("Already opened");
	}
	
	// Open media file
	int		ret;
	ret = avformat_open_input(&fmtCtx, filename, NULL, NULL);
	if(ret == 0) {
		ret = avformat_find_stream_info(fmtCtx, NULL);
	}

	// If not valid
	if(ret != 0) {
		//Unlock
		pthread_mutex_unlock(&mutex);
		//Return error
		return Error("File [%s] Open Fail (ret:%d)", filename, ret);
	}
	
	//No tracks
	audio = NULL;
	video = NULL;
	useAudioCodec = false;
	useVideoCodec = false;

	//IterAate thougth tracks
	int		i = 0;
	AVCodec	*avCodec;
	for(i = 0; i < fmtCtx->nb_streams; i++) {
		avCodec = avcodec_find_decoder(fmtCtx->streams[i]->codec->codec_id);
		Log("-Found stream [idx:%d][type:%s][codec_id:%d][codec_name=%s]",
				i,
				av_get_media_type_string(fmtCtx->streams[i]->codec->codec_type),
				fmtCtx->streams[i]->codec->codec_id,
				(avCodec != NULL)? avCodec->name : "UNKNOWN");

		if(fmtCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO && !audio) {
			switch(fmtCtx->streams[i]->codec->codec_id) {
			/*
			case AV_CODEC_ID_PCM_ALAW:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::PCMA);
				break;
			case AV_CODEC_ID_PCM_MULAW:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::PCMU);
				break;
			case AV_CODEC_ID_GSM:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::GSM);
				break;
			case AV_CODEC_ID_SPEEX:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::SPEEX16);
				break;
			case AV_CODEC_ID_AMR_NB:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::AMR);
				break;
			case AV_CODEC_ID_AMR_WB:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::AMRWB);
				break;
			case AV_CODEC_ID_OPUS:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::OPUS);
				break;
			case AV_CODEC_ID_ADPCM_G722:
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::G722);
				break;
			*/
			case AV_CODEC_ID_AAC:
				/*
				avcodec_open2(fmtCtx->streams[i]->codec, avCodec, NULL);
				*/
				useAudioCodec = true;
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::AAC);
				break;
			case AV_CODEC_ID_MP3:
				/*
				avcodec_open2(fmtCtx->streams[i]->codec, avCodec, NULL);
				*/
				useAudioCodec = true;
				audio = new MFRtpTrack(MediaFrame::Audio, AudioCodec::MP3);
				break;
			default:
				Log("-Unsupported Audio Codec");
				continue;
			}

			audio->timeScale = fmtCtx->streams[i]->time_base.den;
			audio->fmtCtx = fmtCtx;
			audio->streamId = i;
		}
		else if(fmtCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && !video) {
			switch(fmtCtx->streams[i]->codec->codec_id) {
			/*
			case AV_CODEC_ID_H263:
				video = new MFRtpTrack(MediaFrame::Video, VideoCodec::H263_1998);
				break;
			case AV_CODEC_ID_MPEG4:
				video = new MFRtpTrack(MediaFrame::Video, VideoCodec::MPEG4);
				break;
			*/
			case AV_CODEC_ID_H264:
				/*
				avcodec_open2(fmtCtx->streams[i]->codec, avCodec, NULL);
				*/
				useVideoCodec = true;
				video = new MFRtpTrack(MediaFrame::Video, VideoCodec::H264);
				break;
			/*
			case AV_CODEC_ID_VP6:
				video = new MFRtpTrack(MediaFrame::Video, VideoCodec::VP6);
				break;
			case AV_CODEC_ID_VP8:
				video = new MFRtpTrack(MediaFrame::Video, VideoCodec::VP8);
				break;
			*/
			default:
				Log("-Unsupported Video Codec");
				continue;
			}

			video->timeScale = fmtCtx->streams[i]->time_base.den;
			video->fmtCtx = fmtCtx;
			video->streamId = i;
		}
	}

	//We are opened
	opened = true;

	//Unlock
	pthread_mutex_unlock(&mutex);
	
	return 1;
}

int MFStreamer::SerializeCodecCtx(MediaFrame::Type type, char* serial)
{
	AVCodecContext	*ctx;

	switch(type) {
	case MediaFrame::Audio:
		ctx = GetAudioCodecCtx();
		break;
	case MediaFrame::Video:
		ctx = GetVideoCodecCtx();
		break;
	}

	if(!ctx) return 0;

	char *start = serial;

	//codec_type
	*(AVMediaType*)serial = ctx->codec_type;
	serial += sizeof(AVMediaType);

	//codec_id
	*(AVCodecID*)serial = ctx->codec_id;
	serial += sizeof(AVCodecID);

	//codec_tag
	*(unsigned int*)serial = ctx->codec_tag;
	serial += sizeof(unsigned int);

	//bit_rate
	*(int64_t*)serial = ctx->bit_rate;
	serial += sizeof(int64_t);

	//extradata_size & extradata
	*(int*)serial = ctx->extradata_size;
	serial += sizeof(int);
	if(ctx->extradata_size > 0) {
		memcpy(serial, ctx->extradata, ctx->extradata_size);
		serial += ctx->extradata_size;
	}

	//time_base
	*(AVRational*)serial = ctx->time_base;
	serial += sizeof(AVRational);

	//ticks_per_frame
	*(int*)serial = ctx->ticks_per_frame;
	serial += sizeof(int);

	//width
	*(int*)serial = ctx->width;
	serial += sizeof(int);

	//height
	*(int*)serial = ctx->height;
	serial += sizeof(int);

	//coded_width
	*(int*)serial = ctx->coded_width;
	serial += sizeof(int);

	//coded_height
	*(int*)serial = ctx->coded_height;
	serial += sizeof(int);

	//gop_size
	*(int*)serial = ctx->gop_size;
	serial += sizeof(int);

	//pix_fmt
	*(AVPixelFormat*)serial = ctx->pix_fmt;
	serial += sizeof(AVPixelFormat);

	//max_b_frames
	*(int*)serial = ctx->max_b_frames;
	serial += sizeof(int);

	//has_b_frames
	*(int*)serial = ctx->has_b_frames;
	serial += sizeof(int);

	//sample_aspect_ratio
	*(AVRational*)serial = ctx->sample_aspect_ratio;
	serial += sizeof(AVRational);

	//refs
	*(int*)serial = ctx->refs;
	serial += sizeof(int);

	//chroma_sample_location
	*(AVChromaLocation*)serial = ctx->chroma_sample_location;
	serial += sizeof(AVChromaLocation);

	//slices
	*(int*)serial = ctx->slices;
	serial += sizeof(int);

	//field_order
	*(AVFieldOrder*)serial = ctx->field_order;
	serial += sizeof(AVFieldOrder);

	//sample_rate
	*(int*)serial = ctx->sample_rate;
	serial += sizeof(int);

	//channels
	*(int*)serial = ctx->channels;
	serial += sizeof(int);

	//sample_fmt
	*(AVSampleFormat*)serial = ctx->sample_fmt;
	serial += sizeof(AVSampleFormat);

	//frame_size
	*(int*)serial = ctx->frame_size;
	serial += sizeof(int);

	//frame_number
	*(int*)serial = ctx->frame_number;
	serial += sizeof(int);

	//idct_algo
	*(int*)serial = ctx->idct_algo;
	serial += sizeof(int);

	//bits_per_coded_sample
	*(int*)serial = ctx->bits_per_coded_sample;
	serial += sizeof(int);

	//bits_per_raw_sample
	*(int*)serial = ctx->bits_per_raw_sample;
	serial += sizeof(int);

	//profile
	*(int*)serial = ctx->profile;
	serial += sizeof(int);

	//level
	*(int*)serial = ctx->level;
	serial += sizeof(int);

	//framerate
	*(AVRational*)serial = ctx->framerate;
	serial += sizeof(AVRational);

	//pkt_timebase
	*(AVRational*)serial = ctx->pkt_timebase;
	serial += sizeof(AVRational);

	return serial - start;
}

AVCodecContext* MFStreamer::GetAudioCodecCtx() 
{
	if(!audio || !useAudioCodec) return NULL;
	return fmtCtx->streams[audio->streamId]->codec;
}

AVCodecContext* MFStreamer::GetVideoCodecCtx()
{
	if(!video || !useVideoCodec) return NULL;
	return fmtCtx->streams[video->streamId]->codec;
}

int MFStreamer::Play()
{
	Log(">Media File Play");
	
	//Stop just in case
	Stop();

	//Lock
	pthread_mutex_lock(&mutex);

	//Check we are opened
	if (!opened)
	{
		//Unlock
		pthread_mutex_unlock(&mutex);
		//Exit
		return Error("Media File not opened!");
	}
	
	//We are playing
	playing = 1;

	//Arrancamos los procesos
	createPriorityThread(&thread,play,this,0);

	//Unlock
	pthread_mutex_unlock(&mutex);

	Log("<Media File Play");

	return playing;
}

void* MFStreamer::play(void* par)
{
	Log("-PlayThread [%p]",pthread_self());

	MFStreamer *player = (MFStreamer *)par;

	blocksignals();

	player->PlayLoop();
	//Exit
	return NULL;
}

int MFStreamer::PlayLoop()
{
	QWORD audioNext = MF_INVALID_TIMESTAMP;
	QWORD videoNext = MF_INVALID_TIMESTAMP;
	timeval tv ;
	timespec ts;

	Log(">MFStreamer::PlayLoop()");

	// If we have audio
	if (audio)
	{
		//Reset
		audio->Reset();
		// Send audio
		audioNext = audio->GetNextFrameTime();
	}

	// If we have video
	if (video)
	{
		//Reset
		video->Reset();
		// Send video
		videoNext = video->GetNextFrameTime();
	}

	// Calculate start time
	getUpdDifTime(&tv);

	//Lock
	pthread_mutex_lock(&mutex);

	//Reset time counter
	t = 0;

	AVPacket	avPkt;
	while(opened && playing && (!(audioNext == MF_INVALID_TIMESTAMP && videoNext == MF_INVALID_TIMESTAMP))) {
		t = (audioNext < videoNext)? audioNext : videoNext;

		// Wait time diff
		QWORD now = (QWORD)getDifTime(&tv) / 1000;

		if(t > now) {
			//Calculate timeout
			calcAbsTimeout(&ts,&tv,t);
			//Wait next or stopped
			pthread_cond_timedwait(&cond,&mutex,&ts);
			//loop
			continue;
		}

		// if we have to send audio
		if(audioNext <= t) {
			audioNext = ReadFrame(MediaFrame::Audio, &avPkt);
			if(audioNext != MF_INVALID_TIMESTAMP) {
				audioNext = audio->Read(listener, &avPkt);
				av_packet_unref(&avPkt);
			}
		}
		// or video
		if(videoNext <= t) {
			videoNext = ReadFrame(MediaFrame::Video, &avPkt);
			if(videoNext != MF_INVALID_TIMESTAMP) {
				videoNext = video->Read(listener, &avPkt);
				av_packet_unref(&avPkt);
			}
		}
	}

	//Check if we were stoped
	bool stoped = !opened || !playing;

	//Not playing anymore
	playing = 0;

	//Unlock
	pthread_mutex_unlock(&mutex);

	//Check end of file
	if (!stoped && listener)
		//End of file
		listener->onEnd();

	Log("<MFStreamer::PlayLoop()");

	return 1;
}

QWORD MFStreamer::ReadFrame(MediaFrame::Type media, AVPacket* avPkt)
{
	int			audioIdx = -1;
	int			videoIdx = -1;
	AVPacket	packet;

	if(audio) audioIdx = audio->streamId;
	if(video) videoIdx = video->streamId;

	while(1) {
		if(media == MediaFrame::Audio && !audioPkt.empty()) {
			*avPkt = audioPkt.front();
			audioPkt.pop_front();
			return 0;
		}
		if(media == MediaFrame::Video && !videoPkt.empty()) {
			*avPkt = videoPkt.front();
			videoPkt.pop_front();
			return 0;
		}

		if(av_read_frame(fmtCtx, &packet) < 0) break;
		if(packet.stream_index == audioIdx) {
			audioPkt.push_back(packet);
		}
		else if(packet.stream_index == videoIdx) {
			videoPkt.push_back(packet);
		}
	}

	return MF_INVALID_TIMESTAMP;
}

int MFStreamer::Stop()
{
	Pause();
	
	pthread_mutex_lock(&mutex);
	seek(0);
	pthread_mutex_unlock(&mutex);
}

int MFStreamer::Pause()
{
	//Check
	if (!playing)
		return 0;

	Log(">Media File Stop(Pause)");

	//Lock
	pthread_mutex_lock(&mutex);

	//Change playing state
	playing = false;

	//Get running thread
	pthread_t running = thread;

	//Clean thread
	setZeroThread(&thread);

	//Signal
	pthread_cond_signal(&cond);

	//Unlock
	pthread_mutex_unlock(&mutex);

	//If got thread
	if (running)
		//Wait for thread, will return EDEADLK if called from same thread, i.e. in from onEnd
		pthread_join(running,NULL);


	Log("<Media File Stop(Pause)");
	
	return !playing;
}

int MFStreamer::Resume()
{
	Log(">Media File Resume");
	
	//Stop just in case
	Pause();

	//Lock
	pthread_mutex_lock(&mutex);

	//Check we are opened
	if (!opened)
	{
		//Unlock
		pthread_mutex_unlock(&mutex);
		//Exit
		return Error("Media File not opened!");
	}
	
	//We are playing
	playing = 1;

	//Arrancamos los procesos
	createPriorityThread(&thread,play,this,0);

	//Unlock
	pthread_mutex_unlock(&mutex);

	Log("<Media File Resume");

	return playing;
}

int MFStreamer::Seek(QWORD time)
{
	Pause();
	
	pthread_mutex_lock(&mutex);
	seek(time);
	pthread_mutex_unlock(&mutex);
	
	return Resume();
}

void MFStreamer::seek(QWORD time)
{
	int64_t		ts;

	Log(">Media File seek to %d", time);

	if(audio) {
		ts = av_rescale(time, fmtCtx->streams[audio->streamId]->time_base.den, fmtCtx->streams[audio->streamId]->time_base.num);
		ts /= 1000;
		if(avformat_seek_file(fmtCtx, audio->streamId, 0, ts, ts, AVSEEK_FLAG_ANY) < 0) {
			return;
		}
	}
	else if(video) {
		ts = av_rescale(time, fmtCtx->streams[video->streamId]->time_base.den, fmtCtx->streams[video->streamId]->time_base.num);
		ts /= 1000;
		if(avformat_seek_file(fmtCtx, video->streamId, 0, ts, ts, AVSEEK_FLAG_ANY) < 0) {
			return;
		}
	}
	/*
	if(useAudioCodec) avcodec_flush_buffers(fmtCtx->streams[audio->streamId]->codec);
	if(useVideoCodec) avcodec_flush_buffers(fmtCtx->streams[video->streamId]->codec);
	*/

	AVPacket	packet;
	while(!audioPkt.empty()) {
		packet = audioPkt.front();
		audioPkt.pop_front();
		av_packet_unref(&packet);
	}
	while(!videoPkt.empty()) {
		packet = videoPkt.front();
		videoPkt.pop_front();
		av_packet_unref(&packet);
	}

	memset(&packet, 0x00, sizeof(AVPacket));
	packet.data = (uint8_t*)malloc(4);
	packet.data[0] = 's';
	packet.data[1] = 'e';
	packet.data[2] = 'e';
	packet.data[3] = 'k';
	packet.size = 4;
	audioPkt.push_back(packet);

	memset(&packet, 0x00, sizeof(AVPacket));
	packet.data = (uint8_t*)malloc(4);
	packet.data[0] = 's';
	packet.data[1] = 'e';
	packet.data[2] = 'e';
	packet.data[3] = 'k';
	packet.size = 4;
	videoPkt.push_back(packet);

	
	Log("<Media File seeked");
}

int MFStreamer::Close()
{
	Log(">Media File Close");
	
	//Lock
	pthread_mutex_lock(&mutex);

	//Change  state
	opened = false;

	//If it waas playing
	if (playing)
	{
		//Not playing
		playing = 0;

		//Signal
		pthread_cond_signal(&cond);

		//Get running thread
		pthread_t running = thread;

		//Clean thread
		setZeroThread(&thread);

		//Unlock
		pthread_mutex_unlock(&mutex);

		//Check thread
		if (running)
			//Wait for running thread
			pthread_join(running,NULL);
	} else {
		//Unlock
		pthread_mutex_unlock(&mutex);
	}

	if(audio) {
		delete (audio);
		audio = NULL;
	}
	if(video) {
		delete (video);
		video = NULL;
	}

	//If we have been opened
	if(fmtCtx != NULL) {
		/*
		if(useAudioCodec) avcodec_close(fmtCtx->streams[audio->streamId]->codec);
		if(useVideoCodec) avcodec_close(fmtCtx->streams[video->streamId]->codec);
		*/
		avformat_close_input(&fmtCtx);
		fmtCtx = NULL;
	}
	
	Log("<Media File Close");

	return 1;
}

int MFRtpTrack::Reset()
{
	next_pts = 0;
}

QWORD MFRtpTrack::Read(Listener* listener, AVPacket* avPkt)
{
	BYTE	*data = NULL;
	DWORD	dataLen = 0;
	int		pos = 0;
	int		last;

	data = rtp.GetMediaData();
	do {
		dataLen = rtp.GetMaxMediaLength();
		if(dataLen > avPkt->size - pos) {
			dataLen = avPkt->size - pos;
		}

		memcpy(data, &avPkt->data[pos], dataLen);
		pos += dataLen;

		last = (pos == avPkt->size)? 1 : 0;
		rtp.SetMark(last);
		rtp.SetMediaLength(dataLen);
		listener->onRTPPacket(rtp);

	} while(pos < avPkt->size);

	next_pts += avPkt->duration;
	return GetNextFrameTime();
}

QWORD MFRtpTrack::GetNextFrameTime()
{
	QWORD	ts;

	ts = next_pts * 1000 / timeScale;

	return ts;
}


double MFStreamer::GetDuration()
{
	double	audioDuration = 0;
	double	videoDuration = 0;
	
	if(audio) {
		audioDuration = fmtCtx->streams[audio->streamId]->duration / (int64_t)audio->timeScale;
	}
	if(video) {
		videoDuration = fmtCtx->streams[video->streamId]->duration / (int64_t)video->timeScale;
	}

	return (audioDuration >= videoDuration)? audioDuration : videoDuration;
}

DWORD MFStreamer::GetVideoWidth()
{
	return fmtCtx->streams[video->streamId]->codec->width;
}

DWORD MFStreamer::GetVideoHeight()
{
	return fmtCtx->streams[video->streamId]->codec->height;
}

DWORD MFStreamer::GetVideoBitrate()
{
	return fmtCtx->streams[video->streamId]->codec->bit_rate;
}

double MFStreamer::GetVideoFramerate()
{
	return (int64_t)fmtCtx->streams[video->streamId]->nb_frames / GetDuration();
}

AVCDescriptor* MFStreamer::GetAVCDescriptor()
{
	return NULL;
}
