/* 
 * File:   audiopipe.cpp
 * Author: endryu
 * 
 * Created on 2016-06-29
 */

#include "audiopipe.h"
#include "log.h"
#include <math.h>

AudioPipe::AudioPipe()
{
	pthread_mutex_init(&mutex,NULL);
	pthread_cond_init(&cond,NULL);

	inited = false;
	calcVAD = false;
	recording = false;
	canceled = false;

	acu = 0;

	playRate = 0;
	recordRate = 0;
}

AudioPipe::~AudioPipe()
{
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

int AudioPipe::Init()
{
	Log("AudioPipe init");

	pthread_mutex_lock(&mutex);
	inited = true;
	pthread_mutex_unlock(&mutex);

	return true;
}

int AudioPipe::End()
{
	pthread_mutex_lock(&mutex);
	inited = false;
	pthread_cond_signal(&cond);
	pthread_mutex_unlock(&mutex);

	return true;
}

int AudioPipe::PlayBuffer(SWORD* buffer, DWORD size, DWORD frameTime, BYTE vadLevel)
{
	SWORD resampled[1024];				//based on 20ms max rate 48000Hz
	DWORD resampledSize = 1024;
	int v = -1;//vadLevel;

	if(!recording) return 0;

	//Check if we need to calculate it
	if (calcVAD && v<0 && vad.IsRateSupported(playRate))
		//Calculate vad
		v = vad.CalcVad(buffer,size,playRate);

	//Check if we are transtrating
	if (transrater.IsOpen())
	{
		//Proccess
		if (!transrater.ProcessBuffer( buffer, size, resampled, &resampledSize))
			//Error
			return Error("-PipeAudioOutput could not transrate");

		//Check if we need to calculate it
		if (calcVAD && v<0 && vad.IsRateSupported(playRate))
			//Calculate vad
			v = vad.CalcVad(buffer,size,playRate);

		//Update parameters
		buffer = resampled;
		size = resampledSize;
	}

	pthread_mutex_lock(&mutex);

	if(fifoBuffer.length() > 9600 && vadLevel > 24) {
		//Log("-AudioPipe fluctuation & drop silence audio");
		pthread_mutex_unlock(&mutex);
		return size;
	}

	//Get left space
	int left = fifoBuffer.size()-fifoBuffer.length();

	//if not enought
	if(size > left)
		//Free space
		fifoBuffer.remove(size - left);

	//Get initial bump
	if (!acu && v>0)
		//1 second minimum at 8khz
		acu+=8000;

	//Check if we have level info
	if (v>0)
	{
		double sum = 0;
		for (int i = 0; i < size ; ++i)
		{
		    double sample = buffer[i] / 32768.0;
		    sum += (sample * sample);
		}
		//Debug("-sum %f %f",sum,sum/size);
		//Set RMS level
		vadLevel = sqrt(sum)*4;
	}

	//Check we have detected speech
	if (v>0)
		//Acumule VAD at 8Khz
		acu += v*vadLevel*size*8000/playRate;

	//Debug("-%p acu:%.6d v:%.2d level:%.2d",this,acu,v,vadLevel);
	
	//Check max
	if (acu>48000)
		//Limit so it can timeout faster
		acu = 48000;

	//Metemos en la fifo
	fifoBuffer.push(buffer,size);
	pthread_cond_signal(&cond);

	//Desbloqueamos
	pthread_mutex_unlock(&mutex);

	return size;
}

int AudioPipe::StartPlaying(DWORD rate)
{
	Log("-AudioPipe start playing [rate:%d,vad:%d]",rate,calcVAD);

	//Lock
	pthread_mutex_lock(&mutex);

	//Store play rate
	playRate = rate;

	//If we already had an open transcoder
	if(transrater.IsOpen())
		//Close it
		transrater.Close();

	//if rates are different
	if(recording && playRate != recordRate)
		//Open it
		transrater.Open(playRate, recordRate);

	//Unlock
	pthread_mutex_unlock(&mutex);
	
	//Exit
	return true;
}

int AudioPipe::StopPlaying()
{
	Log("-AudioPipe stop playing");

	pthread_mutex_lock(&mutex);
	transrater.Close();
	pthread_mutex_unlock(&mutex);

	return true;
}

int AudioPipe::RecBuffer(SWORD* buffer, DWORD size)
{
	int len = 0;

	//Bloqueamos
	pthread_mutex_lock(&mutex);

	//Mientras no tengamos suficientes muestras
	while(recording && (fifoBuffer.length()<size))
	{
		//Esperamos la condicion
		pthread_cond_wait(&cond,&mutex);

		//If we have been canceled
		if (canceled)
		{
			//Remove flag
			canceled = false;
			//Exit
			Log("AudioPipe: RecBuffer cancelled");
			//End
			goto end;
		}
	}

	//Get samples from queue
	len = fifoBuffer.pop(buffer,size);

end:
	//Desbloqueamos
	pthread_mutex_unlock(&mutex);

	return len;
}

int AudioPipe::ClearBuffer()
{
	pthread_mutex_lock(&mutex);
	fifoBuffer.clear();
	pthread_mutex_unlock(&mutex);

	return true;
}

void AudioPipe::CancelRecBuffer()
{
	pthread_mutex_lock(&mutex);

	//Cancel
	canceled = true;

	pthread_cond_signal(&cond);

	pthread_mutex_unlock(&mutex);
}

int AudioPipe::StartRecording(DWORD rate)
{
	Log("-AudioPipe start recording [rate:%d]",rate);

	pthread_mutex_lock(&mutex);

	recordRate = rate;

	if(playRate && playRate != recordRate)
		//Open transrater
		transrater.Open(playRate, recordRate);

	recording = true;

	pthread_mutex_unlock(&mutex);

	return true;
}

int AudioPipe::StopRecording()
{
	Log("-AudioPipe stop recording");
	
	pthread_mutex_lock(&mutex);
	
	recording = false;

	transrater.Close();
	
	pthread_cond_signal(&cond);

	pthread_mutex_unlock(&mutex);

	return true;
}
