/*****************************************************************************
**  Copyright (C) 2008 NEXCOS Co.                                           **
**  All right Reserved                                                      **
**                                                                          **
**  This program Contains proprietary information of NEXCOS. No part of     **
**  this program may be reproduced and disclosed, except as may be          **
**  specifially authorized by prior agreement or permission of NEXCOS.      **
*****************************************************************************/

/*============================== File Header =================================
  FILE NAME     : avcodecutils.cpp
  SUBSYSTEM     :
  SOR-NAME      :
  BLOCK         : 
  VERSION       : 
  DATE          : 2017.02.06
  AUTHOR        :
  HISTORY       : 
  PROCESS(TASK) :
  FUNCTIONS     :
  DESCRIPTION   :
  REMARKS       :
============================================================================*/

#include "avcodecutils.h"


AVCodecContext* cloneCodecCtx(AVCodecContext* ctx)
{
	AVCodecContext*	clone;

	clone = avcodec_alloc_context3(ctx->codec);
	
	clone->codec_type = ctx->codec_type;
	clone->codec_id = ctx->codec_id;
	clone->codec_tag = ctx->codec_tag;
	clone->bit_rate = ctx->bit_rate;
	clone->extradata_size = ctx->extradata_size;
	if(ctx->extradata_size > 0) {
		clone->extradata = (uint8_t*)malloc(ctx->extradata_size);
		memcpy(clone->extradata, ctx->extradata, ctx->extradata_size);
	}
	clone->time_base = ctx->time_base;
	clone->ticks_per_frame = ctx->ticks_per_frame;
	clone->width = ctx->width;
	clone->height = ctx->height;
	clone->coded_width = ctx->coded_width;
	clone->coded_height = ctx->coded_height;
	clone->gop_size = ctx->gop_size;
	clone->pix_fmt = ctx->pix_fmt;
	clone->max_b_frames = ctx->max_b_frames;
	clone->has_b_frames = ctx->has_b_frames;
	clone->sample_aspect_ratio = ctx->sample_aspect_ratio;
	clone->refs = ctx->refs;
	clone->chroma_sample_location = ctx->chroma_sample_location;
	clone->slices = ctx->slices;
	clone->field_order = ctx->field_order;
	clone->sample_rate = ctx->sample_rate;
	clone->channels = ctx->channels;
	clone->sample_fmt = ctx->sample_fmt;
	clone->frame_size = ctx->frame_size;
	clone->frame_number = ctx->frame_number;
	clone->idct_algo = ctx->idct_algo;
	clone->bits_per_coded_sample = ctx->bits_per_coded_sample;
	clone->bits_per_raw_sample = ctx->bits_per_raw_sample;
	clone->profile = ctx->profile;
	clone->level = ctx->level;
	clone->framerate = ctx->framerate;
	clone->pkt_timebase = ctx->pkt_timebase;

	AVCodec *codec = avcodec_find_decoder(ctx->codec_id);
	avcodec_open2(clone, codec, NULL);

	return clone;
}
