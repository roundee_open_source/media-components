/* 
 * File:   aacdecoder.h
 * Author: endryu
 */

#ifndef AACDECODER_H
#define	AACdECODER_H

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavresample/avresample.h>
#include <libavutil/opt.h>
}
#include "config.h"
#include "codecs.h"
#include "audio.h"
#include "ffstreamer.h"


class AACDecoder : public AudioDecoder
{
public:
	AACDecoder();
	void Init(FFStreamer* streamer);
	virtual ~AACDecoder();
	virtual int Decode(BYTE* in, int inLen, SWORD* out, int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return this->rate; }
	virtual DWORD GetRate()					{ return rate; }
private:
	AVCodecContext		*ctx;
	AVFrame				*frame;
	DWORD				rate;
};

#endif	/* AACDECODER_H */

