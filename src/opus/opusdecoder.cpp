/* 
 * File:   opusdecoder.cpp
 * Author: Sergio
 * 
 * Created on 14 de marzo de 2013, 11:19
 */

#include "opusdecoder.h"
#include "log.h"

OpusDecoder::OpusDecoder()
{
	int error = 0;
	//Set type
	type = AudioCodec::OPUS;
	//Set rate
	rate = 48000;
	//Create decoder
	//dec = opus_decoder_create(rate,1,&error);
	dec = opus_decoder_create(rate,2,&error);	// create opus decoder of 2 ch.
	//Check error
	if (!dec || error)
		Error("Could not open OPUS decoder");
}

DWORD OpusDecoder::TrySetRate(DWORD rate)
{
	int error = 0;
	//Try to create a new decored with that rate
	//OpusDecoder *aux = opus_decoder_create(rate,1,&error);
	OpusDecoder *aux = opus_decoder_create(rate,2,&error);
	//If no error
	if (aux && !error)
	{
		//Destroy old one
		if (dec) opus_decoder_destroy(dec);
		//Get new decoded
		dec = aux;
		//Store new rate
		this->rate = rate;
	}
	
	//Return codec rate
	return this->rate;
}

OpusDecoder::~OpusDecoder()
{
	if (dec)
		opus_decoder_destroy(dec);
}

int OpusDecoder::Decode(BYTE *in,int inLen,SWORD* out,int outLen)
{
	SWORD outStereo[2048];

	//Decode without FEC
	//int ret = opus_decode(dec,in,inLen,out,outLen,0);
	int ret = opus_decode(dec,in,inLen,outStereo,rate*20/1000,0);
	//Check error
	if (ret<0)
		return Error("-Opus decode error [%d]",ret);

	// Convert Stereo to Mono
	for(size_t i = 0; i < ret; i++) {
		//out[i] = (outStereo[i * 2] + outStereo[i* 2 + 1]) / 2;
		out[i] = (outStereo[i * 2] > outStereo[i * 2 + 1])? outStereo[i * 2] : outStereo[i * 2 + 1];
	}
	
	//return decoded samples
	return ret;				// return frames based on 1 ch.
}

int OpusDecoder::DecodeFEC(BYTE *in,int inLen,SWORD* out,int outLen)
{
	SWORD outStereo[2048];

	int ret = opus_decode(dec, in, inLen, outStereo, rate*20/1000, 1);
	//Check error
	if (ret<0)
		return Error("-Opus decode error [%d]",ret);

	// Convert Stereo to Mono
	for(size_t i = 0; i < ret; i++) {
		//out[i] = (outStereo[i * 2] + outStereo[i* 2 + 1]) / 2;
		out[i] = (outStereo[i * 2] > outStereo[i * 2 + 1])? outStereo[i * 2] : outStereo[i * 2 + 1];
	}
	
	//return decoded samples
	return ret;				// return frames based on 1 ch.
}
