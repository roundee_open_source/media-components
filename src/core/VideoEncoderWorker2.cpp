#include "core/VideoEncoderWorker.h"
#include "log.h"
#include "tools.h"
#include "acumulator.h"

VideoEncoderWorker::VideoEncoderWorker() 
{
	//Nothing
	mediaListener = NULL;
	input = NULL;
	encoding = false;
	sendFPU = false;
	codec = (VideoCodec::Type)-1;
	//Create objects
	pthread_mutex_init(&mutex,NULL);
	pthread_cond_init(&cond,NULL);
}

VideoEncoderWorker::~VideoEncoderWorker()
{
	End();
	//Clean object
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
}

int VideoEncoderWorker::Init(VideoInput *input)
{
	//Store it
	this->input = input;
}

int VideoEncoderWorker::SetCodec(VideoCodec::Type codec,int mode,int fps,int bitrate,int intraPeriod, const Properties& properties)
{
	Log("-SetVideoCodec [%s,%d,%d,%d,%d,%d]",VideoCodec::GetNameFor(codec),codec,mode,fps,bitrate,intraPeriod);

	//Store parameters
	this->codec	  = codec;
	this->mode	  = mode;
	this->bitrate	  = bitrate;
	this->fps	  = fps;
	this->intraPeriod = intraPeriod;
	//Init limits
	this->bitrateLimit	= bitrate;
	this->bitrateLimitCount	= fps;
	//Store properties
	this->properties  = properties;

	if(this->mode < 0) {
		width = height = mode;
	}
	else {
		//Get width and height
		width = GetWidth(mode);
		height = GetHeight(mode);

		//Check size
		if (!width || !height)
			//Error
			return Error("Unknown video mode");
	}

	//Exit
	return 1;
}

int VideoEncoderWorker::Start()
{
	//Check
	if (!input)
		//Exit
		return Error("null video input");
	
	//Check if need to restart
	if (encoding)
		//Stop first
		Stop();

	//Start decoding
	encoding = 1;

	//launc thread
	createPriorityThread(&thread,startEncoding,this,0);

	return 1;
}

void * VideoEncoderWorker::startEncoding(void *par)
{
	Log("VideoEncoderWorkerThread [%p]",pthread_self());
	//Get worker
	VideoEncoderWorker *worker = (VideoEncoderWorker *)par;
	//Block all signals
	blocksignals();
	//Run
	worker->Encode();
	//Exit
	return NULL;
}

int VideoEncoderWorker::Stop()
{
	Log(">Stop VideoEncoderWorker");

	//If we were started
	if (encoding)
	{
		//Stop
		encoding=0;

		//Cancel and frame grabbing
		input->CancelGrabFrame();

		//Cancel sending
		pthread_cond_signal(&cond);

		//Esperamos
		pthread_join(thread,NULL);
	}

	Log("<Stop VideoEncoderWorker");

	return 1;
}

int VideoEncoderWorker::End()
{
	//Check if already decoding
	if (encoding)
		//Stop
		Stop();

	//Set null
	input = NULL;
}

int VideoEncoderWorker::Encode()
{
	timeval first;
	timeval prev;
	timeval lastFPU;
	
	DWORD num = 0;

	/*
	Acumulator bitrateAcu(1000);
	Acumulator fpsAcu(1000);
	*/

	Log(">SendVideo [width:%d,size:%d,bitrate:%d,fps:%d,intra:%d]",width,height,bitrate,fps,intraPeriod);

	VideoEncoder* videoEncoder = NULL;
	int grabWidth;
	int grabHeight;
	int setWidth = width;
	int setHeight = height;

	//Comrpobamos que tengamos video de entrada
	if (input == NULL)
		return Error("No video input");

	//Iniciamos el tama�o del video
	if (!input->StartVideoCapture(width,height,fps))
		return Error("Couldn't set video capture");

	//Start at 80%
	//int current = bitrate*0.8;
	//always 120% - by endryu
	int current = bitrate * 1.2;

	//No wait for first
	QWORD frameTime = 0;
	QWORD frameTimeError = 1000000 - (1000000 / fps * fps);

	//The time of the first one
	gettimeofday(&first,NULL);

	//Fist FPU
	gettimeofday(&lastFPU,NULL);

	//Started
	Log("-Sending video");

	//Mientras tengamos que capturar
	while(encoding)
	{
		if(frameTime == 0) {
			gettimeofday(&prev,NULL);
		}

		//Nos quedamos con el puntero antes de que lo cambien
		BYTE *pic = input->GrabFrame(frameTime / 10000);

		//Check picture
		if(!pic)
			//Exit
			continue;

		if(mode < 0) {
			input->GetVideoSize(&grabWidth, &grabHeight);
			if(grabWidth != setWidth || grabHeight != setHeight) {
				Log("-VideoEncoderWorker re-create VideoEncoder");
				if(videoEncoder) {
					delete videoEncoder;
					videoEncoder = NULL;
				}
				setWidth = grabWidth;
				setHeight = grabHeight;
			}
		}

		if(videoEncoder == NULL) {
			videoEncoder = VideoCodecFactory::CreateEncoder(codec, properties);
			videoEncoder->SetFrameRate(fps, bitrate, intraPeriod);
			videoEncoder->SetSize(setWidth, setHeight);
		}

		//Check if we need to send intra
		if (sendFPU)
		{
			//Do not send anymore
			sendFPU = false;
			//Do not send if just send one (100ms)
			if (getDifTime(&lastFPU)/100>100)
			{
				//Set it
				videoEncoder->FastPictureUpdate();
				//Update last FPU
				getUpdDifTime(&lastFPU);
			}
		}

		/*
		Do not change bitrata
		
		//Calculate target bitrate
		int target = current;

		//Check temporal limits for estimations
		if (bitrateAcu.IsInWindow())
		{
			//Get real sent bitrate during last second and convert to kbits
			DWORD instant = bitrateAcu.GetInstantAvg()/1000;
			//If we are in quarentine
			if (bitrateLimitCount)
				//Limit sending bitrate
				target = bitrateLimit;
			//Check if sending below limits
			else if (instant<bitrate)
				//Increase a 8% each second or fps kbps
				target += (DWORD)(target*0.08/fps)+1;
		}

		//Check target bitrate agains max conf bitrate
		if (target>bitrate*1.2)
			//Set limit to max bitrate allowing a 20% overflow so instant bitrate can get closer to target
			target = bitrate*1.2;

		//Check limits counter
		if (bitrateLimitCount>0)
			//One frame less of limit
			bitrateLimitCount--;

		//Check if we have a new bitrate
		if (target && target!=current)
		{
			Debug("VideoEncoder::SetFrameRate [fps=%d][target=%d][intraPeriod=%d]", fps, target, intraPeriod);
			//Reset bitrate
			videoEncoder->SetFrameRate(fps,target,intraPeriod);
			//Upate current
			current = target;
		}
		*/

		//Procesamos el frame
		VideoFrame *videoFrame = videoEncoder->EncodeFrame(pic,input->GetBufferSize());

		//If was failed
		if (!videoFrame) {
			//Next
			continue;
		}

		/*
		//Increase frame counter
		fpsAcu.Update(getTime()/1000,1);
		*/

		/*
		//Add frame size in bits to bitrate calculator
		bitrateAcu.Update(getDifTime(&first)/1000,videoFrame->GetLength()*8);
		*/

		if(videoEncoder->type != VideoCodec::H264Rec) {
			//Set frame timestamp
			videoFrame->SetTimestamp(getDifTime(&first)/1000);
					
			//Calculate sending times based on bitrate
			DWORD sendingTime = videoFrame->GetLength()*8/current;

			//Adjust to maximum time
			if (sendingTime > frameTime / 1000)
				//Cap it
				sendingTime = frameTime / 1000;

			//If it was a I frame
			if (videoFrame->IsIntra())
				//Clean rtp rtx buffer
				FlushRTXPackets();

			//Send it smoothly
			SmoothFrame(videoFrame,sendingTime);
		}
		else {
			SmoothFrame(videoFrame, 0);
		}
		
		//Check if we have mediaListener
		if (mediaListener)
			//Call it
			mediaListener->onMediaFrame(*videoFrame);

		frameTime = 1000000 / fps;
		if(++num == fps) {
			frameTime += frameTimeError;
			num = 0;
		}

		prev.tv_usec += frameTime;
		if(prev.tv_usec > 1000000) {
			prev.tv_sec++;
			prev.tv_usec -= 1000000;
		}

		timespec ts;
		//Lock
		pthread_mutex_lock(&mutex);
		//Calculate slept time
		//Calculate timeout
		calcAbsTimeoutNS(&ts, &prev, 0);

		//Wait next or stopped
		pthread_cond_timedwait(&cond,&mutex,&ts);

		//Unlock
		pthread_mutex_unlock(&mutex);

		/*
		//Dump statistics
		if (num && (num % (fps * 10) == 0))
		{
			//Debug("-Send bitrate current=%d avg=%llf rate=[%llf,%llf] fps=[%llf,%llf] limit=%d",current,bitrateAcu.GetInstantAvg()/1000,bitrateAcu.GetMinAvg()/1000,bitrateAcu.GetMaxAvg()/1000,fpsAcu.GetMinAvg(),fpsAcu.GetMaxAvg(),bitrateLimit);
			bitrateAcu.ResetMinMax();
			fpsAcu.ResetMinMax();
		}
		*/
	}

	Log("-SendVideo out of loop");

	//Terminamos de capturar
	input->StopVideoCapture();

	//Check
	if (videoEncoder) {
		if(videoEncoder->type == VideoCodec::H264Rec) {
			VideoFrame *videoFrame;
			while(1) {
				videoFrame = videoEncoder->EncodeFrame(NULL, 0);
				if(videoFrame) SmoothFrame(videoFrame, 0);
				else break;
			}
		}
		//Borramos el encoder
		delete videoEncoder;
	}

	//Salimos
	Log("<SendVideo [%d]",encoding);
}

int VideoEncoderWorker::SetMediaListener(MediaFrame::Listener *listener)
{
	//Set it
	Log("-SetMediaListener [listener=0x%p]", listener);
	this->mediaListener = listener;
}

int VideoEncoderWorker::SetTemporalBitrateLimit(int estimation)
{
	//Set bitrate limit
	bitrateLimit = estimation/1000;
	//Set limit of bitrate to 1 second;
	bitrateLimitCount = fps;
	//Exit
	return 1;
}
void VideoEncoderWorker::SendFPU()
{
	sendFPU = true;
}
