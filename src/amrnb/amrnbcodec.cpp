#include "amrnbcodec.h"
#include "log.h"

AMRNBEncoder::AMRNBEncoder(const Properties &properties)
{
	dtx = 1;
	mode = MR122;

	type = AudioCodec::AMR;
	numFrameSamples = 160;

	for(Properties::const_iterator it=properties.begin(); it!=properties.end(); ++it) {
		//Check
		if (it->first.compare("dtx")==0) {
			dtx = atoi(it->second.c_str());
		}
		if (it->first.compare("mode")==0) {
			mode = atoi(it->second.c_str());
		}
	}

	if(AMREncodeInit(&enc, &sidSync, dtx) < 0)
		Error("Could not open AMR encoder");
}

AMRNBEncoder::~AMRNBEncoder()
{
	AMREncodeExit(&enc, &sidSync);
}

int AMRNBEncoder::Encode (SWORD* in, int inLen, BYTE* out, int outLen)
{
	enum Frame_Type_3GPP frame_type = (enum Frame_Type_3GPP) mode;
	int ret = AMREncode(enc, sidSync, (Mode)mode, (Word16*)in, (UWord8*)out, &frame_type, AMR_TX_IETF);
	if(ret < 0)
		return Error("-AMR encode error [%d]", ret);
	
	out[0] |= 0x04;
	return ret;
}

AMRNBDecoder::AMRNBDecoder()
{
	type = AudioCodec::AMR;
	numFrameSamples = 160;

	if(GSMInitDecode(&state, (Word8*)"Deocder") < 0)
		Error("Could not open AMR decoder");
}
AMRNBDecoder::~AMRNBDecoder()
{
	GSMDecodeFrameExit(&state);
}

int AMRNBDecoder::Decode(BYTE* in, int inLen, SWORD* out, int outLen)
{
	unsigned char	type = (in[0] >> 3) & 0x0f;
	Word16			ret;

	in++;
	ret = AMRDecode(state, (enum Frame_Type_3GPP)type, (UWord8*)in, (Word16*)out, MIME_IETF);
	if(ret < 0)
		return Error("-AMR decode error [%d]", ret);

	return 160;
}
