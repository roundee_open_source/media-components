#ifndef AMRNB_H
#define AMRNB_H


#ifdef __cplusplus
extern "C" {
#endif
#include <sp_dec.h>
#include <amrdecode.h>
#include <amrencode.h>
#ifdef __cplusplus
}
#endif
#include "audio.h"


class AMRNBEncoder : public AudioEncoder
{
public:
	AMRNBEncoder(const Properties &properties);
	virtual ~AMRNBEncoder();
	virtual int Encode(SWORD* in,int inLen,BYTE* out,int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return 8000;	}
	virtual DWORD GetRate()					{ return 8000;	}
	virtual DWORD GetClockRate()			{ return 8000;	}
private:
	int		dtx;
	int		mode;
	void	*enc;
	void	*sidSync;

};


class AMRNBDecoder : public AudioDecoder
{
public:
	AMRNBDecoder();
	virtual ~AMRNBDecoder();
	virtual int Decode(BYTE* in,int inLen,SWORD* out,int outLen);
	virtual DWORD TrySetRate(DWORD rate)	{ return 8000;	}
	virtual DWORD GetRate()					{ return 8000;	}
private:
	void	*state;

};


#endif	/* AMRNB_H */
