#include "log.h"
#include "audio.h"
#include "g711/g711codec.h"
#include "gsm/gsmcodec.h"
//#include "speex/speexcodec.h"
//#include "nelly/NellyCodec.h"
#include "opus/opusencoder.h"
#include "opus/opusdecoder.h"
#include "g722/g722codec.h"
#include "aac/aacencoder.h"
#include "aac/aacdecoder.h"
#include "amrnb/amrnbcodec.h"
#include "amrwb/amrwbcodec.h"
#include "mp3/mp3encoder.h"
#include "mp3/mp3decoder.h"

AudioEncoder* AudioCodecFactory::CreateEncoder(AudioCodec::Type codec)
{
	//Empty properties
	Properties properties;

	//Create codec
	return CreateEncoder(codec,properties);
}

AudioEncoder* AudioCodecFactory::CreateEncoder(AudioCodec::Type codec, const Properties &properties)
{
	Log("-CreateAudioEncoder [%d,%s]",codec,AudioCodec::GetNameFor(codec));

	//Creamos uno dependiendo del tipo
	switch(codec)
	{
		case AudioCodec::GSM:
			return new GSMEncoder(properties);
		case AudioCodec::PCMA:
			return new PCMAEncoder(properties);
		case AudioCodec::PCMU:
			return new PCMUEncoder(properties);
		case AudioCodec::AMR:
			return new AMRNBEncoder(properties);
		case AudioCodec::OPUS:
			return new OpusEncoder(properties);
		case AudioCodec::G722:
			return new G722Encoder(properties);
		case AudioCodec::AAC:
			return new AACEncoder(properties);
		case AudioCodec::AMRWB:
			return new AMRWBEncoder(properties);
		case AudioCodec::MP3:
			return new MP3Encoder(properties);
		default:
			Error("Codec not found [%d]",codec);
	}

	return NULL;
}

AudioDecoder* AudioCodecFactory::CreateDecoder(AudioCodec::Type codec)
{
	Log("-CreateAudioDecoder [%d,%s]",codec,AudioCodec::GetNameFor(codec));

	//Creamos uno dependiendo del tipo
	switch(codec)
	{
		case AudioCodec::GSM:
			return new GSMDecoder();
		case AudioCodec::PCMA:
			return new PCMADecoder();
		case AudioCodec::PCMU:
			return new PCMUDecoder();
		case AudioCodec::AMR:
			return new AMRNBDecoder();
		case AudioCodec::OPUS:
			return new OpusDecoder();
		case AudioCodec::G722:
			return new G722Decoder();
		case AudioCodec::AAC:
			return new AACDecoder();
		case AudioCodec::AMRWB:
			return new AMRWBDecoder();
		case AudioCodec::MP3:
			return new MP3Decoder();
		default:
			Error("Codec not found [%d]",codec);
	}

	return NULL;
}

int AudioDecoder::DecodeFEC(BYTE* in,  int inLen, SWORD* out, int outLen)
{
	Log("-AudioDecoder::DecodeFEC");
	return 0;
}
