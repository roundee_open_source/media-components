###############################################################################
0. check EPEL repo
###############################################################################
yum -y install epel-release
yum repolist

###############################################################################
1. install dev tools
###############################################################################
yum install gcc-c++
yum install git

###############################################################################
2. install codecs
###############################################################################
yum install gsm-devel
yum install speex-devel
yum install lame-devel
(if didn't installed) yum install libogg-devel

###############################################################################
3. install openssl-1.1.0h
###############################################################################
./config --prefix=/usr --libdir=lib64 -fPIC shared
make
make install
-- OR --
yum install openssl-devel

###############################################################################
4. install ImageMagick++
###############################################################################
yum install ImageMagick-c++-devel

###############################################################################
5. compile & install libsrtp-2.1.0
###############################################################################
./configure --bindir=/usr/bin --libdir=/usr/lib64 --enable-openssl
make
make install

###############################################################################
6. compile & install opus-1.3.tar.gz
###############################################################################
./configure --bindir=/usr/bin --libdir=/usr/lib64 --with-pic
make
make install

###############################################################################
7. compile & install opencore-amr-0.1.5
###############################################################################
./configure --bindir=/usr/bin --libdir=/usr/lib64 --with-pic
make
make install

###############################################################################
8. compile & install vo-amrwbenc-0.1.3
###############################################################################
./configure --bindir=/usr/bin --libdir=/usr/lib64 --with-pic
make
make install

###############################################################################
9. install nasm
###############################################################################
vi /etc/yum.repos.d/nasm.repo
------------------------------------------------------------------------------
[nasm]
name=The Netwide Assembler
baseurl=http://www.nasm.us/pub/nasm/stable/linux/
enabled=1
gpgcheck=0

[nasm-testing]
name=The Netwide Assembler (release candidate builds)
baseurl=http://www.nasm.us/pub/nasm/testing/linux/
enabled=0
gpgcheck=0

[nasm-snapshot]
name=The Netwide Assembler (daily snapshot builds)
baseurl=http://www.nasm.us/pub/nasm/snapshots/latest/linux/
enabled=0
gpgcheck=0
------------------------------------------------------------------------------

yum install nasm

###############################################################################
10. compile & install libvpx
###############################################################################
git clone --depth 1 https://chromium.googlesource.com/webm/libvpx
mkdir libvpx_build
cd libvpx_build
../libvpx/configure --prefix=/usr --libdir=/usr/lib64 --enable-shared
make
make install

###############################################################################
11. compile & install libx264
###############################################################################
git clone --depth 1 http://git.videolan.org/git/x264.git
cd x264
./configure --bindir=/usr/bin --libdir=/usr/lib64 --enable-shared
make
make install

###############################################################################
12. compile & install openh264-1.8.0
###############################################################################
make
make install
vi /etc/ld.so.conf.d/openh264-1.8.0.conf
------------------------------------------------------------------------------
/usr/local/lib
------------------------------------------------------------------------------
ldconfig

###############################################################################
13. compile & install ffmpeg
###############################################################################
git clone --depth 1 http://git.videolan.org/git/ffmpeg.git
cd ffmpeg
./configure --bindir=/usr/bin --libdir=/usr/lib64 --enable-gpl --enable-version3 --enable-nonfree --disable-static --enable-shared --enable-libgsm --enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libspeex --enable-libvo-amrwbenc --enable-libvpx --enable-libx264 --enable-openssl --enable-avresample
make
make install

###############################################################################
14. install mp4v2
###############################################################################
yum install libmp4v2-devel

###############################################################################
optional. build webrtc common_audio library
###############################################################################
yum install re2c
git clone git://github.com/ninja-build/ninja.git
cd ninja/
git checkout release
./configure.py --bootstrap
cp ninja /usr/bin

cd ${source_path}/ext/out/Release
ninja
