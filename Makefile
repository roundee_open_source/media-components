include config.mk

OPTS+= -fPIC -DPIC -msse -msse2 -msse3 -DSPX_RESAMPLE_EXPORT= -DRANDOM_PREFIX=mcu -DOUTSIDE_SPEEX -DFLOATING_POINT -D__SSE2__

#DEBUG
ifeq ($(DEBUG),yes)
	OPTS+= -g -O0
	TAG=debug
else
	OPTS+= -g -O4 -fexpensive-optimizations -funroll-loops -Wno-unused-result -Wno-write-strings -Wno-deprecated-declarations
	TAG=release
endif

#LOG
ifeq ($(LOG),yes)
	OPTS+= -DLOG_
endif


############################################
#Directorios
############################################
BUILD = $(SRCDIR)/build/$(TAG)
BIN   = $(SRCDIR)/bin/$(TAG)

############################################
#Objetos
############################################
DEPACKETIZERSOBJ=

G711DIR=g711
G711OBJ=g711.o pcmucodec.o pcmacodec.o

MEDIASESSIONDIR=ms
MEDIASESSIONOBJ=RTPEndpoint.o Player2.o Endpoint.o MediaSession.o RTPMultiplexer.o AudioDecoderWorker.o AudioEncoderWorker.o AudioMixerResource.o VideoDecoderWorker.o VideoEncoderWorkerMultiplexer.o VideoMixerResource.o RTPMultiplexerSmoother.o Recorder2.o AudioTranscoder.o VideoTranscoder.o NetJoinable.o NetListener.o

RTPRTCPDIR=rtp
RTPRTCPOBJ=RTCPApp.o RTCPBye.o RTCPCommonHeader.o RTCPCompoundPacket.o RTCPExtendedJitterReport.o RTCPFullIntraRequest.o RTCPNACK.o RTCPPacket.o RTCPPayloadFeedback.o RTCPReceiverReport.o RTCPReport.o RTCPRTPFeedback.o RTCPSDES.o RTCPSenderReport.o RTPDepacketizer.o RTPHeader.o RTPHeaderExtension.o RTPMap.o RTPPacket.o RTPPacketSched.o RTPRedundantPacket.o RTPLostPackets.o RTPIncomingSource.o RTPIncomingSourceGroup.o RTPOutgoingSource.o RTPOutgoingSourceGroup.o

H264DIR=h264
H264OBJ=h264encoder.o h264encoder2.o h264decoder.o
DEPACKETIZERSOBJ+= h264depacketizer.o

VP8DIR=vp8
VP8OBJ=vp8encoder.o vp8decoder.o VP8LayerSelector.o
DEPACKETIZERSOBJ+= vp8depacketizer.o

GSMDIR=gsm
GSMOBJ=gsmcodec.o

SPEEXDIR=speex
SPEEXOBJ=resample.o

OPUSDIR=opus
OPUSOBJ=opusdecoder.o opusencoder.o

G722DIR=g722
G722OBJ=g722codec.o

AACDIR=aac
AACOBJ=aacencoder.o aacdecoder.o

AMRNBDIR=amrnb
AMRNBOBJ=amrnbcodec.o

AMRWBDIR=amrwb
AMRWBOBJ=amrwbcodec.o

MP3DIR=mp3
MP3OBJ=mp3encoder.o mp3decoder.o

COREOBJ=VideoEncoderWorker2.o
COREDIR=core

#2018.01.11 - remove fecdecoder textstream
OBJS= $(COREOBJ) $(RTPRTCPOBJ) audio.o video.o videomixer.o audiomixer.o rtpsession.o RTPTransport.o audiotransrater.o pipeaudioinput.o pipeaudiooutput.o pipevideoinput.o pipevideooutput.o framescaler.o sidebar.o mosaic.o partedmosaic.o asymmetricmosaic.o pipmosaic.o fullupmosaic.o linearPIPmosaic.o logo.o overlay.o amf.o textmixer.o textmixerworker.o pipetextinput.o pipetextoutput.o mfstreamer.o mfrecorder.o vad.o stunmessage.o crc32calc.o remoteratecontrol.o remoterateestimator.o remotevad.o audiopipe.o videopipe.o dtls.o CPUMonitor.o OpenSSL.o avcodecutils.o VideoLayerSelector.o utf8.o oggopus_container.o
OBJS+= $(G711OBJ) $(GSMOBJ) $(H264OBJ) $(SPEEXOBJ) $(G722OBJ) $(MEDIASESSIONOBJ) $(VADOBJ) $(VP8OBJ) $(OPUSOBJ) $(AACOBJ) $(DEPACKETIZERSOBJ) $(AMRNBOBJ) $(AMRWBOBJ) $(MP3OBJ)

TARGETS=libmediacomponents

ifeq ($(VADWEBRTC),yes)
	VADINCLUDE = -I$(SRCDIR)/ext
	VADLD = $(SRCDIR)/ext/out/Release/obj/webrtc/common_audio/libcommon_audio.a
	OPTS+= -DVADWEBRTC
else
	VADINCLUDE =
	VADLD =
endif

OBJSLIB = $(OBJS)
BUILDOBJOBJSLIB = $(addprefix $(BUILD)/,$(OBJSLIB))


###################################
#Compilacion condicional
###################################
VPATH = %.o $(BUILD)/
VPATH+= %.c $(SRCDIR)/src/
VPATH+= %.cpp $(SRCDIR)/src/
VPATH+= %.cpp $(SRCDIR)/src/$(G711DIR)
VPATH+= %.cpp $(SRCDIR)/src/$(GSMDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(H264DIR)
VPATH+= %.cpp $(SRCDIR)/src/$(SPEEXDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(G722DIR)
VPATH+= %.cpp $(SRCDIR)/src/$(MEDIASESSIONDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(VP8DIR)
VPATH+= %.cpp $(SRCDIR)/src/$(OPUSDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(AACDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(AMRNBDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(AMRWBDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(MP3DIR)
VPATH+= %.cpp $(SRCDIR)/src/$(COREDIR)
VPATH+= %.cpp $(SRCDIR)/src/$(RTPRTCPDIR)

INCLUDE+= -I$(SRCDIR)/src -I$(SRCDIR)/include/ $(VADINCLUDE)

INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/oscl
INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/opencore/codecs_v2/audio/gsm_amr/common/dec/include
INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/opencore/codecs_v2/audio/gsm_amr/amr_nb/common/include
INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/opencore/codecs_v2/audio/gsm_amr/amr_nb/enc/src
INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/opencore/codecs_v2/audio/gsm_amr/amr_nb/dec/include
INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/opencore/codecs_v2/audio/gsm_amr/amr_nb/dec/src
INCLUDE+= -I$(SRCDIR)/opencore-amr-0.1.5/amrwb
INCLUDE+= -I$(SRCDIR)/vo-amrwbenc-0.1.3

LDFLAGS+= -lgsm -lpthread -lsrtp2

ifeq ($(STATIC_OPENSSL),yes)
	INCLUDE+= -I$(OPENSSL_DIR)/include
	LDFLAGS+= $(OPENSSL_DIR)/libssl.a $(OPENSSL_DIR)/libcrypto.a
else
	LDFLAGS+= -lssl -lcrypto
endif

ifeq ($(IMAGEMAGICK),yes)
	OPTS+=-DHAVE_IMAGEMAGICK `pkg-config --cflags ImageMagick++`
	LDFLAGS+=`pkg-config --libs ImageMagick++`
endif


LDFLAGS+= -lavcodec -lswscale -lavformat -lavutil -lavresample -lswresample
LDFLAGS+= -lspeex -lvpx -lopus -lopenh264 -logg -lmp4v2
LDFLAGS+= /usr/lib64/libopencore-amrnb.a /usr/lib64/libopencore-amrwb.a /usr/lib64/libvo-amrwbenc.a
LDFLAGS+= -lnsl -lpthread -lz -lresolv

CFLAGS  += $(INCLUDE) $(OPTS)
CXXFLAGS+= $(INCLUDE) $(OPTS)


%.o: %.c
	@echo "[CC ] $(TAG) $<"
	@gcc $(CFLAGS) -c $< -o $(BUILD)/$@

%.o: %.cpp
	@echo "[CXX] $(TAG) $<"
	@$(CXX) -std=c++11 $(CXXFLAGS) -c $< -o $(BUILD)/$@

############################################
#Targets
############################################
all: mkdirs $(TARGETS) certs

mkdirs:
	@mkdir -p $(BUILD)
	@mkdir -p $(BIN)

install:
	@mkdir -p /etc/linearhub/mediacomponents
	cp $(BIN)/$(TARGETS).so /usr/lib64/
	cp $(BIN)/lmp.key /etc/linearhub/mediacomponents/
	cp $(BIN)/lmp.crt /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/fadeIn.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/videoOff.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/borderPIP1.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/borderPIP2.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/borderPIP3.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/borderPIP4.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/borderPIP5.png /etc/linearhub/mediacomponents/
	cp $(SRCDIR)/etc/font.ttf /etc/linearhub/mediacomponents/

restart:
	@echo "reinstall library & restart"
	@systemctl stop linearmp
	@cp $(BIN)/$(TARGETS).so /usr/lib64/
	@systemctl start linearmp

clean:
	rm -f $(BIN)/$(TARGETS).so

cleanall: clean
	rm -rf $(BUILD)

certs:
ifeq ($(wildcard $(BIN)/lmp.crt), )
	@echo "Generating DTLS certificate files"
	@openssl req -sha256 -days 3650 -newkey rsa:1024 -nodes -new -x509 -keyout $(BIN)/lmp.key -out $(BIN)/lmp.crt
endif

libmediacomponents: $(OBJSLIB)
	gcc $(CXXFLAGS) -c src/mainlib.cpp -o $(BUILD)/mainlib.o
	gcc -shared -o $(BIN)/$@.so $(BUILDOBJOBJSLIB) $(BUILD)/mainlib.o $(LDFLAGS) $(VADLD)
